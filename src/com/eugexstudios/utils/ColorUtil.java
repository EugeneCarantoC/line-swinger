package com.eugexstudios.utils;

import android.graphics.Color;

public class ColorUtil {

	public final static int BG1_LAYER1   	    =  Color.rgb(115, 168, 236);
	public final static int BG1_LAYER2   	    =  Color.argb(200, 193, 44, 170); 
	public final static int BTN_SHADOW_1   	    =  Color.argb(65, 0, 0, 0); 
	
	public static int SetOpacity(float p_opacity, int p_red, int p_green, int p_blue)
	{
		return Color.argb((int)(p_opacity*255), p_red, p_green, p_blue);
	}
	public static int SetOpacity(float p_opacity, int p_color[])
	{
		return SetOpacity(p_opacity,  p_color[0]);
	}
	public static int SetOpacity(float p_opacity, int p_color)
	{ 
		return Color.argb((int)(p_opacity*Color.alpha(p_color)), Color.red(p_color), Color.green(p_color), Color.blue(p_color));
	}
	
	 
}
