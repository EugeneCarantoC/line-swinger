package com.eugexstudios.utils;

public interface SharedPref {
	
	//SharedPreferences
	public String HAS_GAME_DATA          = "HAS_GAME_DATA";
	public String LAST_GAME_VERSION      = "LAST_GAME_VERSION";
	public String BEST_SCORE             = "BEST_SCORE";
	public String SOUND_ON			     = "SOUND_ON";
	public String MUSIC_ON			     = "MUSIC_ON";
	public String VIBRATE_ON 		     = "VIBRATE_ON";
	public String LANGUAGE_USED 	     = "LANGUAGE_USED";
	public String SHORTCUT_CREATED 	     = "SHORTCUT_CREATED";
	public String APP_OPENED_CTR	     = "APP_OPENED_CTR";  
	

	public String SWINGER_USED          = "SWINGER_USED"; 
	public String SWINGER_PURCH          = "SWINGER_PURCH";
	public String GEMS_COLLECTED          = "GEMS_COLLECTED";
	
	 
}
