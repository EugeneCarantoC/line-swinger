package com.eugexstudios.utils;

public class Debugger {

	public final static boolean DEMO_MODE 		     = false;   
	public static boolean show_bounds 			     = false;   	// WHEEL, 
	public static boolean level_design 		         = false;    
	public static boolean play_immediate  			 = true;  	 // GAMESCREEN
	public static boolean win_immediate 			 = false;  	 // GAMESCREEN
	public static boolean show_touches 		   		 = false;    // GAMESCREEN
	public static boolean show_fps 		   			 = false;    // GAMESCREEN
	public static boolean has_no_splash 		     = false;     // GAMESCREEN
	public static boolean show_wheel_bottom 		 = false;     // WHEEL 
	public static boolean aquires_level_prog 		 = true;    //This does not refer to Demo mode to be enabled
	
}
