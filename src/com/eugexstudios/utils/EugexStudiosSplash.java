package com.eugexstudios.utils;
 
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.graphics.Rect; 

import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.lineswinger.AssetManager;
import com.eugexstudios.lineswinger.GameScreen;
import com.eugexstudios.lineswinger.Screen;

public class EugexStudiosSplash  extends Screen{

	final int ANIM_SIZE=5;
	private float [] m_animCtr;
	
	public EugexStudiosSplash()
	{
		m_animCtr = new float[ANIM_SIZE];
		for(int idx=0;idx<m_animCtr.length;idx++)
		{
			m_animCtr[idx] =0;
		}
		genSet.game.HideBannerAd();
	}
	@Override
	public void Paint(Graphics g) 
	{ 
		g.drawRect(new Rect(0,0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT), Color.WHITE);  
		
		genSet.setTextProperties(30,  Color.DKGRAY,  Align.RIGHT); 
		g.drawStringFont("e  u  g  e", CONFIG.SCREEN_WIDTH/2-40, CONFIG.SCREEN_HEIGHT/2-40, AssetManager.Comforta,genSet.paint);
		
		genSet.setTextProperties(120,  Color.DKGRAY,  Align.CENTER);  
		g.drawStringFont("X", CONFIG.SCREEN_WIDTH/2, CONFIG.SCREEN_HEIGHT/2, AssetManager.Comforta,genSet.paint);
		
		genSet.setTextProperties(30,  Color.DKGRAY,  Align.LEFT); 
		g.drawStringFont("s t u d i o s", CONFIG.SCREEN_WIDTH/2+40, CONFIG.SCREEN_HEIGHT/2-40, AssetManager.Comforta,genSet.paint);

		g.drawRect(new Rect(0,0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT), ColorUtil.SetOpacity((1-m_animCtr[1]), Color.WHITE));  

	}

	@Override
	public void Updates(float p_deltaTime)
	{
		// 0 - Entrance delay
		// 1 - White overlay entrance
		// 2 - Splash still 
		
		if(m_animCtr[0]<1)
 		{
			m_animCtr[0]+=0.012f * p_deltaTime;
 			
 			if(m_animCtr[0]>1)
 			{
 				m_animCtr[0]=1;
 				AssetManager.splash_sound.play(1.0f);
 			}
 		}
		
 		if(m_animCtr[0]>=1)
 		{
 			if(m_animCtr[1]<1)
 			{
 				m_animCtr[1]+=0.036f  * (p_deltaTime); 
 			}
 			
 			if(m_animCtr[1]>=1)
 			{
 				m_animCtr[1]=1; 
 			}
 		}
 		
 		if(m_animCtr[1]>=1)
 		{
 			if(m_animCtr[2]<1)
 			{
 				m_animCtr[2]+=0.0024f  * (p_deltaTime); 
 			}
 			
 			if(m_animCtr[2]>=1)
 			{
 				m_animCtr[1]=1; 
 				GameScreen.GoToMenu();
 			}
 		} 
	}

	@Override
	public void TouchUpdates(TouchEvent g)
	{
		
	}

	@Override
	public void backButton()
	{ 
		
	}

}
