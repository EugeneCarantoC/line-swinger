package com.eugexstudios.utils;
  
import android.annotation.TargetApi; 
import android.app.Activity; 
import android.os.Build;  
import android.view.WindowManager; 

import com.eugexstudios.framework.Screen;
import com.eugexstudios.framework.implementation.AndroidGame;
import com.eugexstudios.lineswinger.AssetManager;

public class Initializer extends AndroidGame {
 
	boolean firstTimeCreate = true; 
	public static int screenRotation;
	 
	@Override
	public Screen getInitScreen() {

		if (firstTimeCreate) { 
			firstTimeCreate = false; 
		} 
	    
		return new AssetManager(this); 
	}

	@Override
	public void onBackPressed() {
		
		getCurrentScreen().backButton();
	}
 
	@TargetApi(Build.VERSION_CODES.FROYO)
	@Override
	public void onResume() {
		super.onResume();
		WindowManager windowMgr = (WindowManager) this
				.getSystemService(Activity.WINDOW_SERVICE);
				screenRotation = windowMgr.getDefaultDisplay().getRotation();
 
	}

	@Override
	public void onPause() {
		super.onPause(); 

	}

	@Override
	public void SendBetaDatas() {
		// TODO Auto-generated method stub
		
	}
 
	
}