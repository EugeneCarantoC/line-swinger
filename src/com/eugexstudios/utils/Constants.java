package com.eugexstudios.utils;
 

import com.eugexstudios.framework.ButtonData;
import com.eugexstudios.framework.CONFIG;  	
import com.eugexstudios.framework.ImageData;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
  
public class Constants { 

	public static void SetScreenConfig(int p_wid, int p_height)
	{ 
		Point resolution = new Point(p_wid, p_height);

		 m_reso = reso_mode.default_size;
	     CONFIG.SCREEN_WIDTH   = p_wid;
	     CONFIG.SCREEN_HEIGHT  = p_height;  
//		 if(resolution.equals(new Point(720,1280)) // Screen 1
//				  ||resolution.equals(new Point(810,1440))   // Screen 1
//				  ||resolution.equals(new Point(768,1200))  
//		  )  
//		 {
//			 m_reso = reso_mode.default_size;
//		 }else if( 
//				 resolution.equals(new Point(480,800))   ||
//				 resolution.equals(new Point(480,854)) 
//				   )
//			 { 
//				 m_reso = reso_mode.size_b; 
//			 } 
//		 else if(resolution.equals(new Point(1080,1920))) 
//		 { 
//			 m_reso = reso_mode.size_e; 
//		 } 
//		 else if(
//				 resolution.equals(new Point(540,960)) 
//				   )
//		{ 
//				 m_reso = reso_mode.size_c; 
//		}  
//		 else if( 
//				 resolution.equals(new Point(600,1024)) 
//				   )
//		{ 
//				 m_reso = reso_mode.size_d; 
//		} 
//		  

	     	// RECALC all dependencies
	    CONFIG.SCREEN_MID = CONFIG.SCREEN_WIDTH/2;
		SCALE = (float)((float)CONFIG.SCREEN_WIDTH/720);	 
		SCREEN_RECT = new Rect(0,0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT); 
		BACK_BTN_CIR        = ScaleX(0.1388888888888889f);   
		SETT_LANG_BTN_DIST_Y =   ScaleY(0.093f);
		switch(m_reso)
		{
		  default: 

			START_BTN_IMG  = new ImageData(CONFIG.SCREEN_MID-145/2, ScaleY(0.626f),-5,-12,145,145);   
			START_BTN_DATA = new ButtonData(Constants.ScaleX(0.167f), START_BTN_IMG.y, Constants.ScaleX(0.667f), Constants.ScaleY(0.129f));

			GIFT_BTN_DATA = new ButtonData(Constants.ScaleX(0.25f),Constants.ScaleY(0.82f), Constants.ScaleX(0.1389f),  Constants.ScaleX(0.1389f));
			GIFT_BTN_IMG  = new ImageData(GIFT_BTN_DATA.x+GIFT_BTN_DATA.width/2-84/2,GIFT_BTN_DATA.height/2+GIFT_BTN_DATA.y ,13,1056,84,83);   
 
			SETTINGS_BTN_DATA = new ButtonData(Constants.ScaleX(0.44f),Constants.ScaleY(0.82f), Constants.ScaleX(0.1389f),  Constants.ScaleX(0.1389f));
			SETTINGS_BTN_IMG  = new ImageData(SETTINGS_BTN_DATA.x+SETTINGS_BTN_DATA.width/2-84/2,SETTINGS_BTN_DATA.height/2+SETTINGS_BTN_DATA.y ,189,1049,84,86);   
   
		     STORE_BTN_DATA = new ButtonData(Constants.ScaleX(0.63f),Constants.ScaleY(0.82f), Constants.ScaleX(0.1389f),  Constants.ScaleX(0.1389f));
			STORE_BTN_IMG  = new ImageData(STORE_BTN_DATA.x+STORE_BTN_DATA.width/2-80,STORE_BTN_DATA.height/2+STORE_BTN_DATA.y-25, 109,1073,76,65);   

			LOGO_SMALL= new ImageData(Constants.ScaleX(0.182f), Constants.ScaleY(0.16484375f),308,1065,491,246);   
			LOGO_SMALL_2= new ImageData(Constants.ScaleX(0.182f), Constants.ScaleY(0.16484375f),308,1065+250,491,246);   
			LOGO_SMALL_3= new ImageData(Constants.ScaleX(0.182f), Constants.ScaleY(0.16484375f),308,1065+250*2,491,246);   

			GAMEOVER_IMG   = new ImageData(58,179, 709,172,597,297); 
			GAMEOVER_IMG_2 = new ImageData(58,179, 709,172+300,597,297); 
			GAMEOVER_IMG_3 = new ImageData(58,179, 709,172+300*2,597,297);  

			RESTART_IMG    = new ImageData(Constants.ScaleX(0.5f)-160/2,690, 814,1079,160,164); 
			HOME_IMG     = new ImageData(Constants.ScaleX(0.5f)-86-9, Constants.ScaleY(0.85f), 1003, 1089, 86, 82); 
			SHARE_IMG    = new ImageData(Constants.ScaleX(0.5f)+15, Constants.ScaleY(0.85f), 1111, 1097, 80, 78); 
			NEW_BEST_SCORE = new ImageData(Constants.ScaleX(0.5f)-369/2, Constants.ScaleY(0.41f), 1327, 178, 369, 54); 
			DISCOVER_BTN = new ButtonData((Constants.ScaleX(0.19f)), (int)(Constants.ScaleY(0.55f)), (int)(Constants.ScaleX(0.62f)),Constants.ScaleY(0.0859375f)); 
			RATE_BTN = new ButtonData((Constants.ScaleX(0.19f)), (int)(Constants.ScaleY(0.71f)), (int)(Constants.ScaleX(0.62f)),Constants.ScaleY(0.0859375f)); 
			LOCK_IMG = new ImageData(11,-33,1249,1100,48,62); 
			GEM_IMG = new ImageData(0,0,1312,1115,65,60); 
			GEMCOUNT_IMG = new ImageData(0,0,1398,1117,60,51); 
			BACK_BTN_IMG = new ImageData(Constants.ScaleX(0.035f), Constants.ScaleX(0.05f),1476,1091,99,103);
			
			
			
			ACCEPT_INSTR_BTN= new ButtonData(Constants.ScaleX(0.2f),Constants.ScaleY(0.80f),Constants.ScaleX(0.6f),Constants.ScaleY(0.10f));
			YES_EXIT = new ButtonData( 
					 (int)(CONFIG.SCREEN_WIDTH*0.07f)
					,(int)(CONFIG.SCREEN_HEIGHT*0.439f)
					,(int)(CONFIG.SCREEN_WIDTH*0.42f)
					,(int)(CONFIG.SCREEN_HEIGHT*0.112f));  
			
			NO_EXIT = new ButtonData( 
					 (int)(CONFIG.SCREEN_WIDTH*0.07f)+(int)(CONFIG.SCREEN_WIDTH*0.445f)
					,(int)(CONFIG.SCREEN_HEIGHT*0.439f)
					,(int)(CONFIG.SCREEN_WIDTH*0.42f)
					,(int)(CONFIG.SCREEN_HEIGHT* 0.112f));  
		    
			break;
		 
		}
		
	}

	 
	public static int ScaleX(float p_perc)
	{
		return (int)(CONFIG.SCREEN_WIDTH*p_perc);	
	}
	public static int ScaleY(float p_perc)
	{
		return (int)(CONFIG.SCREEN_HEIGHT*p_perc);	
	}
	 
   public enum reso_mode
   {
   	 default_size
    	,size_b
    	,size_c
    	,size_d 
    	,size_e 
   }
   public  static reso_mode m_reso;

	//STARTAPP
	public final static String NATIVE_AD_MENU     = "menu_native_ad_01"; 
	public final static String NATIVE_AD_EXIT     = "exit_native_ad_01"; 
	public final static String NATIVE_AD_GAMEOVER = "game_gameover_ad_02";  
	public final static String NATIVE_AD_GAMEOVER_2 = "game_gameover_ad_03";  
	public final static String VIDEO_AD_CONTINUE  = "game_video_ad_01"; 
	public final static String VIDEO_AD_LVLCOMP  = "video_ad_lvlcomp"; 
	public final static String REWARD_VIDEO_AD_CONT  = "video_ad_reward_cont"; 
	public final static String BETA_LVL_TRY_      ="BETA_LVL_TRY_"; 
	
	public final static int VIDEO_AD_CHANCE =3; // 33% to get a video ad on level complete  
	public  static float SCALE = (float)((float)CONFIG.SCREEN_WIDTH/720);	 // COnstant yung 720, jan original dinesign yung game
	public final int BANNER_AD_SPACE_HEIGHT=125; 
	public   static Rect SCREEN_RECT = new Rect(0,0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT);
	 

	public final int SHADOW_COLOR    		= Color.argb(100,20, 20, 20);
	public final static int ARC_INACTIVE_OVERLAY = Color.argb(150,255, 255, 255); 
	public final int DEBUG_BOUNDS_COLOR   = Color.argb(200, 0, 255,0);
	public final static int DEBUG_BOUNDS_COLOR_2 = Color.argb(50, 255, 255,240);
	public final static int PASTELIZER_COLOR = Color.argb(70, 255, 255, 255);
	public final int GAMEOVER_COLOR_OVERLAY = Color.rgb(245, 30,20);
 	     
	public final static int SHOW_AD_AFTER_N_PLAY = 3;

	public static ImageData START_BTN_IMG;
	public static ButtonData START_BTN_DATA;
	public static ImageData  GIFT_BTN_IMG;
	public static ButtonData GIFT_BTN_DATA;
	public static ImageData  SETTINGS_BTN_IMG;
	public static ButtonData SETTINGS_BTN_DATA;
	public static ImageData  STORE_BTN_IMG;
	public static ButtonData STORE_BTN_DATA; 
	public static ButtonData YES_EXIT;
	public static ButtonData NO_EXIT;   
	public static ButtonData DISCOVER_BTN; 
	public static ButtonData RATE_BTN;   
	public static ButtonData ACCEPT_INSTR_BTN;   
	
	public static ImageData  LOGO_SMALL;
	public static ImageData  LOGO_SMALL_2;
	public static ImageData  LOGO_SMALL_3;
	
	public static ImageData  GAMEOVER_IMG;
	public static ImageData  GAMEOVER_IMG_2;
	public static ImageData  GAMEOVER_IMG_3;
	public static ImageData  RESTART_IMG;
	public static ImageData  SHARE_IMG;
	public static ImageData  HOME_IMG;
	public static ImageData  NEW_BEST_SCORE;
	public static ImageData  LOCK_IMG;
	public static ImageData  GEM_IMG;
	public static ImageData  GEMCOUNT_IMG;
	public static ImageData  BACK_BTN_IMG; 
  
	public static int BACK_BTN_CIR               = ScaleX(0.0694444444444444f);   
	public static int SETTINGS_LS               = 65;
	public static int SETTINGS_BTN1_S               = 30; 
	public static int SETTINGS_BTN_S               = 45; 
	public static int ABOUT_LS               = 50;
	public static int CT_LS               = 75;
	public static int VER_LS               = 28; 
	public static int CREATED_LS               = 38; 
	public static int EUGEX_LS               = 40; 
	public static int PLAY_MORE_LS               = 26; 
	public static int EXPLR_GAMES_LS               = 35; 
	public static int SHARE_CT_LS               = 26;  
	public static int SHARE_NOW_LS               = 35;  

	public static int CONGRATS_LS               = 60;   
	public static int ASK_EXIT_LS               = 50; 
	public static int CONT_GAME_LS               = 75;  

	public static int GAME_OVER_LS               = 85;   
	public static int COUNTDOWN_LS               = 180;     
	public static int RESTART_BS = 155;   
	public static int SETT_LANG_LS = 58;   
	public static int SETT_LANG_BTN_LS = 46;   
	public static int SETT_LANG_BTN_DIST_Y;
	public static int PLAY_INSTR_1 = 40;   
	
	
}
