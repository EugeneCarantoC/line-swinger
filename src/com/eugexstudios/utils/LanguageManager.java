package com.eugexstudios.utils;

import com.eugexstudios.lineswinger.GameScreen;

public class LanguageManager {

	private final static String GAME_TITLE ="Line Swinger";
	public static enum W{
		  LANGUAGE
		  
		, SAVE
		, CANCEL
		
		, ENGLISH 
		, JAPANESE
		, SPANISH
		, GERMAN
		, KOREAN
		, PORTUGUESE
		
		, SOUND
		, MUSIC
		, VIBRATION
		, RESET_DATA

		, ON
		, OFF
		
		, START
		, GOT_IT
		
		, YES
		, NO

		, SETTINGS
		, ASK_EXIT
		, ASK_RESET_L1
		, ASK_RESET_L2
		, SELECT_LEVEL
		, SELECT_STAGE
		
		, GAME_OVER
		, RESTART
		, CONTINUE
		, WATCH_VIDEO
		, LEVEL_COMPLETE
		, STAGE_COMPLETE

		, TRY_IT_NOW
		, INSTALL
		, MAYBE_LATER
		, SPONSORED
		, INSTR_TAP_ANYWHERE
		, INSTR_HIT_MATCH
		, INSTR_AVOID_SHIELD
		, LEVEL
		, STAGE
		, ABOUT
		, VERSION
		, CREATED_BY
		, PLAY_MORE_FROM
		, EXPLORE_GAMES
		, SHARE_CW
		, SHARE_NOW
		, COLORED_SHIELD_INSTR_1
		, COLORED_SHIELD_INSTR_2

		, CONGRATS
		, YOU_COMPLETED_1
		, YOU_COMPLETED_2
		, OKAY
		, CONT_GAME
		, RATE_US
		, DID_YOU_LIKE
		, ASK_PURCHASE
		, PURCH_SUCCESS
		, NOT_ENOUGH_GEMS
		, TAP_ANYWHERE_TO_SWING
		, DONT_DRAG
		
	}
	
	public static String GetLocalized(W p_str)
	{ 
		return GetTranslation(p_str,GameScreen.GetLanguage());
	}
	public static String GetTranslation(W p_str, GameScreen.LANGUAGE p_lang)
	{
		switch(p_str)
		{
		
		case START:
			switch(p_lang)
			{
				case ENGLISH:    return "Start";
				case JAPANESE:   return "開始";
				case SPANISH:    return "Comienzo";
				case GERMAN:     return "Anfang";
				case KOREAN:     return "스타트";
				case PORTUGUESE: return "Começar";
			}
		break;
		case GOT_IT:
			switch(p_lang)
			{
				case ENGLISH:    return "Got it";
				case JAPANESE:   return "�?��?��?�";
				case SPANISH:    return "Lo tengo";
				case GERMAN:     return "Ich habs";
				case KOREAN:     return "알았다";
				case PORTUGUESE: return "Consegui";
			}
		break;
		
		 
		case CONT_GAME:
			switch(p_lang)
			{
				case ENGLISH:    return "Continue game?";
				case JAPANESE:   return "ゲームを続行�?��?��?��?�？";
				case SPANISH:    return "¿Continua el juego?";
				case GERMAN:     return "Weiterspielen?";
				case KOREAN:     return "게임�?� 계�? 하시겠습니까?";
				case PORTUGUESE: return "Continuar o jogo?";
			}
		break;
		case COLORED_SHIELD_INSTR_1:
			switch(p_lang)
			{
				case ENGLISH:    return "Your bullets can pass through";
				case JAPANESE:   return "�?��?��?��?�弾�?��?��?�色�?�盾";
				case SPANISH:    return "Tus balas pueden atravesar";
				case GERMAN:     return "Deine Kugeln können Schilde";
				case KOREAN:     return "�?알�?� 같�?� 색으로 방패를";
				case PORTUGUESE: return "Suas balas podem passar ";
			}
		break;
		case COLORED_SHIELD_INSTR_2:
			switch(p_lang)
			{
				case ENGLISH:    return "shields with the same color";
				case JAPANESE:   return "を通�?��?�る�?��?��?��?��??�?��?�";
				case SPANISH:    return "escudos del mismo color";
				case GERMAN:     return "mit der gleichen Farbe passieren";
				case KOREAN:     return "통과 할 수 있습니다.";
				case PORTUGUESE: return "por escudos com a mesma cor";
			}
		break;
		
		
		case SELECT_LEVEL:
			switch(p_lang)
			{
				case ENGLISH:    return "SELECT LEVEL";
				case JAPANESE:   return "レベルを�?�択";
				case SPANISH:    return "SELECCIONA EL NIVEL";
				case GERMAN:     return "STUFE AUSWÄHLEN";
				case KOREAN:     return "레벨 선�?";
				case PORTUGUESE: return "SELECIONE O N�?VEL";
			}
		break;
		
		case SELECT_STAGE:
			switch(p_lang)
			{
				case ENGLISH:    return "SELECT STAGE";
				case JAPANESE:   return "ステージを�?�択";
				case SPANISH:    return "SELECCIONE LA ETAPA";
				case GERMAN:     return "Wählen Sie Bühne";
				case KOREAN:     return "스테�?�지 선�?";
				case PORTUGUESE: return "Selecione o estágio";
			}
		break;

		
		case SHARE_CW:
			switch(p_lang)
			{
				case ENGLISH:    return "Share this game to your friends!";
				case JAPANESE:   return "�?��?�ゲームを�?��?��?��?��?�人�?�教�?��?��??�?��?��?��?";
				case SPANISH:    return "¡Comparte este juego a tus amigos!";
				case GERMAN:     return "Teile dieses Spiel deinen Freunden!";
				case KOREAN:     return "�?� 게임�?� 친구들과 공유하십시오!";
				case PORTUGUESE: return "Compartilhe este jogo com seus amigos!";
			}
		break; 
		
		case SHARE_NOW:
			switch(p_lang)
			{
				case ENGLISH:    return "SHARE NOW";
				case JAPANESE:   return "今�?��??共有�?�る";
				case SPANISH:    return "COMPARTE AHORA";
				case GERMAN:     return "JETZT TEILEN";
				case KOREAN:     return "지금 공유하기";
				case PORTUGUESE: return "COMPARTILHE AGORA";
			}
		break;
		case EXPLORE_GAMES:
			switch(p_lang)
			{
				case ENGLISH:    return "Explore games";
				case JAPANESE:   return "ゲームを探検�?�る";
				case SPANISH:    return "Explore juegos";
				case GERMAN:     return "Spiele erkunden";
				case KOREAN:     return "게임 �?색";
				case PORTUGUESE: return "Expolar jogos";
			}
		break;
		case PLAY_MORE_FROM:
			switch(p_lang)
			{
				case ENGLISH:    return "Play more from Eugex Studios";
				case JAPANESE:   return "Eugex Studios �?��??�?�他�?�動画";
				case SPANISH:    return "Juega más desde Eugex Studios";
				case GERMAN:     return "Spielen Sie mehr bei Eugex Studios";
				case KOREAN:     return "Eugex Studios �?� �?� 많�?� 제품보기";
				case PORTUGUESE: return "Jogue mais de Eugex Studios";
			}
		break;
		
		
		case ABOUT:
			switch(p_lang)
			{
				case ENGLISH:    return "About";
				case JAPANESE:   return "約";
				case SPANISH:    return "Acerca de";
				case GERMAN:     return "Über";
				case KOREAN:     return "약";
				case PORTUGUESE: return "Sobre";
			}
		break;	
		
		case VERSION:
			switch(p_lang)
			{
				case ENGLISH:    return "Version";
				case JAPANESE:   return "�?ージョン";
				case SPANISH:    return "Versión";
				case GERMAN:     return "Ausführung";
				case KOREAN:     return "번역";
				case PORTUGUESE: return "Versão";
			}
		break;
		
		case CREATED_BY:
			switch(p_lang)
			{
				case ENGLISH:    return "Created by";
				case JAPANESE:   return "�?�よ�?��?�作�?�?�れ�?�";
				case SPANISH:    return "Creado por";
				case GERMAN:     return "Erstellt von";
				case KOREAN:     return "만든 사람 ";
				case PORTUGUESE: return "Criado por";
			}
		break;
		  
		
		case INSTR_TAP_ANYWHERE:
			switch(p_lang)
			{
				case ENGLISH:    return "Tap anywhere to shoot";
				case JAPANESE:   return "�?��?��?�もタップ�?��?�撮影";
				case SPANISH:    return "Toca en algún lugar para disparar";
				case GERMAN:     return "Tippe irgendwo hin, um zu schießen";
				case KOREAN:     return "촬�?할 곳�?� 아무 �?�서 누릅니다.";
				case PORTUGUESE: return "Toque em algum lugar para atirar";
			}
		break;
		case INSTR_HIT_MATCH:
			switch(p_lang)
			{
				case ENGLISH:    return "Hit matching colors";
				case JAPANESE:   return "一致�?�る色を打�?�";
				case SPANISH:    return "Haz coincidir los colores";
				case GERMAN:     return "Treffe passende Farben";
				case KOREAN:     return "�?�치하는 색�? 조회";
				case PORTUGUESE: return "Cores parecidas";
			}
		break;
		
		case INSTR_AVOID_SHIELD:
			switch(p_lang)
			{
				case ENGLISH:    return "Avoid shields with different color";
				case JAPANESE:   return "異�?�る色�?�シールドを�?��?�る";
				case SPANISH:    return "Evite los escudos con diferentes colores";
				case GERMAN:     return "Vermeiden Sie Schilde mit unterschiedlicher Farbe";
				case KOREAN:     return "다른 색�?�?� 방패를 피하십시오.";
				case PORTUGUESE: return "Evite escudos com cores diferentes";
			}
		case SPONSORED:
			switch(p_lang)
			{
				case ENGLISH:    return "Sponsored";
				case JAPANESE:   return "開始";
				case SPANISH:    return "Patrocinador";
				case GERMAN:     return "Sponsor";
				case KOREAN:     return "스�?�서";
				case PORTUGUESE: return "Patrocinador";
			}
		break;
		case LEVEL:
			switch(p_lang)
			{
				case ENGLISH:    return "Level";
				case JAPANESE:   return "レベル";
				case SPANISH:    return "Nivel";
				case GERMAN:     return "Niveau";
				case KOREAN:     return "수�?�";
				case PORTUGUESE: return "Nível";
			}
		break;
		
		case STAGE:
			switch(p_lang)
			{
				case ENGLISH:    return "Stage";
				case JAPANESE:   return "ステージ";
				case SPANISH:    return "Escenario";
				case GERMAN:     return "Bühne";
				case KOREAN:     return "단계";
				case PORTUGUESE: return "Etapa";
			}
		break;
		
		case GAME_OVER:
			switch(p_lang)
			{
				case ENGLISH:    return "GAME OVER";
				case JAPANESE:   return "ゲームオー�?ー";
				case SPANISH:    return "Juego terminado";
				case GERMAN:     return "Spiel ist aus";
				case KOREAN:     return "게임 �??";
				case PORTUGUESE: return "Fim de jogo";
			}
		break;
		case TRY_IT_NOW:
			switch(p_lang)
			{
				case ENGLISH:    return "Try it now!";
				case JAPANESE:   return "や�?��?��?�よ�?��?";
				case SPANISH:    return "¡Pruebalo ahora!";
				case GERMAN:     return "Probieren Sie es jetzt!";
				case KOREAN:     return "지금 시�?�해보십시오!";
				case PORTUGUESE: return "Tente agora!";
			}
		break;
		
		case INSTALL:
			switch(p_lang)
			{
				case ENGLISH:    return "Install";
				case JAPANESE:   return "インストール";
				case SPANISH:    return "Instalar";
				case GERMAN:     return "Installieren";
				case KOREAN:     return "설치";
				case PORTUGUESE: return "Instalar";
			}
		break;
		
		
		case MAYBE_LATER:
			switch(p_lang)
			{
				case ENGLISH:    return "Maybe later";
				case JAPANESE:   return "多分後�?�";
				case SPANISH:    return "Quizas mas tarde";
				case GERMAN:     return "Vielleicht später";
				case KOREAN:     return "나중�?";
				case PORTUGUESE: return "Talvez mais tarde";
			}
		break;
		case RESTART:
			switch(p_lang)
			{
				case ENGLISH:    return "Restart";
				case JAPANESE:   return "�?起動";
				case SPANISH:    return "Reiniciar";
				case GERMAN:     return "Neustart";
				case KOREAN:     return "재시작";
				case PORTUGUESE: return "Reiniciar";
			}
		break;
		case CONTINUE:
			switch(p_lang)
			{
				case ENGLISH:    return "Continue";
				case JAPANESE:   return "�?続�?�る";
				case SPANISH:    return "Continuar";
				case GERMAN:     return "Fortsetzen";
				case KOREAN:     return "잇다";
				case PORTUGUESE: return "Continuar";
			}
		break;
		case WATCH_VIDEO:
			switch(p_lang)
			{
				case ENGLISH:    return "(Watch video)";
				case JAPANESE:   return "(ビデオを見る)";
				case SPANISH:    return "(Ver video)";
				case GERMAN:     return "(Schau Video)";
				case KOREAN:     return "(비디오를보다)";
				case PORTUGUESE: return "(Assista vídeo)";
			}
		break;
		
		case ASK_EXIT:
			switch(p_lang)
			{
				case ENGLISH:    return "Exit " + GAME_TITLE +"?";
				case JAPANESE:   return GAME_TITLE +" 出�?�？";
				case SPANISH:    return "¿Salir de " + GAME_TITLE + "?";
				case GERMAN:     return "Beenden Sie " + GAME_TITLE +"?";
				case KOREAN:     return GAME_TITLE+ " 출구?";
				case PORTUGUESE: return "Sair " + GAME_TITLE + "?";
			}
		break; 
		case ASK_RESET_L1:
			switch(p_lang)
			{
				case ENGLISH:    return "This will reset all your progress.";
				case JAPANESE:     return "�?�れ�?�より�?�?��?��?��?�進�?�状�?�?�リセット�?�れ�?��?�。"; 
				case SPANISH:    return "Esto restablecerá todo tu";
				case GERMAN:      return "Dadurch werden alle deine Fortschritte zurückgesetzt."; 
				case KOREAN:       return "그러면 모든 진행 �?황�?� 재설정�?�니다."; 
				case PORTUGUESE:   return "Isso irá redefinir todo o seu";
			}

			break; 
		case ASK_RESET_L2:
				switch(p_lang)
				{
					case ENGLISH:    return "Are you sure?";
					case JAPANESE:     return "本気�?��?��?�？";
					case SPANISH:    return " progreso. ¿Estás seguro?";
					case GERMAN:       return "Bist du sicher?";
					case KOREAN:      return "확실합니까?";
					case PORTUGUESE:  return "progresso. Você tem certeza?";
				}
		break;
			
			case LANGUAGE:
				switch(p_lang)
				{
					case ENGLISH:    return "Language";
					case JAPANESE:   return "言語";
					case SPANISH:    return "Idioma";
					case GERMAN:     return "Sprache";
					case KOREAN:     return "언어";
					case PORTUGUESE: return "Língua";
				}
			break;
			case SAVE:
				switch(p_lang)
				{
					case ENGLISH:    return "Save";
					case JAPANESE:   return "セーブ";
					case SPANISH:    return "Salvar";
					case GERMAN:     return "Sparen";
					case KOREAN:     return "괜찮아";
					case PORTUGUESE: return "Salve";
				}
			break;
			case CANCEL:
				switch(p_lang)
				{
					case ENGLISH:    return "Cancel";
					case JAPANESE:   return "キャンセル";
					case SPANISH:    return "Cancelar";
					case GERMAN:     return "Stornieren";
					case KOREAN:     return "취소";
					case PORTUGUESE: return "Cancelar";
				}
			break;
			case ENGLISH:
				switch(p_lang)
				{
					case ENGLISH: return "English";
					case JAPANESE: return "英語";
					case SPANISH: return "Inglés";
					case GERMAN: return "Englisch";
					case KOREAN: return  "�?어";
					case PORTUGUESE: return "Inglês";
				}
			break;
			case JAPANESE:
				switch(p_lang)
				{
					case ENGLISH: return "Japanese";
					case JAPANESE: return "日本";
					case SPANISH: return "Japonés";
					case GERMAN: return "Japanisch";
					case KOREAN: return "니혼고";
					case PORTUGUESE: return  "Japonés";
				}
			break;
			case SPANISH:
				switch(p_lang)
				{
					case ENGLISH: return "Spanish";
					case JAPANESE: return "スペイン語";
					case SPANISH: return "Español";
					case GERMAN: return "Spanisch";
					case KOREAN: return "스페�?� 사람";
					case PORTUGUESE: return "Espanhol";
				}
			break;
			case GERMAN:
				switch(p_lang)
				{
					case ENGLISH: return "German";
					case JAPANESE: return "ドイツ人";
					case SPANISH: return "Alemán";
					case GERMAN: return "Deutsche";
					case KOREAN: return "�?��?� 사람";
					case PORTUGUESE: return  "Alemão";
				}
			break;
			case KOREAN:
				switch(p_lang)
				{
					case ENGLISH: return "Korean";
					case JAPANESE: return "韓国語";
					case SPANISH: return "Coreano";
					case GERMAN: return "Koreanisch";
					case KOREAN: return "한국어";
					case PORTUGUESE: return "Coreano";
				}
			break;
			case PORTUGUESE:
				switch(p_lang)
				{
					case ENGLISH: return "Portuguese";
					case JAPANESE: return "�?ルトガル";
					case SPANISH: return "Portugués";
					case GERMAN: return "Portugiesisch";
					case KOREAN: return "�?�르투갈 �?�";
					case PORTUGUESE: return "Português";
				}
			break;
			case SOUND:
				switch(p_lang)
				{
					case ENGLISH: return "Sound";
					case JAPANESE: return "サウンド";
					case SPANISH: return "Sonidos"; 
					case GERMAN: return "Geräusche";
					case KOREAN: return  "소리";
					case PORTUGUESE: return  "Soa";
				}
			break;
			case MUSIC:
				switch(p_lang)
				{
					case ENGLISH: return "Music";
					case JAPANESE: return "音楽"; 
					case SPANISH: return   "Música";
					case GERMAN: return "Musik";
					case KOREAN: return  "�?�악";
					case PORTUGUESE: return  "Música";
				}
			break;
			 
			case RESET_DATA:
				switch(p_lang)
				{
					case ENGLISH: return "Reset Data";
					case JAPANESE: return "Reset Data";
					case SPANISH: return  "Reset Data";
					case GERMAN: return "Reset Data";
					case KOREAN: return "Reset Data";
					case PORTUGUESE: return  "Reset Data";
				}
			break;
			case VIBRATION:
				switch(p_lang)
				{
					case ENGLISH: return "Vibration";
					case JAPANESE: return "電話�?�振動"; 
					case SPANISH: return  "Vibración del teléfono";
					case GERMAN: return "Vibration"; 
					case KOREAN: return  "전화 진�?�";
					case PORTUGUESE: return  "Vibração";
				}
			break;
			case ON:
				switch(p_lang)
				{
					case ENGLISH: return "ON";
					case JAPANESE: return "�?�"; 
					case SPANISH: return  "Encender";
					case GERMAN: return "Anmachen";
					case KOREAN: return  "켜다";
					case PORTUGUESE: return  "Ligar";
				}
			break;
			case OFF:
				switch(p_lang)
				{
					case ENGLISH: return "OFF";
					case JAPANESE: return "オフ"; 
					case SPANISH: return  "Apagar";
					case GERMAN: return "Abschalten"; 
					case KOREAN: return  "떨어져서";
					case PORTUGUESE: return  "Desligar";
				}
			break;
			case SETTINGS:
				switch(p_lang)
				{
					case ENGLISH: return "Settings";
					case JAPANESE: return "設定"; 
					case SPANISH: return  "Configuraciones";
					case GERMAN: return "die Einstellungen"; 
					case KOREAN: return  "설정";
					case PORTUGUESE: return  "Configurações";
				}
			break;
			case YES:
				switch(p_lang)
				{
					case ENGLISH: return "YES";
					case JAPANESE: return "�?��?�"; 
					case SPANISH: return  "Sí";
					case GERMAN: return "Ja"; 
					case KOREAN: return  "예";
					case PORTUGUESE: return  "Sim";
				}
			break;
			case NO:
				switch(p_lang)
				{
					case ENGLISH: return "NO";
					case JAPANESE: return "�?��?��?�"; 
					case SPANISH: return  "No";
					case GERMAN: return "Nein"; 
					case KOREAN: return  "아니";
					case PORTUGUESE: return  "Não";
				}
			break;
			case LEVEL_COMPLETE:
				switch(p_lang)
				{
					case ENGLISH:    return "Level Complete!";
					case JAPANESE:   return "レベル完了�?";
					case SPANISH:    return "¡Nivel completado!";
					case GERMAN:     return "Level geschafft!";
					case KOREAN:     return "레벨 완료!";
					case PORTUGUESE: return "Nível completo!";
				}
			break;
 
			case STAGE_COMPLETE:
				switch(p_lang)
				{
					case ENGLISH:    return "Stage Complete!";
					case JAPANESE:   return "ステージ完�?�?";
					case SPANISH:    return "Etapa completa!";
					case GERMAN:     return "Bühne fertig!";
					case KOREAN:     return "무대 완성!";
					case PORTUGUESE: return "Palco Completo!";
				}
			break;
			
			case CONGRATS:
				switch(p_lang)
				{
					case ENGLISH:    return "Congratulations!";
					case JAPANESE:   return "�?��?�?��?��?�!";
					case SPANISH:    return "Felicitaciones";
					case GERMAN:     return "Herzliche Glückwünsche";
					case KOREAN:     return "치하";
					case PORTUGUESE: return "Parabéns	";
				}
			break;
			case YOU_COMPLETED_1:
				switch(p_lang)
				{
					case ENGLISH:    return "Stage Complete!";
					case JAPANESE:   return "ステージ完�?�?";
					case SPANISH:    return "Etapa completa!";
					case GERMAN:     return "Bühne fertig!";
					case KOREAN:     return "무대 완성!";
					case PORTUGUESE: return "Palco Completo!";
				}
			break;
			case YOU_COMPLETED_2:
				switch(p_lang)
				{
					case ENGLISH:    return "Stage Complete!";
					case JAPANESE:   return "ステージ完�?�?";
					case SPANISH:    return "Etapa completa!";
					case GERMAN:     return "Bühne fertig!";
					case KOREAN:     return "무대 완성!";
					case PORTUGUESE: return "Palco Completo!";
				}
			break;
			case OKAY:
				switch(p_lang)
				{
					case ENGLISH:    return "Okay";
					case JAPANESE:   return "�?��?�";
					case SPANISH:    return "Bueno";
					case GERMAN:     return "Okay";
					case KOREAN:     return "괜찮아";
					case PORTUGUESE: return "OK";
				}
			break;
			case RATE_US:
				switch(p_lang)
				{
					case ENGLISH:    return "Rate us";
					case JAPANESE:   return "レートゲーム";
					case SPANISH:    return "Califica el juego";
					case GERMAN:     return "Rate Spiel";
					case KOREAN:     return "앱 �?�가";
					case PORTUGUESE: return "Taxa de jogo";
				}
			break;
			case DID_YOU_LIKE:
				switch(p_lang)
				{
					case ENGLISH:    return "Did you like 'Line Swinger'?";
					case JAPANESE:   return "�?��?��?��?�ゲーム�?�好�??�?��?��?��?�";
					case SPANISH:    return "¿Te gustó Line Swinger?";
					case GERMAN:     return "Mochtest du Line Swinger?";
					case KOREAN:     return "Line Swinger 좋아 했니?";
					case PORTUGUESE: return "Você gostou de Line Swinger?";
				}
			break;
			case ASK_PURCHASE:
				switch(p_lang)
				{
					case ENGLISH:    return "Purchase item?";
					case JAPANESE:   return "購入アイテム�?��?��?�？";
					case SPANISH:    return "¿Adquirir artículo?";
					case GERMAN:     return "Kaufartikel?";
					case KOREAN:     return "물품�?� 구입 하시겠습니까?";
					case PORTUGUESE: return "Artigo da compra?";
				}
			break;
			
			case PURCH_SUCCESS:
				switch(p_lang)
				{
					case ENGLISH:    return "Item purchased!";
					case JAPANESE:   return "購入�?��?�アイテム";
					case SPANISH:    return "Articulo comprado";
					case GERMAN:     return "Artikel gekauft";
					case KOREAN:     return "구입 한 품목";
					case PORTUGUESE: return "Item comprado";
				}
			break;
			case NOT_ENOUGH_GEMS:
				switch(p_lang)
				{
					case ENGLISH:    return "You don't have enough gems!";
					case JAPANESE:   return "�??分�?��?石�?��?�り�?��?�ん�?";
					case SPANISH:    return "¡No tienes suficientes gemas!";
					case GERMAN:     return "Du hast nicht genug Edelsteine!";
					case KOREAN:     return "당신�?� 충분한 보�?�?� 없습니다!";
					case PORTUGUESE: return "Você não tem gemas suficientes!";
				}
			break; 
			
			case TAP_ANYWHERE_TO_SWING:
				switch(p_lang)
				{
					case ENGLISH:    return "TAP ANYWHERE TO SWING";
					case JAPANESE:   return "スイング�?�る場所をタップ";
					case SPANISH:    return "GOLPEA DONDEQUIERA AL SWING";
					case GERMAN:     return "TAP ÜBERALL ZU SWING";
					case KOREAN:     return "스윙하려면 아무 곳�?�나 누릅니다.";
					case PORTUGUESE: return "TAP EM QUALQUER LUGAR PARA BALANÇAR";
				}
			break;
			case DONT_DRAG:
				switch(p_lang)
				{
					case ENGLISH:    return "You don't need to swipe. Just tap the screen.";
					case JAPANESE:   return "スワイプ�?�る必�?�?��?�り�?��?�ん。画�?�をタップ�?�る�?��?��?��?�。";
					case SPANISH:    return "No necesita deslizar. Solo toca la pantalla.";
					case GERMAN:     return "Sie müssen nicht wischen. Tippe einfach auf den Bildschirm.";
					case KOREAN:     return "스 와�?�프 할 필요가 없습니다. 화면�?� 탭하기 만하면�?�니다.";
					case PORTUGUESE: return "Você não precisa roubar. Basta tocar na tela.";
				}
			break;
			
		}
		return "";
	}
}
