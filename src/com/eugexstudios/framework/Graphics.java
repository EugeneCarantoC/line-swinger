package com.eugexstudios.framework;
  
import com.eugexstudios.utils.LanguageManager;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;

public interface Graphics {
	public static enum ImageFormat {
		ARGB8888, ARGB4444, RGB565
	}
	public int getWidth();
	public int getHeight();
	public Canvas getCanvas(); 
	public AssetManager getAssetManager();
	public Image newImage(String fileName, ImageFormat format);
	public Bitmap newBitmap(String fileName, ImageFormat format); 
	
	public void clearScreen(int color); 
	
    public void BlurEverything();
    public void RemoveBlur();
    public void SaveState();
    public void PaintSavedState();
    
	public void drawCircle(int x, int y, int radius, int color);
	public void drawCircle(Point p_loc, int radius, int color); 
	
    public void drawArc(int p_x, int p_y, int p_radius,  int p_sweepStart,int p_arcAngle, int p_color);
    public void drawMaskedArc(int p_x, int p_y, int p_radius,int p_endRadius,  int p_sweepStart,int p_arcAngle, int p_color);
    
	public void drawLine(int x, int y, int x2, int y2, int color);
	public void drawLine(int x, int y, int x2, int y2, int color, int p_strokeSize);
	public void drawLine(Point p_pointA, Point p_pointB, int color, int p_strokeSize);
	public void drawLine(Point p_pointA, Point p_pointB, int color); 

	public void drawRect(Rect r, int color);
	public void drawRect(ButtonData p_btnData, int color);
	public void drawRect(ImageData p_imgData, int color);
	public void drawRect(int x, int y, int width, int height, int color); 
	public void drawRoundRect(int x, int y, int width, int height, int roundX, int roundY, int color);

	public void drawImage(Bitmap Image, int x, int y);
	public void drawImage(Image Image, int x, int y); 
	public void drawImage(Image Image, float  x, int y);
	public void drawImage(Image Image, int  x, float y);
	public void drawImage(Image Image, float  x, float y);
	public void drawImage(Image Image, int x, int y, int rotation); 
	public void drawImage(Bitmap Image, int x, int y,int angle);
	public void drawImage(Bitmap Image, int x, int y, float p_scale);
	public void drawImage(Image image,  ImageData p_imgData );
	public void drawImage(Image image, int p_addX,  int p_addY, ImageData p_imgData ); 
	public void drawImage(Image image, float p_addX,  float p_addY, ImageData p_imgData);
	public void drawImage(Image image, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight);
	public void drawImage(Image image, float p_addX,  float p_addY, ImageData p_imgData, int p_rotate);
	public void drawImage(Image image, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight,int color);

	public void drawScaledImage(Image Image, int  x, int y, float p_scale);
	public void drawScaledImage(Image p_image, int  x, int y,int  p_width, int p_height, int p_srcX, int p_srcY, float p_scale);
	public void drawScaledImage(Image Image, int x, int y, int width, int height, int srcX, int srcY, int srcWidth, int srcHeight,float  p_scale);
	public void drawStretchedImage(Image p_image, int x, int y, int p_width, int p_height, int p_srcX, int p_srcY,int p_srcWidth, int p_srcHeight);

	public void drawARGB(int i, int j, int k, int l); 
	
	public void drawString(String text, int x, int y, Paint paint);
	public void drawStringFont(String text, int x, int y, Typeface p_typeface, Paint paint); 
	public void drawStringFont(LanguageManager.W p_id, int x, int y, Typeface p_typeface, Paint paint);
	public void drawStringFont(LanguageManager.W p_id, float x, float y, Typeface p_typeface, Paint paint);
	public void drawString(String text, Point p_pos, Paint paint);
	public void drawString(String text, int x, int y, int c, Paint paint); 
	 
	public void drawString(String text, Rect p_rect, Paint paint); 
    public void drawString(String text, Rect p_rect, int p_addX, Paint paint); 
	public void drawImage(Image image, int p_addX,  int p_addY, ImageData p_imgData , float p_scaleX, float p_scaleY);
	public void drawImage(Image Image, int x, int y, int srcX, int srcY,  int srcWidth, int srcHeight, float p_scaleX, float p_scaleY);
	public void drawRoundRect(Rect r, int roundX, int roundY, int color);  
	public void drawRoundRect(int p_addX,Rect r, int roundX, int roundY, int color);  
 	public void drawScaledImage(Image Image, int x, int y, int width, int height, int srcX, int srcY, int srcWidth, int srcHeight, float p_scaleX, float p_scaleY);
  
}
