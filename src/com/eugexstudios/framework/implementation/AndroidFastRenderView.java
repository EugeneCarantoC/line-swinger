package com.eugexstudios.framework.implementation;
  
import java.lang.reflect.Field; 
 

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class AndroidFastRenderView extends SurfaceView implements Runnable {
    AndroidGame 			game;
    Bitmap 					framebuffer;
    Thread 				    renderThread = null;
    SurfaceHolder			holder;
    volatile boolean running = false;
    
    private void disableSurfaceViewLogging() 
    {
        try
        {
            Field field = SurfaceView.class.getDeclaredField("DEBUG");
            field.setAccessible(true);
            field.set(null, false); 
        }
        catch (Exception e)
        { 
        }
    }
    public AndroidFastRenderView(AndroidGame game, Bitmap framebuffer) {
        super(game);
        this.game = game;
        this.framebuffer = framebuffer;
        this.holder = getHolder();   
        disableSurfaceViewLogging();
    }
 
    public void resume() 
    { 
        running = true;
        renderThread = new Thread(this);
        renderThread.start();     
    }
//    public void run() 
//    {
//    	final float DELTAL_IMIT=  4.15f;
//        Rect dstRect = new Rect();
//        float startTime = System.nanoTime();  
//        float lastDeltaTime=0;   
//        
//        while(running)
//        {
//        	if(!holder.getSurface().isValid())
//        		continue;
//        	float l_currentTime = System.nanoTime();
//        	float deltaTime = (l_currentTime - startTime) / 10000000.000f; //orig from kilobolt 
//        	  m_fps=(int)(deltaTime*100);                                            //1000000
//            startTime = l_currentTime;
//            
//            if(deltaTime==0)
//            {
//            	deltaTime=lastDeltaTime;
//            }
//            else
//            {
//            	if(deltaTime>DELTAL_IMIT)
//                {
//            		deltaTime= DELTAL_IMIT;
//                }
//            	lastDeltaTime=deltaTime;
//            }
//            
//            try
//            {
//            	game.getCurrentScreen().update(deltaTime);
//	            game.getCurrentScreen().paint(deltaTime);
//            }
//            catch(Exception e)
//            { 
////            	genSet.ShowToast("Error in AndroidFastRenderView: " + e.getMessage());
//            }
//            
//            Canvas canvas = holder.lockCanvas();
//            canvas.getClipBounds(dstRect);
//            canvas.drawBitmap(framebuffer, null, dstRect, null);                           
//            holder.unlockCanvasAndPost(canvas); 
//        }
//     } 
  public void run() 
  { 
      Rect dstRect = new Rect();
      long startTime = System.currentTimeMillis(); 
      m_fps =0;
      while(running)
      {
      	if(!holder.getSurface().isValid())
      	{
      		continue;
      	}

          startTime = System.currentTimeMillis();
        try
        {
//            game.getCurrentScreen().update(2.55f); // stable
            game.getCurrentScreen().update(1.0f); //1.67//1.87 //2.80f
	      game.getCurrentScreen().paint(2.55f); //1.67//1.87
        }
        catch(Exception e)
        { 
//        	genSet.ShowToast("Error in AndroidFastRenderView: " + e.getMessage());
        }
          Canvas canvas = holder.lockCanvas();
          canvas.getClipBounds(dstRect);
          canvas.drawBitmap(framebuffer, null, dstRect, null);                           
          holder.unlockCanvasAndPost(canvas); 
           
          doFpsCheck(startTime);
      }
   }

    public void pause() {                        
        running = false;                        
        while(true) {
            try {
                renderThread.join();
                break;
            } catch (InterruptedException e) {
                // retry 
            }
            
        }
    }     
    
    private static final long SECOND = 1000;
    private static final long TARGET_FPS = 60;
    private static final long FRAME_PERIOD = SECOND / TARGET_FPS;
 
    /**
     * 
     * @param startTime
     * @return <code>true</code> if the interval between startTime and the time
     *         when this method was called is smaller or equal to the given
     *         frame period.
     * 
     *         Will return <code>false</code> if the interval was longer.
     */
    public boolean doFpsCheck(long startTime)
    {
  
        long sleepTime = FRAME_PERIOD- (System.currentTimeMillis() - startTime);
        m_fps = (int)(((float)(sleepTime)/(float)(FRAME_PERIOD))*TARGET_FPS);
        if (sleepTime > 0) {
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) { 
//            	genSet.ShowToast(e.getMessage());
            }
            return true;
        } else {
            return false;
        }
    }
    int m_fps;
    public int getFPS()
    {
    	return m_fps;
    }
}