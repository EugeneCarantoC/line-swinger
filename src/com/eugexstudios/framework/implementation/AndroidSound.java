package com.eugexstudios.framework.implementation;
  
import android.media.SoundPool;  

import com.eugexstudios.framework.Sound;
import com.eugexstudios.lineswinger.GameScreen;

public class AndroidSound implements Sound {
    int soundId;
    SoundPool soundPool;
    
    public AndroidSound(SoundPool soundPool, int soundId) {
        this.soundId = soundId;
        this.soundPool = soundPool; 
    }

    @Override
    public void play(float volume) {
    	if(GameScreen.IsSoundOn())
    	{
            soundPool.play(soundId, volume, volume, 0, 0, 1);
    	}
    }
    @Override
    public void play() {
    	if(GameScreen.IsSoundOn())
    	{
            soundPool.play(soundId, 1f, 1f, 0, 0, 1);
    	}
    }

    @Override
    public void dispose() {
        soundPool.unload(soundId);
    }

}
