package com.eugexstudios.framework.implementation;

import java.io.IOException;
import java.io.InputStream; 

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;  
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction; 
import android.graphics.Point;
import android.graphics.Rect; 
import android.graphics.RectF;
import android.graphics.Typeface;

import com.eugexstudios.framework.ButtonData;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Image;
import com.eugexstudios.framework.ImageData;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.LanguageManager;

public class AndroidGraphics implements Graphics {
    AssetManager assets;
    Bitmap frameBuffer;
    Canvas canvas;
    Paint paint; 
    Paint m_savedImage;
    Rect srcRect = new Rect();
    Rect dstRect = new Rect();
    Matrix matrix = new Matrix();
    public AndroidGraphics(AssetManager assets, Bitmap frameBuffer) {
        this.assets = assets;
        this.frameBuffer = frameBuffer;
        this.canvas = new Canvas(frameBuffer);
        this.paint = new Paint();
        
    }

    @Override
    public int getWidth() {
        return frameBuffer.getWidth();
    } 
    public int getHeight() {
        return frameBuffer.getHeight();
    }
    public Canvas getCanvas(){ 
       return  canvas;
	}
	public AssetManager getAssetManager()
	{
		return assets;
	}
    public Image newImage(String fileName, ImageFormat format) {
        Config config = null;
        if (format == ImageFormat.RGB565)
            config = Config.RGB_565;
        else if (format == ImageFormat.ARGB4444)
            config = Config.ARGB_4444;
        else
            config = Config.ARGB_8888;  
        Options options = new Options();
        options.inPreferredConfig = config;
         
        InputStream in = null;
        Bitmap bitmap = null;
        try {
            in = assets.open(fileName);
            bitmap = BitmapFactory.decodeStream(in, null, options);
            if (bitmap == null)
                throw new RuntimeException("Couldn't load bitmap from asset '"   + fileName + "'");
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load bitmap from asset '"   + fileName + "'");
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }
       
        if (bitmap.getConfig() == Config.RGB_565)
            format = ImageFormat.RGB565;
        else if (bitmap.getConfig() == Config.ARGB_4444)
            format = ImageFormat.ARGB4444;
        else
            format = ImageFormat.ARGB8888; 
        
        return new AndroidImage(bitmap, format);
    }
    public Bitmap newBitmap(String fileName, ImageFormat format)
  	{
      	 Config config = null;
           if (format == ImageFormat.RGB565)
               config = Config.RGB_565;
           else if (format == ImageFormat.ARGB4444)
               config = Config.ARGB_4444;
           else
               config = Config.ARGB_8888;

           Options options = new Options();
           options.inPreferredConfig = config;
            
           InputStream in = null;
           Bitmap bitmap = null;
           try {
               in = assets.open(fileName);
               bitmap = BitmapFactory.decodeStream(in, null, options);
               if (bitmap == null)
                   throw new RuntimeException("Couldn't load bitmap from asset '"   + fileName + "'");
           } catch (IOException e) {
               throw new RuntimeException("Couldn't load bitmap from asset '"   + fileName + "'");
           } finally {
               if (in != null) {
                   try {
                       in.close();
                   } catch (IOException e) {
                   }
               }
           } 
           return bitmap;
  	}
    
    @Override
    public void clearScreen(int color) {
        canvas.drawRGB((color & 0xff0000) >> 16, (color & 0xff00) >> 8,
                (color & 0xff));
    }
    @Override
    public void BlurEverything()
    { 
        paint.setMaskFilter(new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL));
    }    
    @Override
    public void RemoveBlur()
    { 
        paint.setMaskFilter(null);
    }
    @Override
    public void SaveState()
    {  
    	m_savedImage= paint; 
    	 
    } 
    @Override
    public void PaintSavedState()
    {    
    	paint.set(m_savedImage);
    }

    @Override
    public void drawCircle(int x, int y, int radius, int color) {
        paint.setColor(color);
        canvas.drawCircle(x, y, radius, paint);
    } 
    @Override
    public void drawCircle(Point p_loc, int radius, int color) {
        paint.setColor(color);
        canvas.drawCircle(p_loc.x, p_loc.y, radius, paint);
    } 
  
    public void drawArc(int p_x, int p_y, int p_radius,  int p_sweepStart,int p_sweepAngle, int p_color)  {
        paint.setColor(p_color);
        RectF oval = new RectF(p_x-p_radius,p_y-p_radius,p_x+p_radius,p_y+p_radius); 
        canvas.drawArc(oval, p_sweepStart, p_sweepAngle, true, paint); 
    }
    public  void drawMaskedArc(int p_x, int p_y, int p_radius,int p_endRadius,  int p_sweepStart,int p_arcAngle, int p_color)
    { 
        Path largePath = new Path(); 
        largePath.addCircle(p_x, p_y,p_endRadius,Direction.CW);
        RectF oval = new RectF(p_x-p_endRadius,p_y-p_endRadius,p_x+p_endRadius,p_y+p_endRadius); 
        largePath.addArc(oval,p_sweepStart,p_arcAngle);  
        
        canvas.clipPath(largePath,android.graphics.Region.Op.DIFFERENCE);
        RectF oval2 = new RectF(p_x-p_radius,p_y-p_radius,p_x+p_radius,p_y+p_radius); 
        
        paint.setColor(p_color); 
        canvas.drawArc(oval2, p_sweepStart, p_arcAngle, true, paint);  
        canvas.clipRect(Constants.SCREEN_RECT,android.graphics.Region.Op.UNION); 
    }
    
    @Override
    public void drawLine(int x, int y, int x2, int y2, int color) {
        paint.setColor(color);
        canvas.drawLine(x, y, x2, y2, paint);
    } 
    @Override
    public void drawLine(int x, int y, int x2, int y2, int color, int p_strokeSize) {
        paint.setColor(color);
        paint.setStrokeWidth(p_strokeSize);
        canvas.drawLine(x, y, x2, y2, paint);
    }    
    @Override
    public void drawLine(Point p_pointA, Point p_pointB, int color, int p_strokeSize) {
        paint.setColor(color);
        paint.setStrokeWidth(p_strokeSize);
        canvas.drawLine(p_pointA.x, p_pointA.y, p_pointB.x, p_pointB.y, paint);
    } 
    @Override
    public void drawLine(Point p_pointA, Point p_pointB,  int color) {
        paint.setColor(color);
        canvas.drawLine(p_pointA.x, p_pointA.y, p_pointB.x, p_pointB.y, paint);
    }
     
	@Override
	public void drawRect(Rect r, int color) {
		// TODO Auto-generated method stub

        paint.setColor(color);
        paint.setStyle(Style.FILL);
        canvas.drawRect(r, paint);
	}
	@Override
	public void drawRect(ButtonData p_btnData,  int color)
	{ 
        paint.setColor(color);
        paint.setStyle(Style.FILL);
        canvas.drawRect(p_btnData.rect, paint);
	}
    @Override
	public void drawRect(ImageData p_imgData, int color) {
		// TODO Auto-generated method stub

        paint.setColor(color);
        paint.setStyle(Style.FILL);
        canvas.drawRect(p_imgData.rect, paint);
	}
	@Override
    public void drawRect(int x, int y, int width, int height, int color) {
        paint.setColor(color);
        paint.setStyle(Style.FILL);
        canvas.drawRect(x, y, x + width , y + height, paint);
    }
    @Override
    public void drawRoundRect(int x, int y, int width, int height, int roundX, int roundY,int color) {
        paint.setColor(color);
        paint.setStyle(Style.FILL);  

		 canvas.drawRoundRect(new RectF(x,y,x+width,y+height), (float) roundX, (float)roundY, paint);
 
    }
   
	@Override
	public void drawImage(Bitmap Image, int x, int y) {  
		canvas.drawBitmap(Image, x, y, null); 
	}	
	@Override
    public void drawImage(Image Image, int x, int y) {
        canvas.drawBitmap(((AndroidImage)Image).bitmap, x, y, null);
    }
    @Override
	public void drawImage(Image Image, float x, int y) {
        canvas.drawBitmap(((AndroidImage)Image).bitmap, x, y, null);
	}
	@Override
	public void drawImage(Image Image, int x, float y) {
		// TODO Auto-generated method stub
        canvas.drawBitmap(((AndroidImage)Image).bitmap, x, y, null);
	}
    @Override
	public void drawImage(Image Image, float x, float y) {
		// TODO Auto-generated method stub
        canvas.drawBitmap(((AndroidImage)Image).bitmap, x, y, null);
	 } 
    @Override
    public void drawImage(Image Image, int x, int y,int rotation) {
    	
		canvas.drawBitmap(RotateBitmap(((AndroidImage)Image).bitmap,rotation), x, y, null);
    }
    @Override
	public void drawImage(Bitmap p_image, int x, int y,int angle) { 
		Matrix matrix = new Matrix();
		matrix.setRotate(angle, x, y);
		canvas.drawBitmap(p_image, matrix, null); 
	}
	@Override
	public void drawImage(Bitmap Image, int x, int y, float p_scale)  {  
	 
		 canvas.drawBitmap( Bitmap.createScaledBitmap(Image, (int)(Image.getWidth()*p_scale), (int)(Image.getHeight()*p_scale), false), x, y, null);  
			 
	}
	public void drawImage(Image image, ImageData p_imgData )
	{
		drawImage( image,p_imgData.x,p_imgData.y,p_imgData.src_x, p_imgData.src_y,p_imgData.width,p_imgData.height);

	}
    public void drawImage(Image image, int p_addX, int p_addY, ImageData p_imgData )
	{
    	drawImage( image,p_imgData.x+p_addX,p_imgData.y+p_addY,p_imgData.src_x, p_imgData.src_y,p_imgData.width,p_imgData.height);
	}
    public void drawImage(Image image, float p_addX, float p_addY, ImageData p_imgData )
	{
		drawImage( image, (int)(p_imgData.x +p_addX),(int)(p_imgData.y+p_addY),p_imgData.src_x, p_imgData.src_y,p_imgData.width,p_imgData.height);
	}
    public void drawImage(Image Image, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight) {
        srcRect.left = srcX;
        srcRect.top = srcY;
        srcRect.right = srcX + srcWidth;
        srcRect.bottom = srcY + srcHeight;
        
        
        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = x + srcWidth;
        dstRect.bottom = y + srcHeight;
        canvas.drawBitmap(((AndroidImage) Image).bitmap, srcRect, dstRect,   null); 
    }
    @Override
    public void drawImage(Image Image, float x, float y, ImageData p_imgData, int p_rotation) { 
		canvas.drawBitmap(RotateBitmapz(((AndroidImage)Image).bitmap,p_imgData, p_rotation), x, y, null);
    }
	public void drawImage(Image Image, int x, int y, int srcX, int srcY, int srcWidth, int srcHeight, int p_color) {
        srcRect.left = srcX;
        srcRect.top = srcY;
        srcRect.right = srcX + srcWidth;
        srcRect.bottom = srcY + srcHeight;
        
        
        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = x + srcWidth;
        dstRect.bottom = y + srcHeight;
        canvas.drawBitmap(Image.colorize(p_color), srcRect, dstRect,   null); 
    }
	
	public void drawScaledImage(Image p_image, int  x, int y,float p_scale)
	{
		srcRect.left = 0;
        srcRect.top = 0;
        srcRect.right = p_image.getWidth();
        srcRect.bottom = p_image.getHeight();
         
        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = (int) (x + (p_image.getWidth()*p_scale));
        dstRect.bottom = (int) (y + (p_image.getHeight()*p_scale));
        
        canvas.drawBitmap(((AndroidImage) p_image).bitmap, srcRect, dstRect, null);
	}
	public void drawScaledImage(Image p_image, int  x, int y,int  p_width, int p_height, int p_srcX, int p_srcY, float p_scale)
	{
		srcRect.left = p_srcX;
        srcRect.top = p_srcY;
        srcRect.right = p_width;
        srcRect.bottom = p_height;
         
        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = (int) (x + ((float)(p_width*p_scale)));
        dstRect.bottom = (int) (y + ((float)(p_height*p_scale)));
        
        canvas.drawBitmap(((AndroidImage) p_image).bitmap, srcRect, dstRect, null);
	}
	public void drawScaledImage(Image Image, int x, int y, int width, int height, int srcX, int srcY, int srcWidth, int srcHeight,float p_scale){
	    	
	    	srcRect.left = srcX;
	        srcRect.top = srcY;
	        srcRect.right = srcX + srcWidth ;
	        srcRect.bottom = srcY + srcHeight;
	         
	        dstRect.left = x;
	        dstRect.top = y;
	        dstRect.right = x + (int)(width*p_scale);
	        dstRect.bottom = y + (int)(height*p_scale);
	        
	        canvas.drawBitmap(((AndroidImage) Image).bitmap, srcRect, dstRect, null);
	    }
	@Override
	public void drawStretchedImage(Image p_image, int x, int y, int p_width, int p_height, int p_srcX, int p_srcY,int p_srcWidth, int p_srcHeight ) {
		
		srcRect.left = p_srcX;
        srcRect.top = p_srcY;
        srcRect.right = p_srcX + p_srcWidth ;
        srcRect.bottom = p_srcY + p_srcHeight;
         
        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = x + p_width;
        dstRect.bottom = y + p_height;
        
        canvas.drawBitmap(((AndroidImage) p_image).bitmap, srcRect, dstRect, null);
	 }
	
	@Override
    public void drawARGB(int a, int r, int g, int b) {
        paint.setStyle(Style.FILL);
        canvas.drawARGB(a, r, g, b); 
    } 

    @Override
    public void drawString(String text, int x, int y, Paint paint){
    	canvas.drawText(text, x, y, paint); 
    }    
	@Override
	public void drawStringFont(String text, int x, int y, Typeface p_font, Paint paint) 
	{ 
    	paint.setTypeface(p_font);  
		canvas.drawText(text, x, y, paint); 
	} 
	@Override
	public void drawStringFont(LanguageManager.W p_id, int x, int y, Typeface p_font, Paint paint)
	{  
    	paint.setTypeface(p_font); 
		canvas.drawText(LanguageManager.GetLocalized(p_id), x, y, paint); 
	} 
    @Override
	public void drawStringFont(LanguageManager.W p_id, float x, float y, Typeface p_typeface, Paint paint)
	{ 
		 drawStringFont( p_id,  x, y,  p_typeface, paint);
	}
    @Override
    public void drawString(String text, Point p_pos, Paint paint){
    	canvas.drawText(text, p_pos.x, p_pos.y, paint);
 
    } 
   @Override
	public void drawString(String text, int x, int y, int c, Paint paint) {
		// TODO Auto-generated method stub 
		paint.setColor(c);
	    canvas.drawText(text, x, y, paint);
	  
	}
 
    @Override
    public void drawString(String text, Rect p_rect, Paint paint){
    	
    	canvas.drawText(text, p_rect.left+ p_rect.width()/2, p_rect.top+ p_rect.height()/2+ paint.getTextSize()/3, paint);
    } 
    @Override
    public void drawString(String text, Rect p_rect, int p_addX, Paint paint){
    	
    	canvas.drawText(text, p_rect.left+ p_rect.width()/2 + p_addX, p_rect.top+ p_rect.height()/2+ paint.getTextSize()/3, paint);
  
    } 
	@Override
	public void drawImage(Image image, int p_addX,  int p_addY, ImageData p_imgData , float p_scaleX, float p_scaleY)
	{	 
		drawScaledImage(image , p_imgData.x+p_addX,p_imgData.y+p_addY, p_imgData.width,p_imgData.height,p_imgData.src_x, p_imgData.src_y,p_imgData.width,p_imgData.height,   p_scaleX,  p_scaleY);
	 }
	public void drawImage(Image Image, int x, int y, int srcX, int srcY,   int srcWidth, int srcHeight,float p_scaleX, float p_scaleY) {
        srcRect.left = srcX;
        srcRect.top = srcY;
        srcRect.right = srcX + srcWidth;
        srcRect.bottom = srcY + srcHeight;
        
        
        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = x + (int)(srcWidth*p_scaleX);
        dstRect.bottom = y + (int)(srcHeight*p_scaleY);
        canvas.drawBitmap(((AndroidImage) Image).bitmap, srcRect, dstRect,   null); 
    }
	@Override
    public void drawRoundRect(Rect r, int roundX, int roundY, int color)
    {
        paint.setColor(color);
        paint.setStyle(Style.FILL);  
        canvas.drawRoundRect(new RectF(r), (float) roundX, (float)roundY, paint); 
    }
	@Override
	public void drawRoundRect(int p_addX ,Rect r, int roundX, int roundY, int color)
    {
        paint.setColor(color);
        paint.setStyle(Style.FILL);  
        canvas.drawRoundRect(new RectF(p_addX+r.left,r.top, p_addX+r.right, r.bottom), (float) roundX, (float)roundY, paint); 
    }
	public void drawScaledImage(Image Image, int x, int y, int width, int height, int srcX, int srcY, int srcWidth, int srcHeight, float p_scaleX,float p_scaleY){
    	
    	srcRect.left = srcX;
        srcRect.top = srcY;
        srcRect.right = srcX + srcWidth ;
        srcRect.bottom = srcY + srcHeight;
         
        dstRect.left = x;
        dstRect.top = y;
        dstRect.right = x + (int)(width*p_scaleX);
        dstRect.bottom = y + (int)(height*p_scaleY);
        
        canvas.drawBitmap(((AndroidImage) Image).bitmap, srcRect, dstRect, null);
    }
 
	//LOCAL METHODS
	private static Bitmap RotateBitmap(Bitmap source, int angle)
	{
	      Matrix matrix = new Matrix();
	      matrix.postRotate(angle);
	      return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	}
	private static Bitmap RotateBitmapz(Bitmap source, ImageData p_imageData, int angle)
	{
	      Matrix matrix = new Matrix();  
	      matrix.setRotate(angle);
//	      matrix.setRotate(angle,p_imageData.width/2, p_imageData.height/2); 
	      return Bitmap.createBitmap(source, p_imageData.src_x, p_imageData.src_y,p_imageData.width, p_imageData.height, matrix, true);
	}
	 
}
