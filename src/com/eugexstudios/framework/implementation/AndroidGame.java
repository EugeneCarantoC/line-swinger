package com.eugexstudios.framework.implementation;
     
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;  
import android.app.Activity;  
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;   
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;  
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager; 
import android.os.PowerManager.WakeLock; 
import android.os.Vibrator;  
import android.view.Display;
import android.view.View;
import android.view.ViewManager;
import android.view.Window;
import android.view.WindowManager;     
import android.widget.RelativeLayout;
import android.widget.Toast;
    






import com.eugexstudios.adprovider.StartApp;
import com.eugexstudios.adprovider.StartApp.VideoAdType;
import com.eugexstudios.lineswinger.R;
import com.eugexstudios.framework.Audio;
import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.FileIO;
import com.eugexstudios.framework.Game;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input;
import com.eugexstudios.framework.Screen;
import com.eugexstudios.lineswinger.GameScreen;
import com.eugexstudios.lineswinger.MenuScreen; 
import com.eugexstudios.lineswinger.GameScreen.LANGUAGE; 
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Initializer; 
import com.eugexstudios.utils.SharedPref;
import com.eugexstudios.utils.genSet;  
import com.startapp.android.publish.ads.banner.Banner;
import com.startapp.android.publish.ads.banner.BannerListener; 
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;
@SuppressLint("NewApi")
public abstract class AndroidGame extends Activity implements Game  
{  
	private AndroidFastRenderView 	   m_renderView;
    private Graphics 				   m_graphics;
    private Context 				   m_gameContext;
    private Audio 					   m_audio;
    private Input 					   m_input;
    private FileIO 					   m_fileIO;
    private Screen 				       m_screen; 
    private Vibrator 				   m_vibrator;
    private WakeLock				   m_wakeLock;  
    private StartApp				   m_startApp;   
    private Banner                     m_startAppBannerAd;
    private boolean 	               m_hasAds;
    private static  ProgressDialog     m_dialog; 
 
   @Override
    public void onCreate(Bundle p_savedInstanceState) 
    { 
        super.onCreate(p_savedInstanceState);  
        m_startApp = new StartApp(p_savedInstanceState,this, getBaseContext());  
      
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  WindowManager.LayoutParams.FLAG_FULLSCREEN);

        m_gameContext	 = getBaseContext(); 
        Point CurrentDisplaySize= getDisplaySize();  
//      Constants.SetScreenConfig(CurrentDisplaySize.x,CurrentDisplaySize.y);  
      Constants.SetScreenConfig(720,1280);  

        //############ SIZE B ##############################
//      Constants.SetScreenConfig(480,800); 
//      Constants.SetScreenConfig(480,854);  
        
        //############ SIZE C ##############################
//      Constants.SetScreenConfig(540,960); 
        
//        ############ SIZE E ##############################
//      Constants.SetScreenConfig(1080,1920); 
         
        boolean l_isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;  
           
        float l_frameBufferWidth  = l_isPortrait ? CONFIG.SCREEN_WIDTH: CONFIG.SCREEN_HEIGHT;
        float l_frameBufferHeight = l_isPortrait ? CONFIG.SCREEN_HEIGHT: CONFIG.SCREEN_HEIGHT; 
        float l_scaleX = ((float)(l_frameBufferWidth/CurrentDisplaySize.x));
     	float l_scaleY = ((float)(l_frameBufferHeight/CurrentDisplaySize.y));  
     	
        Bitmap l_frameBuffer      = Bitmap.createBitmap((int)l_frameBufferWidth, (int) l_frameBufferHeight, Config.RGB_565);

        m_renderView	 = new AndroidFastRenderView(this, l_frameBuffer); 
        m_graphics 		 = new AndroidGraphics(getAssets(), l_frameBuffer);
        m_fileIO 		 = new AndroidFileIO(this);
        m_audio 		 = new AndroidAudio(this);
        m_screen 		 = getInitScreen();
        m_wakeLock       = ((PowerManager)getSystemService(Context.POWER_SERVICE)).newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Color wheel");
        m_vibrator       = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);  
 
        HideNavigBar();   
     	 
        m_input			 = new AndroidInput(this, m_renderView, l_scaleX, l_scaleY);

        setContentView(m_renderView);
      
        if(!GetSharedPrefBoolean(SharedPref.SHORTCUT_CREATED))
        { 
        	addShortcut(); 
        	SetSharedPrefBoolean(SharedPref.SHORTCUT_CREATED, true);
        }  
        
        m_hasAds= GetSharedPrefInt(SharedPref.APP_OPENED_CTR)>2;
        LoadBannerAd();    
         
    } 
   public int getFPS()
   {
	   return m_renderView.getFPS();
   }
   public void ShowDialog(final String p_str)
   {  
	  final ProgressDialog  l_dialog = new ProgressDialog( AndroidGame.this  ); 
	  l_dialog.setIndeterminate(false);
	  l_dialog.show(); 
//       m_dialog = ProgressDialog.show( this, "",    "sdf" , false);  
   		 
   }
   @Override
   public void HideDialog()
   {
	   m_dialog.hide();
   }
   public void getEmailAddress()
   {
	     AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
	     Account[] list = manager.getAccounts();
	     String gmail = null;
	
	     for(Account account: list)
	     { 
	         if(account.type.equalsIgnoreCase("com.google"))
	         {
	             gmail = account.name;
	             break;
	         }
	     }
	     showToast(gmail); 
   }
   
   public void LoadBannerAd()
   {
       m_startAppBannerAd = new Banner(getApplicationContext()
       		, 
       		new BannerListener() {
		        	@Override
		        	public void onReceiveAd(View banner) { 
		        	}
		        	@Override
		        	public void onFailedToReceiveAd(View banner) { 
		        	}
		        	@Override
		        	public void onClick(View banner) {
		        	}
       }
  );
       RelativeLayout.LayoutParams bannerParameters =
       			new RelativeLayout.LayoutParams(
       					RelativeLayout.LayoutParams.MATCH_PARENT,
       					RelativeLayout.LayoutParams.WRAP_CONTENT);
         
       // Add to main Layout
       View currentView = this.getWindow().getDecorView().findViewById(android.R.id.content); 
       ((ViewManager) currentView).addView(m_startAppBannerAd, bannerParameters); 
   }
   public boolean IsBannerAdEnabled()
   {
	   return m_startAppBannerAd.isEnabled()&&m_startAppBannerAd.isLaidOut() && m_hasAds;
   }
   public int GetBannerHeight()
   {
	   return m_startAppBannerAd.getHeight();
   }
   public void ShowBannerAd()
   {
		runOnUiThread(new Runnable()
    	{
    		public void run()
    		{
    		    m_startAppBannerAd.showBanner();
    		}
    		});
   }
   
   public void HideBannerAd()
   {

		runOnUiThread(new Runnable()
    	{
    		public void run()
    		{
    		    m_startAppBannerAd.hideBanner();
    		}
    		});
   }
   
   public void GoToPublisherPage()
   {
	   String PublisherPage = "http://bit.ly/EugexStudios";
	    Uri uri = Uri.parse(PublisherPage);
	    Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
	    try {
	        startActivity(myAppLinkToMarket);
	    } catch (ActivityNotFoundException e) {
	        Toast.makeText(this, " Unable to publisher in the market.", Toast.LENGTH_LONG).show();
	    }
   }
   public void GoToPlayStoreLink()
   {
	   
	    Uri uri = Uri.parse("market://details?id=" + getPackageName());
	    Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
	    try {
	        startActivity(myAppLinkToMarket);
	    } catch (ActivityNotFoundException e) {
	        Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
	    }
	}
   
   public String GetVersionName() 
   { 
       try {
		return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
	} catch (NameNotFoundException e) { 
		e.printStackTrace();
	}
       return "";
   } 
   private void addShortcut() 
   { 
	   
	    Intent shortcutIntent = new Intent(getApplicationContext(),  Initializer.class);

	    shortcutIntent.setAction(Intent.ACTION_MAIN);

	    Intent addIntent = new Intent();
	    addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
	    addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "Line Swinger");
	    addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
	            Intent.ShortcutIconResource.fromContext(getApplicationContext(),
	                    R.drawable.hi_res_icon));

	    addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
	    addIntent.putExtra("duplicate", false);  //may it's already there so don't duplicate
	    getApplicationContext().sendBroadcast(addIntent);
	}
    
   @SuppressLint("NewApi")
private Point getDisplaySize()
   {
       WindowManager wm = (WindowManager)    m_gameContext.getSystemService(Context.WINDOW_SERVICE);
       Display display = wm.getDefaultDisplay();
       Point size = new Point();
       display.getRealSize(size); 
     return size;
   }
   
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void addListen()
    {
    	 
        View decorView = getWindow().getDecorView();
         
        decorView.setOnFocusChangeListener(
        		
        		new View.OnFocusChangeListener() {
					
					@Override
					public void onFocusChange(View arg0, boolean arg1)
					{ 
						if(arg1)
						{
//							showToast("onFocusChange: " + arg1);
						}
					}
				}  
        		
        		);

        View decorView2 = getWindow().getDecorView();
        decorView2.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                // Note that system bars will only be "visible" if none of the
                // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    // The system bars are visible. Make any desired
                    // adjustments to your UI, such as showing the action bar or
                    // other navigational controls.
//                    showToast("onSystemUiVisibilityChange: 2");
                } else {
                    // The system bars are NOT visible. Make any desired
                    // adjustments to your UI, such as hiding the action bar or
                    // other navigational controls.
//                    showToast("onSystemUiVisibilityChange: 1");
 
                }
            }
        });
    } 
   
    public void ShareApp(int p_score)
    {
    	try { 
    	    Intent i = new Intent(Intent.ACTION_SEND);  
    	    i.setType("text/plain");
    	    i.putExtra(Intent.EXTRA_SUBJECT, "Try Line Swinger!"); 
    	    String sAux = "";
    	    if(p_score>0)
    	    { 
    	    	sAux = "I just scored " +p_score + " on Line Swinger!\n\n";   
    	    }
    	    else
    	    {
    	    	sAux = "Try Line Swinger!";
    	    }
    	    sAux = sAux + "https://play.google.com/store/apps/details?id=com.eugexstudios.lineswinger"  + (GameScreen.GetLanguage()!=LANGUAGE.ENGLISH? "&hl=" + GameScreen.GetLanguageCode():"");
    	    i.putExtra(Intent.EXTRA_TEXT, sAux);  
    	    startActivity(Intent.createChooser(i, "Choose one"));
    	} catch(Exception e) {  
    	}   
    }
    
   
    public void SendEmailFeedback()
    {
    	Intent feedbackEmail = new Intent(Intent.ACTION_SEND);

    	feedbackEmail.setType("text/email");
    	feedbackEmail.putExtra(Intent.EXTRA_EMAIL, new String[] {"admin@eugexstudios.com"});
    	feedbackEmail.putExtra(Intent.EXTRA_SUBJECT, "Feedback for Color Twirl");
    	startActivity(Intent.createChooser(feedbackEmail, "Send Feedback:"));
    }
    private String getFacebookPageURL()
    {
    	 String FACEBOOK_URL = "https://www.facebook.com/EugexStudios";
         String FACEBOOK_PAGE_ID = "EugexStudios";

       	//method to get the right URL to use in the intent 
       	        PackageManager packageManager = this.getApplicationContext().getPackageManager();
       	        try {
       	            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
       	            if (versionCode >= 3002850) { //newer versions of fb app
       	                return "fb://facewebmodal/f?href=" + FACEBOOK_URL;
       	            } else { //older versions of fb app
       	                return "fb://page/" + FACEBOOK_PAGE_ID;
       	            }
       	        } catch (PackageManager.NameNotFoundException e) {
       	            return FACEBOOK_URL; //normal web url
       	        }  
    }
    public void OpenFacebookPage()
    {
    	Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
    	String facebookUrl = getFacebookPageURL();
    	facebookIntent.setData(Uri.parse(facebookUrl));
    	startActivity(facebookIntent); 
    }
     
     
    
   @Override
   public void SetSharedPrefString(String p_prefName, String p_val)
   { 
       SharedPreferences sharedpreferences = getSharedPreferences(p_prefName, Context.MODE_PRIVATE);
       SharedPreferences.Editor editor = sharedpreferences.edit(); 
       editor.putString(p_prefName, p_val);
       editor.commit(); 
   }
   
   @Override
   public void SetSharedPrefBoolean(String p_prefName, boolean p_val)
   { 
       SharedPreferences sharedpreferences = getSharedPreferences(p_prefName, Context.MODE_PRIVATE);
       SharedPreferences.Editor editor = sharedpreferences.edit(); 
       editor.putBoolean(p_prefName, p_val);
       editor.commit(); 
   }
 
   @Override
   public void SetSharedPrefInt(String p_prefName, int p_val)
   { 
       SharedPreferences sharedpreferences = getSharedPreferences(p_prefName, Context.MODE_PRIVATE);
       SharedPreferences.Editor editor = sharedpreferences.edit(); 
       editor.putInt(p_prefName, p_val);
       editor.commit(); 
   }
   @Override
   public int GetSharedPrefInt(String p_prefName)
   { 
       SharedPreferences sharedpreferences = getSharedPreferences(p_prefName, Context.MODE_PRIVATE); 
       return sharedpreferences.getInt(p_prefName, -1);
   }
   @Override
   public String GetSharedPrefString(String p_prefName)
   { 
       SharedPreferences sharedpreferences = getSharedPreferences(p_prefName, Context.MODE_PRIVATE); 
       return sharedpreferences.getString(p_prefName, "");
   }
   
   @Override
   public boolean GetSharedPrefBoolean(String p_prefName)
   { 
       SharedPreferences sharedpreferences = getSharedPreferences(p_prefName, Context.MODE_PRIVATE); 
       return sharedpreferences.getBoolean(p_prefName, false);
   }

   @SuppressLint("InlinedApi")
   @TargetApi(Build.VERSION_CODES.HONEYCOMB)
   public void HideNavigBar()
   { 
	   View decorView = getWindow().getDecorView();
	   
	   decorView.setSystemUiVisibility(
                  View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
	    
   }
   
@Override
   public void onWindowFocusChanged(boolean hasFocus)
   { 
	   super.onWindowFocusChanged(hasFocus);
	   if (hasFocus)
	   {
		   HideNavigBar();
	   }
   } 
	  @Override
   public void onResume()
   {   
	   m_startApp.resume();
       super.onResume(); 
       m_wakeLock.acquire();
       m_screen.resume();
       m_renderView.resume();  
		 
   }

    @Override
    public void onPause()
    {   
    	switch(GameScreen.GetState())
    	{
    	case onMenu: 
    		if(MenuScreen.GetState()==  MenuScreen.State.AD_OPENLOAD)
        	{ 
			    genSet.game.HideDialog();
          		MenuScreen.GoToIdle();
        	}
    		break;
    	case onPlay: 
//    		if(PlayScreen2.GetState()==PlayScreen2.State.AD_OPENLOAD)
//        	{  
//        		PlayScreen2.EndGame();
//			    genSet.game.HideDialog();
//        	} 
//    		else if(PlayScreen2.GetState()==PlayScreen2.State.GAMEOVER && PlayScreen2.isOnGameOverOpenAd())
//        	{ 
//        		PlayScreen2.IdleGameOver();
//          		MenuScreen.GoToIdle();
//        	}  
    		break;
		default:
			break; 
     
    	} 
        super.onPause();   
        m_wakeLock.release();
        m_renderView.pause();
        m_screen.pause(); 
        if (isFinishing())
        {
           m_screen.dispose();
        }
    }

    @Override
    public void showToast(final String toast)
    { 
    	m_gameContext= getApplicationContext();
         
    	runOnUiThread(new Runnable()
    	{
    		public void run()
    		{
    			Toast.makeText(m_gameContext,toast, Toast.LENGTH_SHORT).show();
    		}
    		});
    } 
    
    @Override
    public void Vibrate(long[] p_pattern)
    {
     	m_vibrator.vibrate(p_pattern, -1);
    }

    @Override
    public void RateApp()
    {
        Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button, 
        // to taken back to our application, we need to add following flags to intent. 
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
//                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) { 
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName()   + (GameScreen.GetLanguage()!=LANGUAGE.ENGLISH? "&hl=" + GameScreen.GetLanguageCode():""))));
        }
    }

    @Override
    public void Vibrate(long p_duration)
    {
    	m_vibrator.vibrate(p_duration);
    }

    
    @Override
    public Input getInput()
    {
        return m_input;
    }

    @Override
    public FileIO getFileIO() 
    {
        return m_fileIO;
    }

    @Override
    public Context getGameContext() 
    {
        return m_gameContext;
    }

    @Override
    public Graphics getGraphics() 
    {
        return m_graphics;
    }

    @Override
    public Audio getAudio()
    {
        return m_audio;
    }
    

    @Override
    public void setScreen(Screen screen) 
    {
        if (screen == null)
        {
            throw new IllegalArgumentException("Screen must not be null");
        }
        
        this.m_screen.pause();
        this.m_screen.dispose();
        screen.resume();
        screen.update(0);
        this.m_screen = screen; 
    }
    
    public Screen getCurrentScreen()
    { 
    	return m_screen;
    }
    @Override
	public boolean StartAppHasNativeAd()
    {
    	return m_startApp.HasNativeAd();
    }
    
    @Override
   	public boolean StartAppIsInstiReady() 
    { 
    	return m_startApp.IsInstiReady(); 
    } 
    @Override
   	public void StartAppShowInsti(AdDisplayListener p_adDisplayListener) 
    { 
    	m_startApp.ShowInstilAd(p_adDisplayListener); 
    } 
    @Override
   	public boolean StartAppIsOfferWallReady() 
    { 
    	return m_startApp.IsOfferWallReady(); 
    } 
    
    @Override
   	public void StartAppRequestOfferWallAd() 
    { 
    	m_startApp.RequestOfferWallAd(getApplicationContext()); 
    }
    
    @Override
   	public void StartAppRequestInstiAd() 
    { 
    	m_startApp.RequestInstiAd(getApplicationContext()); 
    } 
    
    
    @Override
   	public void StartAppShowOfferWall(AdDisplayListener p_adDisplayListener) 
    { 
    	m_startApp.ShowOfferWallAd(p_adDisplayListener); 
    } 
    @Override
 	public void StartAppRequestNativeAd(String p_adTag) 
     {
     	 m_startApp.RequestNativeAd(getApplicationContext(),p_adTag);
     	
     } 
    @Override
 	public void StartAppRequestVideoAd(String p_adTag, VideoAdType p_vidAdType) 
     {
     	 m_startApp.RequestVideoAd(getApplicationContext(),p_adTag,p_vidAdType);
     	
     }
    @Override
   	public void StartAppExitAd()
    {  
    	m_startApp.ShowExitAd(getApplicationContext()); 
     }
    
    @Override
	public void StartAppSendClick()
    {  
    	runOnUiThread(new Runnable()
    	{
    		public void run()
    		{
    			try{
    	    	m_startApp.NativeAdSendClick(getApplicationContext()); 
    			}catch(Exception e)
    			{ 
    			}
    		}
    		}); 
    }
    
    @Override
	public void StartAppSendImpression()
    {
    	runOnUiThread(new Runnable()
    	{
    		public void run()
    		{
    	    	m_startApp.NativeAdSendImpression(getApplicationContext());
    		}
    		});
    } 
    @Override
	public Bitmap StartAppGetNativeAdBitmap()
    { 
    	return m_startApp.GetNativeAdBitmap();
    }
    @Override
	public Bitmap StartAppGetNativeAd2ndBitmap()
    {
    	return m_startApp.GetNativeAd2ndBitmap();
    }
    @Override
	public String StartAppGetNativeAdDesc()
    {
    	return m_startApp.GetNativeAdDesc();
    } 
    @Override
	public String StartAppGetNativeAdTitle()
    {
    	return m_startApp.GetNativeAdTitle();
    }
    
    @Override
	public void StartAppShowVideoAds()
    {
    	  m_startApp.ShowVideoAds();
    }

    @Override
	public boolean StartAppIsVidAdAvailable()
    {
    	 return m_startApp.isVidAdAvailable();
    }
    
    
 
}