package com.eugexstudios.framework;

import android.graphics.Rect;

public class ImageData {

	public int x,y, src_x, src_y, width, height;
	public Rect rect;
	public ImageData(int p_src_x,  int p_src_y, int p_wid, int p_hei)
	{
		x= 0;
		y= 0;
		src_x=p_src_x;
		src_y=p_src_y;
		width= p_wid;
		height= p_hei;
		calcRect();
	}
	public ImageData(int p_x, int p_y, int p_src_x,  int p_src_y, int p_wid, int p_hei)
	{
		x= p_x;
		y= p_y;
		src_x=p_src_x;
		src_y=p_src_y;
		width= p_wid;
		height= p_hei;
		calcRect();
	}
	public ImageData(float p_x, int p_y, int p_src_x,  int p_src_y, int p_wid, int p_hei)
	{
		x=(int) p_x;
		y= p_y;
		src_x=  p_src_x;
		src_y=p_src_y;
		width= p_wid;
		height= p_hei;
		calcRect();
	}
	public ImageData(int p_x, float p_y, int p_src_x,  int p_src_y, int p_wid, int p_hei)
	{
		x= p_x;
		y= (int)p_y;
		src_x=  p_src_x;
		src_y=p_src_y;
		width= p_wid;
		height= p_hei;
		calcRect();
	}
	public ImageData(float p_x, float p_y, int p_src_x,  int p_src_y, int p_wid, int p_hei)
	{
		x= (int)p_x;
		y= (int)p_y;
		src_x=  p_src_x;
		src_y=p_src_y;
		width= p_wid;
		height= p_hei;
		calcRect();
	}	
	private void calcRect()
	{
		rect = new Rect(x,y,x+width, y+height);
	}
}
