package com.eugexstudios.framework;

import android.graphics.Bitmap;  
import com.eugexstudios.framework.Graphics.ImageFormat;

public interface Image {
    public int getWidth();
    public int getHeight();
    public ImageFormat getFormat();
    public void dispose();

    public Bitmap colorize( int dstColor); 
}
