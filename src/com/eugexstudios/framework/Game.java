package com.eugexstudios.framework;

import com.eugexstudios.adprovider.StartApp.VideoAdType;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;

import android.content.Context; 
import android.graphics.Bitmap; 

public interface Game {

	
	public void GoToPlayStoreLink();
	
    public Audio getAudio();

    public Input getInput();

		
    public FileIO getFileIO(); 
    public void OpenFacebookPage();
    public void GoToPublisherPage();
    public void SendEmailFeedback();
    public void ShareApp(int p_score);
    public void SendBetaDatas();
     
    public Context getGameContext();

	void showToast(String toast);

  public String GetVersionName() ;  
    
    public Graphics getGraphics();

    public void setScreen(Screen screen);

    public Screen getCurrentScreen();
 
    public Screen getInitScreen(); 
    public void Vibrate(long[] p_pattern); 

    public void RateApp();
    public void Vibrate(long p_duration);

    public void SetSharedPrefString(String p_prefName, String p_val);
    public void SetSharedPrefBoolean(String p_prefName, boolean p_val);
    public void SetSharedPrefInt(String p_prefName, int p_val);
    public String GetSharedPrefString(String p_prefName);
    public boolean GetSharedPrefBoolean(String p_prefName);
    public int GetSharedPrefInt(String p_prefName);

	public void StartAppRequestOfferWallAd();
	public void StartAppRequestInstiAd();

   	public void StartAppShowInsti(AdDisplayListener p_adDisplayListener) ;
   	public boolean StartAppIsOfferWallReady();
   	public boolean StartAppIsInstiReady(); 
	public void StartAppShowOfferWall(AdDisplayListener p_adDisplayListener);
	public void StartAppRequestNativeAd(String p_adTag);
	public void StartAppRequestVideoAd(String p_adTag,VideoAdType p_vidAdType) ;
	public boolean StartAppHasNativeAd();
	public void StartAppSendClick(); 
	public void StartAppExitAd(); 
	public void StartAppSendImpression();
	public Bitmap StartAppGetNativeAdBitmap();
	public Bitmap StartAppGetNativeAd2ndBitmap();
	public String StartAppGetNativeAdDesc();
	public String StartAppGetNativeAdTitle();

	public void StartAppShowVideoAds();
	public boolean StartAppIsVidAdAvailable();
	 
	void onWindowFocusChanged(boolean hasFocus);

	public void HideNavigBar();
	public boolean IsBannerAdEnabled(); 
	public int GetBannerHeight();
	public void ShowBannerAd();
	public void HideBannerAd();

	public void ShowDialog(final String p_str);
	public void HideDialog();
	public int getFPS();
	 

}
