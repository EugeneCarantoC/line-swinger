package com.eugexstudios.adprovider; 
import java.util.ArrayList; 

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log; 

import com.eugexstudios.framework.CONFIG; 
import com.eugexstudios.lineswinger.GameScreen;
import com.startapp.android.publish.ads.nativead.NativeAdDetails; 
import com.startapp.android.publish.ads.nativead.NativeAdPreferences;
import com.startapp.android.publish.ads.nativead.StartAppNativeAd;  
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.SDKAdPreferences;  
import com.startapp.android.publish.adsCommon.StartAppAd;
import com.startapp.android.publish.adsCommon.StartAppAd.AdMode;
import com.startapp.android.publish.adsCommon.StartAppSDK; 
import com.startapp.android.publish.adsCommon.VideoListener;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;
import com.startapp.android.publish.adsCommon.adListeners.AdEventListener;  

public class StartApp {


	private StartAppAd				   m_rewardedVidAd;  
	private StartAppAd				   m_exitAd;  
	private StartAppAd				   m_offerWallAd;
	private StartAppAd				   m_instiAd;  
    private NativeAdPreferences 	   m_nativePrefs; 
    private StartAppNativeAd 		   m_nativeAd;  
    private NativeAdDetails			   m_nativeAdDetail; 
    
    
    public void ShowVideoAds()
    {
    	m_rewardedVidAd.showAd();
    }
    public boolean isVidAdAvailable()
    {
    	return m_rewardedVidAd.isReady();
    }
    public void resume()
    {
    	m_rewardedVidAd.onResume();  
    }
    public void onPause()
    {
    	m_rewardedVidAd.onPause(); 
    } 
     
    public StartApp(Bundle p_savedInstanceState, Activity p_activity, Context p_context)
    { 
    	initStartApp(p_savedInstanceState,p_activity, p_context);
    	
    }
    
    public void initStartApp(Bundle p_savedInstanceState, Activity p_activity, Context p_context)
    { 
 	   StartAppSDK.init(p_activity,  
 			   CONFIG.STARTAPP_APP_ID, 
                new SDKAdPreferences().setAge(3) ); 

       StartAppAd.init(p_context,  
 			   CONFIG.STARTAPP_DEV_ID , 
 			   CONFIG.STARTAPP_APP_ID);  
       m_offerWallAd   = new StartAppAd(p_context);  
       m_instiAd       = new StartAppAd(p_context);
       m_nativeAd      = new StartAppNativeAd(p_context); 
       m_exitAd        = new StartAppAd(p_context);  
       m_rewardedVidAd = new StartAppAd(p_context) ;   
       m_rewardedVidAd.disableSplash();   
    }  
     
    public boolean HasNativeAd()
    {
    	return m_nativeAdDetail!=null;
    }
    public void NativeAdSendImpression(Context p_context)
    {
 	   m_nativeAd.getNativeAds().get(0).sendImpression(p_context); 
    }
    
    public void NativeAdSendClick(Context p_context)
    {  
    	m_nativeAdDetail.sendClick(p_context); 
    } 
    
    public void ShowExitAd(Context p_context)
    {   
    	m_exitAd.showAd(new AdDisplayListener() {
    	    @Override
    	    public void adHidden(Ad ad) {
    	    	GameScreen.CloseGame();
    	    }
    	    @Override
    	    public void adDisplayed(Ad ad) {
    	    }
    	    @Override
    	    public void adClicked(Ad ad) {
    	    }
    	    @Override
    	    public void adNotDisplayed(Ad ad) { 
    	    	GameScreen.CloseGame();
    	    }
    	}) ;
    } 
    
    
    public Bitmap GetNativeAdBitmap()
    {   
 	   return m_nativeAdDetail.getImageBitmap(); 
    } 
    public String GetNativeAdDesc()
    {   
 	   return m_nativeAdDetail.getDescription(); 
    }  
    public String GetNativeAdTitle()
    {   
  	   return m_nativeAdDetail.getTitle(); 
     } 
    
    public Bitmap GetNativeAd2ndBitmap()
    { 
 	   return m_nativeAdDetail.getSecondaryImageBitmap(); 
    }
    
    
    public void RequestNativeAd(Context p_context, final String p_adTag)
    {    
    	m_nativeAdDetail=null;  
           
         m_nativePrefs = new NativeAdPreferences()
         	 .setAdsNumber(3)                // Load 3 Native Ads 
         	 .setAutoBitmapDownload(true)    // Retrieve Images object
             .setPrimaryImageSize(4);       
            
         AdEventListener l_adListener  = new AdEventListener() 
         {   
         	  @Override
           	  public void onReceiveAd(Ad arg0) 
           	  {
         		  ArrayList<NativeAdDetails> ads = m_nativeAd.getNativeAds(p_adTag);    // get NativeAds list
           		 
         		  for(int idx=0;idx<ads.size();idx++)
           		  {
         			 m_nativeAdDetail= ads.get(idx);
           		  }   
           	  }
         	  
           	  @Override
           	  public void onFailedToReceiveAd(Ad arg0) 
           	  {
           		  Log.e("MyApplication", "Error while loading Ad");
           		  
           	  }  	
         };
         
         m_nativeAd.loadAd(m_nativePrefs, l_adListener); 
    }
    
    
    public void RequestOfferWallAd(Context p_context)
    {  
        m_offerWallAd   = new StartAppAd(p_context); 
    	m_offerWallAd.loadAd(AdMode.OFFERWALL );  
    } 
    
    public void RequestInstiAd(Context p_context)
    {  
    	m_instiAd   = new StartAppAd(p_context); 
    	m_instiAd.loadAd(AdMode.FULLPAGE );  
    }
    public void ShowInstilAd(AdDisplayListener p_adDisplayListener)
    { 
    	m_instiAd.showAd(p_adDisplayListener);
    }
    
    public boolean IsInstiReady()
    { 
    	return m_instiAd.isReady();
    }  
    
    public boolean IsOfferWallReady()
    { 
    	return m_offerWallAd.isReady();
    }
    public void ShowOfferWallAd(AdDisplayListener p_adDisplayListener)
    { 
//    	new AdDisplayListener() {
//		    @Override
//		    public void adHidden(Ad ad) {
//		    }
//		    @Override
//		    public void adDisplayed(Ad ad) {
//		    }
//		    @Override
//		    public void adClicked(Ad ad) {
//		    }
//		    @Override
//		    public void adNotDisplayed(Ad ad) {
//		    }
//		}
		
    	if(m_offerWallAd.isReady())
    	{
    		m_offerWallAd.showAd(p_adDisplayListener );
    	}
    }
    
    public enum VideoAdType 
    {
    	 NextLevel
    	,RewardToContinue
    }
      VideoAdType m_vidType;
    public void RequestVideoAd(Context p_context, final String p_adTag, VideoAdType p_vidType)
    {      
    	m_vidType = p_vidType; 
        m_rewardedVidAd.setVideoListener(new VideoListener() {
     	     @Override
     	     public void onVideoCompleted() {
     	    	 switch(m_vidType)
     	    	 {	
//     	    	 	case NextLevel:        GameScreen.GoToNextLevel(); break; 
     	    	 	case RewardToContinue: GameScreen.ContinueGame(); break; 
     	    	 }
     	     }
     	});
        
        
//        m_rewardedVidAd.loadAd(AdMode.  VIDEO 
//        		,
//     		   new AdEventListener() {
//     	    @Override
//     	    public void onReceiveAd(Ad ad) {  
//    	    	 m_rewardedVidAd.showAd();
//     	    }
//     	    @Override
//     	    public void onFailedToReceiveAd(Ad ad) {   
//     	    } 
//     		}
//        ); 
         
        switch(m_vidType)
    	{
    	case NextLevel: m_rewardedVidAd.loadAd(AdMode.VIDEO ); break; 
    	case RewardToContinue: m_rewardedVidAd.loadAd(AdMode.REWARDED_VIDEO ); break; 
    	}
    
         

    }
    
}
