package com.eugexstudios.lineswinger;

import android.graphics.Color;
import android.graphics.Paint.Align;

import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent; 
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.LanguageManager;
import com.eugexstudios.utils.genSet;
import com.eugexstudios.utils.LanguageManager.W;

public class InstructionScreen extends Screen {
	
	
	public enum type 
	{ 
		  TAP    
		, SHOP
	}

	private int                        m_tapCount;
	private boolean                    m_hasAcceptBtn;
	private boolean                    m_willExit;
	private int                        m_dragCtr;
 
	private type 				       m_type;    
	
	   private final byte 
			  ACCEPT_BTN         = 1 ;
	public InstructionScreen(type p_type)
	{
		m_type = p_type;
		m_tapCount=0;
		m_hasAcceptBtn = false;
		m_willExit     = false;
		m_dragCtr      = 0;
	}
	public type Type()
	{
		return m_type;
	}
	@Override
	public void Paint(Graphics g) { 
		switch(m_type)
		{ 
			case TAP: PaintTap(g);   break; 
			case SHOP:    break;   
			default: break;
		}
	}

	private void PaintTap(Graphics g)
	{
		genSet.setTextProperties(43, Color.argb(255, 255, 255, 255), Align.CENTER); 
		g.drawStringFont(W.TAP_ANYWHERE_TO_SWING, CONFIG.SCREEN_MID, Constants.ScaleY(0.8f), AssetManager.Comforta, genSet.paint);
	 
		if(m_hasAcceptBtn)
		{
			drawRoundRect(g, 6,5, Constants.ACCEPT_INSTR_BTN, Color.argb(120,255,255,255), Color.argb(160,88,20,92), ACCEPT_BTN); 
			drawRoundRect(g,  Constants.ACCEPT_INSTR_BTN, Color.argb(160,255,255,255), Color.argb(162,88,20,92), ACCEPT_BTN);   

			genSet.setTextProperties(45, ACCEPT_BTN==buttonPressed?Color.WHITE:Color.DKGRAY, Align.CENTER); 
			g.drawStringFont(W.GOT_IT, Constants.ACCEPT_INSTR_BTN.x + Constants.ACCEPT_INSTR_BTN.width/2,Constants.ACCEPT_INSTR_BTN.y+ Constants.ACCEPT_INSTR_BTN.height + 45/2, AssetManager.Comforta,genSet.paint); 

		}
	}
	@Override
	public void Updates(float p_deltaTime) { 
		
	}
	public boolean canExitInstruction()
	{
		return m_hasAcceptBtn;
	}
	public boolean willExitInstruction()
	{
		return m_willExit;
	}
	public boolean isButtonPressed()
	{
		return buttonPressed!=0;
	}
	@Override
	public void TouchUpdates(TouchEvent g) {
		switch(m_type)
		{ 
			case TAP: 
 
				if(g.type == TouchEvent.TOUCH_DOWN)
				{ 
					m_dragCtr=0;
					if(m_hasAcceptBtn)
					{ 
						drawRoundRect(g,  Constants.ACCEPT_INSTR_BTN, Color.argb(160,255,255,255), Color.argb(162,88,20,92), ACCEPT_BTN);   
					}
					m_tapCount++;
					if(m_tapCount==4)
					{
						m_hasAcceptBtn= true;
					} 
				}
				else if(g.type == TouchEvent.TOUCH_DRAGGED)
				{
					m_dragCtr++;
					if(m_dragCtr%8==0)
					{
						m_tapCount=0;
						genSet.ShowToast(LanguageManager.GetLocalized(W.DONT_DRAG));
					}
				}
				else if(g.type == TouchEvent.TOUCH_UP)
				{
					if(buttonPressed!=0)
					{
						switch(buttonPressed)
						{
							case ACCEPT_BTN:m_willExit = true; break;
						}
					}
				}
				
				break; 
			case SHOP:    break;   
			default: break;
		}
	}

	@Override
	public void backButton() {
		
		switch(m_type)
		{ 
			case TAP: 
				if(m_hasAcceptBtn)
				{ 
					m_willExit=true;
				}
				
				break; 
			case SHOP:    break;   
			default: break;
		}
		

	}

}
