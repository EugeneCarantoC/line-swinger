package com.eugexstudios.lineswinger;
  
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent; 
import com.eugexstudios.utils.Constants;

public class Backbutton extends Screen{
    
	private boolean m_isPressed,m_backPressed;
	
	public Backbutton()
	{
		m_isPressed = false;
	}
	@Override
	public void Paint(Graphics g)
	{
// 		g.drawRect(Constants.BACK_BTN_IMG,Color.WHITE);//bounds 
 		if(m_isPressed)
 			g.drawImage(AssetManager.sprites,3,6,Constants.BACK_BTN_IMG);
 		else
 			g.drawImage(AssetManager.sprites,Constants.BACK_BTN_IMG);
	}

	@Override
	public void Updates(float p_deltaTime) 
	{  
	}
	public boolean isPressed()
	{
		if(m_backPressed)
		{ 
			m_backPressed=false;
			return true;
		}
		return false;
	}
	@Override
	public void TouchUpdates(TouchEvent g) { 
		if(inBounds(g, Constants.BACK_BTN_IMG.rect))
		{
			if(g.type==TouchEvent.TOUCH_DOWN)
			{
				m_isPressed=true;
			}
			else if(g.type==TouchEvent.TOUCH_UP)
			{
				m_isPressed=false;
				m_backPressed=true;
			}
		}
		else 
		{
			  if(g.type==TouchEvent.TOUCH_DRAGGED)
			{
				m_isPressed=false; 
			}
		}
	}
	
	@Override
	public void backButton() { 
		
		
	}

}
