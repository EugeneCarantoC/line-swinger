package com.eugexstudios.lineswinger;

import android.graphics.Color;
import android.graphics.Paint.Align; 
import android.graphics.Rect;

import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.ImageData;
import com.eugexstudios.framework.Input.TouchEvent; 
import com.eugexstudios.utils.ColorUtil;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.LanguageManager;
import com.eugexstudios.utils.genSet;
import com.eugexstudios.utils.LanguageManager.W;

public class ShopScreen extends Screen {
	
	final byte 
	  YES_BTN        = 1
	, NO_BTN         = 2;
	
	private int AVAILABLE_SWINGER  = 15;
	private final int XSIZE        = 3; 
	private int m_ySize            = 4;  
//	final int m_btnColor1          = Color.argb(140,109,26,67);  
	final int m_btnColor1          = Color.argb(200,242,49,98);  
	final int m_btnColor2          = Color.argb(240,202,43,92);  
	final int m_inusedColor        = Color.argb(140,72,87,205);   
	final int m_inusedColor2        = Color.argb(220,70,84,185);   
	private int DRAG_UP_LIMIT      = Constants.ScaleY(0.9f); 
	private int MARGIN_X           = Constants.ScaleX(0.125f); 
	private int MARGIN_Y           = Constants.ScaleY(0.22f); 
	private int WINDOW_WIDTH       = CONFIG.SCREEN_WIDTH - MARGIN_X*2; 
	private int WINDOW_HEIGHT      = Constants.ScaleY(0.65f);  
	private int BTN_MARGIN_X       = Constants.ScaleX(0.05f); 
	private int BTN_MARGIN_Y       = Constants.ScaleX(0.05f); 
	private int BTN_WIDTH          = (int)(WINDOW_WIDTH/XSIZE)-BTN_MARGIN_X - Constants.ScaleX(0.021f); 
	private int BTN_HEIGHT         = (int)(WINDOW_HEIGHT/m_ySize)- BTN_MARGIN_Y- Constants.ScaleY(0.015f);  

	private int XGAP = WINDOW_WIDTH/XSIZE-BTN_MARGIN_X/2;
	private int YGAP = WINDOW_HEIGHT/m_ySize-BTN_MARGIN_Y/2;
	private final Rect COVER_RECT=new Rect(0,0,CONFIG.SCREEN_WIDTH,MARGIN_Y);

	enum states {
		  	IDLE   
		  , PURCH  
	}; 
	private states                               m_state;
	private boolean[]                            m_isPurchased; 
	private ImageData[]                          m_merchImage;
	private int[]                                m_merchPrice; 

	private int                                  m_merchToPurch;
	private int                                  m_swingerUsed; 
	private int                                  m_gems; 
	private float                                m_dragY; 
	private boolean                              m_onDrag;
	private int                                  m_holdPosY;
	private int                                  m_windowPosY;
	private int                                  m_windowBottom;
	private Backbutton                           m_backButton;
	
	public ShopScreen() 
	{ 
		m_state = states.IDLE;
		m_isPurchased = GameScreen.GetSwingerPurchaseData();
		m_swingerUsed = GameScreen.GetSwingerUsed();
		m_merchPrice  = GameScreen.GetMerchPrice();
		m_gems        = GameScreen.GetGemsCollected(); 
		m_merchImage  = new ImageData[m_isPurchased.length]; 
		m_dragY       = 0;
		m_onDrag      = false;
		m_holdPosY    = 0;
		m_backButton  = new Backbutton();
		for(int idx=0;idx<AVAILABLE_SWINGER;idx++)
		{
			if(idx<m_isPurchased.length)
			{
				m_merchImage[idx] = GameScreen.GetSwingerImageData(idx);
				if(m_merchPrice[idx]==0)
				m_isPurchased[idx]= true;
			} 
		} 
		m_ySize       = (AVAILABLE_SWINGER/XSIZE)+(AVAILABLE_SWINGER%XSIZE==0?0:1);
		WINDOW_HEIGHT = m_ySize*(YGAP) +YGAP/3; 
		updateWindowPosY();
	}
	
	@Override
	public void Paint(Graphics g) {
		
		switch(m_state)
		{
			case IDLE:  PaintIdle(g);break;
			case PURCH: PaintAskPurch(g);break;
		} 
		m_backButton.Paint(g);
	}
	
	private void updateWindowPosY()
	{ 
		m_windowPosY  = (int)(m_dragY+MARGIN_Y); 
		m_windowBottom = m_windowPosY + WINDOW_HEIGHT; 
	}
	
	private void PaintIdle(Graphics g) 
	{  
		g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(45, 88, 20, 92)); //Red
		g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(110, 0, 0, 0)); //BLACK  
 
		g.drawRoundRect(MARGIN_X, m_windowPosY, WINDOW_WIDTH, WINDOW_HEIGHT, 40, 40, Color.rgb(255, 235, 253));//Window
		
		genSet.setTextProperties(24, Color.WHITE,Align.CENTER);
		
		for(int idx=0;idx<m_ySize;idx++)
		{
			for(int idx2=0;idx2<XSIZE;idx2++)
			{	
				int l_xPos =  BTN_MARGIN_X+MARGIN_X+idx2*XGAP +5;
				int l_yPos =  (int)(m_windowPosY+BTN_MARGIN_Y+idx*YGAP-BTN_HEIGHT/2);
				
				int lIdx = (idx*XSIZE) +idx2; 
				if(lIdx<AVAILABLE_SWINGER)
				{
					drawRoundRect(g,l_xPos, l_yPos, BTN_WIDTH, BTN_HEIGHT,lIdx==m_swingerUsed?m_inusedColor:m_btnColor1,lIdx==m_swingerUsed?m_inusedColor2:m_btnColor2, lIdx+1);
					drawRoundRect(g,l_xPos+5 , l_yPos+8, BTN_WIDTH, BTN_HEIGHT,lIdx==m_swingerUsed?m_inusedColor:m_btnColor1,lIdx==m_swingerUsed?m_inusedColor2:m_btnColor2, lIdx+1);
					
					g.drawImage(AssetManager.swingers, l_xPos+BTN_WIDTH*0.5f-m_merchImage[lIdx].width/2, l_yPos + BTN_HEIGHT*0.75f, m_merchImage[lIdx]);
					
					if(m_merchPrice[lIdx]!=0&&m_isPurchased[lIdx]==false)
					{
//						drawRoundRect(g,l_xPos, l_yPos, BTN_WIDTH, BTN_HEIGHT,Color.argb(75, 110, 110, 110),Color.argb(30, 255, 255, 255), lIdx+1);
//						drawRoundRect(g,l_xPos+5 , l_yPos+8, BTN_WIDTH, BTN_HEIGHT,Color.argb(75, 110, 110, 110),Color.argb(30, 255, 255, 255), lIdx+1);
						g.drawImage(AssetManager.sprites, BTN_WIDTH/2+l_xPos,+YGAP+l_yPos-BTN_HEIGHT/2, Constants.LOCK_IMG);

						g.drawRoundRect(l_xPos+2,  YGAP+l_yPos, BTN_WIDTH,BTN_HEIGHT/4, 10,10, Color.argb(240,109,26,67));
						g.drawStringFont(m_merchPrice[lIdx]+"", BTN_WIDTH/2+l_xPos+2, YGAP+l_yPos+BTN_HEIGHT/4-12,AssetManager.Comforta,genSet.paint);
					}  
				}
			}
		} 
		
		g.drawRect(COVER_RECT, ColorUtil.BG1_LAYER1);     
		g.drawRect(COVER_RECT, ColorUtil.BG1_LAYER2);  
		g.drawRect(COVER_RECT, Color.argb(45, 88, 20, 92)); //Red
		g.drawRect(COVER_RECT, Color.argb(110, 0, 0, 0)); //BLACK  
		genSet.setTextProperties(65, Color.argb(45, 50, 50, 50),Align.CENTER);  
		g.drawStringFont("SWINGERS",CONFIG.SCREEN_MID+5, 170+5, AssetManager.Comforta, genSet.paint);
		genSet.setTextProperties(65, Color.WHITE,Align.CENTER);  
		g.drawStringFont("SWINGERS",CONFIG.SCREEN_MID, 170, AssetManager.Comforta, genSet.paint);
		 
		genSet.setTextProperties(40, Color.WHITE,Align.LEFT);
		g.drawStringFont(m_gems+"", CONFIG.SCREEN_MID, 230, AssetManager.Comforta, genSet.paint);
		g.drawImage(AssetManager.sprites,CONFIG.SCREEN_MID-Constants.GEMCOUNT_IMG.width-8,230-Constants.GEMCOUNT_IMG.height/2-13, Constants.GEMCOUNT_IMG);
 
		 
	}
	
	private void PaintAskPurch(Graphics g) 
	{
		PaintIdle(g);
		g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(55, 0, 0, 0)); //BLACK 
		g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(75, 88, 20, 92)); //Red 
		drawRoundRect(g,  (int)(CONFIG.SCREEN_WIDTH*0.0375f),(int)(CONFIG.SCREEN_HEIGHT*0.19f), (int)(CONFIG.SCREEN_WIDTH*(1-(0.0375f*2))), (int)(CONFIG.SCREEN_HEIGHT* 0.29f), Color.WHITE, Color.rgb(190, 190, 190), -1); 
			
		genSet.setTextProperties(Constants.ASK_EXIT_LS, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont(W.ASK_PURCHASE, CONFIG.SCREEN_WIDTH/2, (int)(CONFIG.SCREEN_HEIGHT*0.355f)+(int)(CONFIG.SCREEN_HEIGHT*0.078125f), AssetManager.Comforta,genSet.paint);

		drawRoundRect(g, 4,-5, Constants.YES_EXIT, Color.argb(120,88,20,92), Color.argb(160,88,20,92), YES_BTN); 
		drawRoundRect(g, Constants.YES_EXIT, Color.argb(160,88,20,92), Color.argb(162,88,20,92), YES_BTN);  

		drawRoundRect(g, 4,5, Constants.NO_EXIT, Color.argb(120,88,20,92), Color.argb(160,88,20,92), NO_BTN); 
		drawRoundRect(g,  Constants.NO_EXIT, Color.argb(160,88,20,92), Color.argb(162,88,20,92), NO_BTN);   

		genSet.setTextProperties(Constants.ASK_EXIT_LS, Color.WHITE, Align.CENTER); 
		g.drawStringFont(W.YES, (int)(CONFIG.SCREEN_WIDTH*0.07f)+(int)(CONFIG.SCREEN_WIDTH*0.42f)/2,								  (int)(CONFIG.SCREEN_HEIGHT*0.4365f)+ (int)(CONFIG.SCREEN_HEIGHT*0.132f), AssetManager.Comforta,genSet.paint); 
		g.drawStringFont(W.NO,  (int)(CONFIG.SCREEN_WIDTH*0.07f)+(int)(CONFIG.SCREEN_WIDTH*0.42f)/2+(int)(CONFIG.SCREEN_WIDTH*0.44f), (int)(CONFIG.SCREEN_HEIGHT*0.4365f)+ (int)(CONFIG.SCREEN_HEIGHT*0.132f), AssetManager.Comforta,genSet.paint);
 
	}
	
	@Override
	public void Updates(float p_deltaTime) {

		if(m_backButton.isPressed())
		{ 
			backButton();
		}
		
		switch(m_state)
		{
			case IDLE:  	break;
			case PURCH: break;
		}  
	}

	@Override
	public void TouchUpdates(TouchEvent g) 
	{
		switch(m_state)
		{
			case IDLE: IdleTouchUpdates(g);break;
			case PURCH: PurchAskTouchUpdates(g); break;
		} 
	}


	private void PurchAskTouchUpdates(TouchEvent g) 
	{
		if(g.type == TouchEvent.TOUCH_DOWN)
		{
			drawRoundRect(g, Constants.YES_EXIT, Color.argb(160,88,20,92), Color.argb(162,88,20,92), YES_BTN); 
			drawRoundRect(g,  Constants.NO_EXIT, Color.argb(160,88,20,92), Color.argb(162,88,20,92), NO_BTN);   
		}
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{ 
				case YES_BTN:
					if(m_gems>=m_merchPrice[m_merchToPurch])
					{ 
						GameScreen.UseGems(m_merchPrice[m_merchToPurch]);
						m_gems = GameScreen.GetGemsCollected(); 
						m_isPurchased[m_merchToPurch]=true;
						GameScreen.updateSwingerPurch(m_isPurchased); 
						m_state = states.IDLE;
						genSet.ShowToast(LanguageManager.GetLocalized(W.PURCH_SUCCESS));
					}
					else
					{ 
						genSet.ShowToast(LanguageManager.GetLocalized(W.NOT_ENOUGH_GEMS)); 
						m_state = states.IDLE;
					}
					
					break;
				case NO_BTN: m_state = states.IDLE; break;
			}
			buttonPressed=0;
		}
	}
	
	private void IdleTouchUpdates(TouchEvent g) 
	{
		m_backButton.TouchUpdates(g);
		if(g.type == TouchEvent.TOUCH_DOWN)
		{
			for(int idx=0;idx<m_ySize;idx++) 
			{
				for(int idx2=0;idx2<XSIZE;idx2++)
				{	
					int lIdx = (idx*XSIZE)+idx2;
	
					if(lIdx<AVAILABLE_SWINGER)
					{
						int l_xPos =  BTN_MARGIN_X+MARGIN_X+idx2*XGAP +5;  
						int l_yPos =  (int)(m_windowPosY+BTN_MARGIN_Y+idx*YGAP-BTN_HEIGHT/2); 
						drawRoundRect(g,l_xPos, l_yPos, BTN_WIDTH, BTN_HEIGHT,m_btnColor1,m_btnColor2, lIdx+1);  
					}
				}
			}
			
		} 
		if(g.type == TouchEvent.TOUCH_DRAGGED)
		{

			if(buttonPressed==0)
			{
				if(m_onDrag==false)
				{
					m_onDrag = true;
					m_holdPosY=g.y;
				}
				else
				{
					int l_dragSize =g.y -m_holdPosY;
					m_holdPosY= g.y;
					m_dragY+=l_dragSize; 
				}
			}

			updateWindowPosY();
		}
		if(g.type == TouchEvent.TOUCH_UP)
		{
			if(buttonPressed!=0)
			{
				int l_merchPressed =buttonPressed-1;
				
				if(m_isPurchased[l_merchPressed])
				{
					m_swingerUsed = l_merchPressed;
					GameScreen.SetSwingerUsed(l_merchPressed);  
				}
				else
				{
					if(m_gems>=m_merchPrice[l_merchPressed])
					{ 
						m_merchToPurch = l_merchPressed;
						m_state = states.PURCH;
					}
					else
					{ 
						genSet.ShowToast(LanguageManager.GetLocalized(W.NOT_ENOUGH_GEMS)); 
						m_state = states.IDLE;
					}
				}
			}
			else
			{
				if(m_onDrag)
				{
					m_onDrag=false;
					if(m_windowPosY>MARGIN_Y)
					{ 
						m_dragY=0;
					}  
					updateWindowPosY();
					if(m_windowBottom<DRAG_UP_LIMIT)
					{ 
						m_windowPosY   = DRAG_UP_LIMIT-WINDOW_HEIGHT; 
						m_dragY        = m_windowPosY- MARGIN_Y;
						m_windowBottom = m_windowPosY + WINDOW_HEIGHT;
					}
						
				}
			}
			buttonPressed=0;
		}
		
	}

	@Override
	public void backButton() 
	{		
		switch(m_state)
		{
			case IDLE: GameScreen.GoToMenu();break;
			case PURCH: break;
		} 
		
	}

}
