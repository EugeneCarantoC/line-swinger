package com.eugexstudios.lineswinger;

import java.util.Random;
 
import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;

import android.graphics.Point;

public class Shatter {

	private Point m_pos;
	private float m_speedX;
	private float m_speedY;
	private short m_rad;
	private int   m_force; 
	private int   m_color; 
	private boolean   m_isVisib; 
	private Random m_rand = new Random();
	
	public Shatter(float p_posX,float p_posY, int p_color,int p_force)
	{
		init(new Point((int)p_posX, (int)p_posY) ,p_color, p_force);
	}
	public Shatter(Point p_pos, int p_color,int p_force)
	{
		init(p_pos,p_color,p_force);
	}
	private void init(Point p_pos, int p_color,int p_force)
	{
		m_rand   = new Random();
		m_pos    = p_pos;
		m_speedX = (m_rand.nextBoolean()?m_rand.nextFloat():-(m_rand.nextFloat()));
		m_speedY = (m_rand.nextBoolean()?m_rand.nextFloat():-(m_rand.nextFloat()));
		
		m_rad   = (short)(m_rand.nextInt(4)+5);
		m_color = p_color; 
		m_force = p_force;
		m_isVisib =false;
	}
	public void show(float p_posX,float p_posY)
	{
		m_isVisib = true;
		m_pos     = new Point((int)p_posX, (int)p_posY);
	}
	
	public boolean isDone()
	{
		return m_force<=0;
	}
	public void Update(float p_deltatime)
	{ 
		if(!m_isVisib)return; 
		m_pos.x += m_speedX * p_deltatime;
		m_pos.y += m_speedY * p_deltatime;

		if(m_pos.x<=0 || m_pos.x>=CONFIG.SCREEN_WIDTH)
		{
			if(m_pos.x<=0)
			{
				m_pos.x = 0;
			}
			else if(m_pos.x>=CONFIG.SCREEN_WIDTH)
			{
				m_pos.x = CONFIG.SCREEN_WIDTH;
			}
			
			m_speedX = -m_speedX; 
			m_speedY = (m_rand.nextBoolean()?m_rand.nextInt(16)+4:-(m_rand.nextInt(16)+4)); 
		}
		
		if(m_pos.y<=0 || m_pos.y>=CONFIG.SCREEN_HEIGHT)
		{
			if(m_pos.y<=0)
			{
				m_pos.y = 0;
			}
			else if(m_pos.y>=CONFIG.SCREEN_HEIGHT)
			{
				m_pos.y = CONFIG.SCREEN_HEIGHT;
			}
			
			m_speedY = -m_speedY;

			m_speedX = (m_rand.nextBoolean()?m_rand.nextInt(16)+4:-(m_rand.nextInt(16)+4)); 
			 
		}
		
		m_force -= Math.abs(m_speedX);
		if(m_force<=0)
			m_isVisib=false;
		
	}
	
	public void Paint(Graphics g)
	{
		if(!m_isVisib)return;
		g.drawCircle(m_pos, m_rad, m_color); 
	}
	
}
