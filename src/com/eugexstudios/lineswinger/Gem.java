package com.eugexstudios.lineswinger;

import java.util.Random;

import android.graphics.Color;
import android.graphics.Rect;

import com.eugexstudios.framework.Graphics;
import com.eugexstudios.utils.Constants;

public class Gem {

	private float                        m_xPos, m_yPos;
	private Rect                         m_bounds;
	private float                        m_ySpeed; 
	private Shatter[]                    m_shatter;
	private boolean                      m_onShatter;
	private boolean                      m_shatterDone;
	private int                          m_value;
	public Gem(int p_xPos)
	{
		m_xPos          = p_xPos;
		m_yPos          = -100;
		m_ySpeed        = 2f;
		m_onShatter     = false;
		m_shatterDone   = false;
		UpdateBounds();
		 
		Random l_rand = new Random();
		m_shatter = new Shatter[l_rand.nextInt(4)+10];
		for(int idx=0;idx<m_shatter.length;idx++)
		{
			m_shatter[idx] = new Shatter(m_xPos +m_bounds.width()/2,m_yPos+m_bounds.height()/2,

					Color.argb(l_rand.nextInt(100)+105, 234,55,40)
					,l_rand.nextInt(5)+25);
		}
		m_value = 1;
	}

    public int getValue()
    { 
    	return m_value;
    }
    public void removeValue()
    { 
    	m_value =0;
    }
	public void updates(float p_deltatime)
	{
		if(m_onShatter)
		{
			m_shatterDone=true;
			for(int idx=0;idx<m_shatter.length;idx++)
			{ 
				m_shatter[idx].Update(p_deltatime);
				
				if(m_shatter[idx].isDone()==false)
				{
					m_shatterDone=false;
				}  
			}
			if(m_shatterDone)
				m_onShatter=false;
		}
		else
		{
			m_yPos+= p_deltatime*m_ySpeed;
			UpdateBounds();
		}
	}
	public void Paint(Graphics g)
	{
//		g.drawRect(m_bounds, Color.LTGRAY);
		if(m_onShatter==false)
		{
			g.drawImage(AssetManager.sprites,m_xPos,m_yPos, Constants.GEM_IMG);
		}
		else
		{
			for(int idx=0;idx<m_shatter.length;idx++)
			{
				m_shatter[idx].Paint(g);
			}
		}
	}
	private void UpdateBounds()
	{
		m_bounds = new Rect((int)(m_xPos),(int)(m_yPos),(int)(m_xPos)+Constants.GEM_IMG.width,(int)(m_yPos)+Constants.GEM_IMG.height-4);
	}
	public Rect getBounds() { 
		return m_bounds;
	}
	public void disapear()
	{
		if(m_onShatter) return;
		m_onShatter = true;
		for(int idx=0;idx<m_shatter.length;idx++)
		{
			m_shatter[idx].show(m_xPos +m_bounds.width()/2,m_yPos+m_bounds.height());
		}
	}
	public boolean isOnShatter()
	{
		return m_onShatter;
	}
	public boolean isGone()
	{
		return m_shatterDone&&!m_onShatter;
	}

}
