package com.eugexstudios.lineswinger;

import java.util.Random;

import android.graphics.Color;
import android.graphics.Paint.Align; 

import com.eugexstudios.lineswinger.AskUserWindow.QUESTION;
import com.eugexstudios.lineswinger.GameScreen.LANGUAGE;  
import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.LanguageManager;
import com.eugexstudios.utils.genSet;
import com.eugexstudios.utils.LanguageManager.W;

public class SettingsWindow extends Screen{
 
	
	enum states {
		  	IDLE   
		  , SELECT_LANG 
		  , ASK_TO_RESET
		  , ABOUT
	}; 
	
	private final byte 
			  LANG_BTN   = 1
			, SOUND_BTN  = 2
			, MUSIC_BTN  = 3
			, VIB_BTN    = 4
			, LANG_BTN_1 = 5
			, LANG_BTN_2 = 7
			, LANG_BTN_3 = 8
			, LANG_BTN_4 = 9
			, LANG_BTN_5 = 10
			, LANG_BTN_6 = 11 
			, RESET_DATA  = 13
		    , ABOUT_BTN  = 14
		    , DISCOVER_BTN= 15
		    , RATE_BTN= 16
			, OKAY_SAVE_LANG_BTN   = 52
			, CANCEL_SAVE_LANG_BTN = 53
			;
	 
	final int BUTTON_GAP = Constants.ScaleY(0.109375f);
	final int BUTTON_LABEL_GAP = Constants.ScaleY(0.3395f);
	final int m_btnColor1 = Color.argb(160,88,20,92);  
	final int m_selectedLangColor = Color.argb(225,88,20,92);  
	final int m_btnColorPressed1 = Color.argb(220,88,20,92); 
	private final int BUTTON_HEIGHT =  Constants.ScaleY(0.088f);
	private final int BUTTON_WIDTH =  Constants.ScaleX(0.7f);
	private final int[][] CONST = new int[][]{
			 new int[]{(int)(CONFIG.SCREEN_WIDTH*0.15f),       (int)(CONFIG.SCREEN_HEIGHT*0.171875f)}  //0 LANG BTNS X, Y  
			,new int[]{(int)(CONFIG.SCREEN_WIDTH*0.7f),        (int)(CONFIG.SCREEN_HEIGHT*0.078125f)}  //1 LANG BTNS LEN, WID
			,new int[]{(int)(CONFIG.SCREEN_WIDTH*0.1f),        (int)(CONFIG.SCREEN_HEIGHT*0.117187f)}  //2 BTN WINDOW X,Y
			,new int[]{(int)(CONFIG.SCREEN_WIDTH*0.8f),        (int)(CONFIG.SCREEN_HEIGHT*0.76171f)}   //3 BTN WINDOW LEN WID 
			,new int[]{(int)((CONFIG.SCREEN_WIDTH*0.7f)/2)-10, (int)(CONFIG.SCREEN_HEIGHT*0.078125f)}  //4 LANG SAVE/CANCEL BTNS LEN WID 
			,new int[]{(int)(CONFIG.SCREEN_WIDTH * 0.15f),     (int)(CONFIG.SCREEN_HEIGHT*0.234f)}   //5 MAIN SETTINGS BTN X Y 
			, new int[]{0,                                     (int)(CONFIG.SCREEN_HEIGHT*0.08984375f)}  //6  LANG TEXT BTNS X, Y  
	};
	 
	private states					   m_state; 
	private int 					   m_languagePreviewIdx; 
	private boolean 				   m_isVisib; 
	private boolean                    m_onShowLogoEffect;
	private float[]                    m_logoEffect;
	private String                     m_version;
	private Random 					   m_rand;
	private AskUserWindow		       m_askWindow;
	private Backbutton                 m_backButton;
	
	public SettingsWindow()
	{
		m_isVisib     = true;
		m_state       = states.IDLE;
		m_rand        = new Random(); 
		m_version     = genSet.game.GetVersionName();
		m_backButton  = new Backbutton();
	}
	
	public boolean isVisib()
	{
		return m_isVisib;
	}
	
	public void Paint(Graphics g) {
   
		switch(m_state)
		{ 
			case IDLE:         PaintIdle(g);		 break;
			case SELECT_LANG:  PaintLangSettings(g); break;
			case ASK_TO_RESET: PaintAskToReset(g);   break;
			case ABOUT:        PaintAbout(g);        break;
		} 

		m_backButton.Paint(g);
	}
	
	private void PaintAskToReset(Graphics g)
	{ 
		PaintIdle(g);	
		m_askWindow.Paint(g); 		
	}

	private void PaintIdle(Graphics g)
	{   
//		 g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(130, 237, 28, 36)); //Red 
		 g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(55, 0, 0, 0)); //BLACK 
		 g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(75, 88, 20, 92)); //Red 

	 	 drawRoundRect(g, Constants.ScaleX(0.1f), Constants.ScaleY(0.151f)- Constants.ScaleY(0.7f)/2, 	       Constants.ScaleX(0.8f),   Constants.ScaleY(0.7f),      Color.WHITE,m_btnColorPressed1, -1); 

		 genSet.setTextProperties( Constants.SETTINGS_LS, Color.argb(45, 0, 0, 0), Align.CENTER);  
		 g.drawStringFont(W.SETTINGS, CONFIG.SCREEN_MID+5, (int)(CONFIG.SCREEN_HEIGHT * 0.21f)+Constants.SETTINGS_LS/2+5, AssetManager.Comforta,genSet.paint);  
		 genSet.setTextProperties( Constants.SETTINGS_LS, Color.DKGRAY, Align.CENTER);  
		 g.drawStringFont(W.SETTINGS, CONFIG.SCREEN_MID, (int)(CONFIG.SCREEN_HEIGHT * 0.21)+Constants.SETTINGS_LS/2, AssetManager.Comforta,genSet.paint);  
		   
	 	 drawRoundRect(g, CONST[5][0], CONST[5][1], 	           BUTTON_WIDTH,   BUTTON_HEIGHT,      m_btnColor1,m_btnColorPressed1, LANG_BTN); 
	 	 drawRoundRect(g, CONST[5][0]+6, CONST[5][1]-7, 	       BUTTON_WIDTH,   BUTTON_HEIGHT,      m_btnColor1,m_btnColorPressed1, LANG_BTN); 

	 	 drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*1, 	   BUTTON_WIDTH,   BUTTON_HEIGHT,  m_btnColor1,m_btnColorPressed1, VIB_BTN); 
	 	 drawRoundRect(g, CONST[5][0]-6, CONST[5][1]+BUTTON_GAP*1+6, 	   BUTTON_WIDTH,   BUTTON_HEIGHT,  m_btnColor1,m_btnColorPressed1, VIB_BTN); 

	 	 drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*2, 	   BUTTON_WIDTH,   BUTTON_HEIGHT,  m_btnColor1,m_btnColorPressed1, SOUND_BTN); 
	 	 drawRoundRect(g, CONST[5][0]-7, CONST[5][1]+BUTTON_GAP*2+8, 	   BUTTON_WIDTH,   BUTTON_HEIGHT,  m_btnColor1,m_btnColorPressed1, SOUND_BTN); 

	 	 drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*3, 	   BUTTON_WIDTH ,  BUTTON_HEIGHT,  m_btnColor1,m_btnColorPressed1, MUSIC_BTN);
	 	 drawRoundRect(g, CONST[5][0]+6, CONST[5][1]+BUTTON_GAP*3-7, 	   BUTTON_WIDTH ,  BUTTON_HEIGHT,  m_btnColor1,m_btnColorPressed1, MUSIC_BTN);

		 drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*4, 	   BUTTON_WIDTH ,  BUTTON_HEIGHT,  m_btnColor1,m_btnColorPressed1, ABOUT_BTN); 
		 drawRoundRect(g, CONST[5][0]-7, CONST[5][1]+BUTTON_GAP*4+9, 	   BUTTON_WIDTH ,  BUTTON_HEIGHT,  m_btnColor1,m_btnColorPressed1, ABOUT_BTN); 
		 	
		 genSet.setTextProperties( Constants.SETTINGS_BTN1_S, Color.WHITE, Align.CENTER);   
		 if(GameScreen.GetLanguage()!=LANGUAGE.ENGLISH)
			 g.drawStringFont("Language (" + LanguageManager.GetLocalized(W.LANGUAGE) + ")" ,  CONFIG.SCREEN_MID, (int)(CONFIG.SCREEN_HEIGHT * 0.302f),	    AssetManager.Comforta, genSet.paint);
		 else
			 g.drawStringFont(LanguageManager.GetLocalized(W.LANGUAGE),  CONFIG.SCREEN_MID, (int)(CONFIG.SCREEN_HEIGHT * 0.302f),	    AssetManager.Comforta, genSet.paint); 
		 
		 g.drawStringFont(W.VIBRATION, CONFIG.SCREEN_MID, (int)(CONFIG.SCREEN_HEIGHT*0.3f)+BUTTON_GAP*1, AssetManager.Comforta, genSet.paint);
		 g.drawStringFont(W.SOUND,     CONFIG.SCREEN_MID, (int)(CONFIG.SCREEN_HEIGHT*0.3f)+BUTTON_GAP*2, AssetManager.Comforta, genSet.paint);
		 g.drawStringFont(W.MUSIC, 	   CONFIG.SCREEN_MID, (int)(CONFIG.SCREEN_HEIGHT*0.3f)+BUTTON_GAP*3, AssetManager.Comforta, genSet.paint); 
			 
 
		 genSet.setTextProperties( Constants.SETTINGS_BTN_S, Color.WHITE, Align.CENTER);  
		 g.drawStringFont(GameScreen.GetLanguageT(),  		    CONFIG.SCREEN_MID, BUTTON_LABEL_GAP, 	       AssetManager.Comforta, genSet.paint);
		 g.drawStringFont(GameScreen.IsVibrateOn()?W.ON:W.OFF,  CONFIG.SCREEN_MID, BUTTON_LABEL_GAP+BUTTON_GAP*1, AssetManager.Comforta, genSet.paint);
		 g.drawStringFont(GameScreen.IsSoundOn()?W.ON:W.OFF,    CONFIG.SCREEN_MID, BUTTON_LABEL_GAP+BUTTON_GAP*2, AssetManager.Comforta, genSet.paint);
		 g.drawStringFont(GameScreen.IsMusicOn()?W.ON:W.OFF,    CONFIG.SCREEN_MID, BUTTON_LABEL_GAP+BUTTON_GAP*3, AssetManager.Comforta, genSet.paint);
		 g.drawStringFont(W.ABOUT,CONFIG.SCREEN_MID,                          BUTTON_LABEL_GAP+BUTTON_GAP*4, AssetManager.Comforta, genSet.paint);
		  
//		 PaintManualBackBtn(g);
	} 
	
	public void PaintAbout(Graphics g)
	{
		 g.drawRect(Constants.SCREEN_RECT, Color.rgb(115, 168, 236));     
		 g.drawRect(Constants.SCREEN_RECT, Color.argb(200 , 193, 44, 170));    
		 g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(70, 237, 28, 36)); //Red 

		 if(m_onShowLogoEffect)
		 { 
			 g.drawImage(AssetManager.sprites, m_logoEffect[0],m_logoEffect[1], Constants.LOGO_SMALL_2);
			 g.drawImage(AssetManager.sprites, m_logoEffect[2],m_logoEffect[3], Constants.LOGO_SMALL_3);
			 g.drawImage(AssetManager.sprites, m_logoEffect[1],m_logoEffect[2], Constants.LOGO_SMALL);
		 }
		 else
		 {
			 g.drawImage(AssetManager.sprites, Constants.LOGO_SMALL);
		 }

		genSet.setTextProperties(30,  Color.WHITE,  Align.RIGHT); 
		g.drawStringFont("e  u  g  e", CONFIG.SCREEN_MID -43, CONFIG.SCREEN_HEIGHT/2-40-80, AssetManager.Comforta,genSet.paint);
			
		genSet.setTextProperties(120,  Color.WHITE,  Align.CENTER);  
		g.drawStringFont("X", CONFIG.SCREEN_MID , CONFIG.SCREEN_HEIGHT/2-80, AssetManager.Comforta,genSet.paint);
			
		genSet.setTextProperties(30,  Color.WHITE,  Align.LEFT); 
		g.drawStringFont("s t u d i o s", CONFIG.SCREEN_MID +40, CONFIG.SCREEN_HEIGHT/2-40-80, AssetManager.Comforta,genSet.paint);

		genSet.setTextProperties(30,  Color.argb(130, 255, 255,255),  Align.CENTER); 
		g.drawString(m_version, CONFIG.SCREEN_MID+203,458, genSet.paint);

		genSet.setTextProperties(30,  Color.WHITE,  Align.CENTER);  
		g.drawStringFont(W.PLAY_MORE_FROM,  CONFIG.SCREEN_MID,Constants.DISCOVER_BTN.y+33, AssetManager.Comforta,genSet.paint);
		drawRoundRect(g, Constants.DISCOVER_BTN, m_btnColor1, m_btnColorPressed1, DISCOVER_BTN);
		drawRoundRect(g, 4,6,Constants.DISCOVER_BTN, m_btnColor1, m_btnColorPressed1, DISCOVER_BTN);

		g.drawStringFont(W.DID_YOU_LIKE ,  CONFIG.SCREEN_MID,Constants.RATE_BTN.y+33, AssetManager.Comforta,genSet.paint);
		drawRoundRect(g, Constants.RATE_BTN, m_btnColor1, m_btnColorPressed1, RATE_BTN);
		drawRoundRect(g, 5,-6,Constants.RATE_BTN, m_btnColor1, m_btnColorPressed1, RATE_BTN); 

		genSet.setTextProperties(50,  Color.WHITE,  Align.CENTER);  
		g.drawStringFont(W.EXPLORE_GAMES ,  CONFIG.SCREEN_MID,Constants.DISCOVER_BTN.y+Constants.DISCOVER_BTN.height+20, AssetManager.Comforta,genSet.paint);
		g.drawStringFont(W.RATE_US ,  CONFIG.SCREEN_MID,Constants.RATE_BTN.y+Constants.RATE_BTN.height+14, AssetManager.Comforta,genSet.paint);
		
	}
	 
	private void ShowLogoEffect()
	{
		 m_onShowLogoEffect = true;
		 m_logoEffect = new float[5];
		 m_logoEffect[0] = m_rand.nextFloat()+(m_rand.nextInt(14)+3);
		 m_logoEffect[1] = m_rand.nextFloat()+(m_rand.nextInt(10)+1);
		 m_logoEffect[2] = m_rand.nextFloat()+(m_rand.nextInt(13)+3);
		 m_logoEffect[3] = m_rand.nextFloat()+(m_rand.nextInt(10)+1);
		 
		 m_logoEffect[0] = m_rand.nextBoolean()? - m_logoEffect[0]: m_logoEffect[0];
		 m_logoEffect[1] = m_rand.nextBoolean()? - m_logoEffect[1]: m_logoEffect[1];
		 m_logoEffect[2] = m_rand.nextBoolean()? - m_logoEffect[2]: m_logoEffect[2];
		 m_logoEffect[3] = m_rand.nextBoolean()? - m_logoEffect[3]: m_logoEffect[3];
	}
	 
	private void PaintLangSettings(Graphics g) 
	{   
		PaintIdle(g);
		g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(55, 0, 0, 0)); //BLACK 
		g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(75, 88, 20, 92)); //Red 
		g.drawRoundRect( CONST[2][0],CONST[2][1],CONST[3][0],CONST[3][1],20,20,  Color.WHITE);  //BTN WINDOW 

		genSet.setTextProperties(Constants.SETT_LANG_LS, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont(LanguageManager.GetLocalized(W.LANGUAGE), CONFIG.SCREEN_MID, Constants.ScaleY(0.17578125f), AssetManager.Comforta,genSet.paint); 

		genSet.setTextProperties(Constants.SETT_LANG_BTN_LS, Color.WHITE, Align.CENTER);  
		drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*0), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.ENGLISH.getIdx()?     m_selectedLangColor:m_btnColor1, m_selectedLangColor, LANG_BTN_1);  
		drawRoundRect(g, CONST[0][0]-7, CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*0)-5, CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.ENGLISH.getIdx()?     m_selectedLangColor:m_btnColor1, m_selectedLangColor, LANG_BTN_1);  
		
		drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*1), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.JAPANESE.getIdx()?    m_selectedLangColor:m_btnColor1, m_selectedLangColor, LANG_BTN_2);  
		drawRoundRect(g, CONST[0][0]+6, CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*1)+7, CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.JAPANESE.getIdx()?    m_selectedLangColor:m_btnColor1, m_selectedLangColor, LANG_BTN_2);  

		drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*2), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.SPANISH.getIdx()?     m_selectedLangColor:m_btnColor1, m_selectedLangColor, LANG_BTN_3);  
		drawRoundRect(g, CONST[0][0]-7, CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*2)+6, CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.SPANISH.getIdx()?     m_selectedLangColor:m_btnColor1, m_selectedLangColor, LANG_BTN_3);  
		
		drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*3), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.GERMAN.getIdx()?      m_selectedLangColor:m_btnColor1,  m_selectedLangColor, LANG_BTN_4);  
		drawRoundRect(g, CONST[0][0]+8, CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*3)+7, CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.GERMAN.getIdx()?      m_selectedLangColor:m_btnColor1,  m_selectedLangColor, LANG_BTN_4);  
		
		drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*4), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.KOREAN.getIdx()?      m_selectedLangColor:m_btnColor1, m_selectedLangColor, LANG_BTN_5);  
		drawRoundRect(g, CONST[0][0]+6, CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*4)+7, CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.KOREAN.getIdx()?      m_selectedLangColor:m_btnColor1, m_selectedLangColor, LANG_BTN_5);  
		
		drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*5), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.PORTUGUESE.getIdx()?  m_selectedLangColor:m_btnColor1,  m_selectedLangColor, LANG_BTN_6);  
		drawRoundRect(g, CONST[0][0]-7, CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*5)-5, CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.PORTUGUESE.getIdx()?  m_selectedLangColor:m_btnColor1, m_selectedLangColor, LANG_BTN_6);  
		
		g.drawStringFont(LanguageManager.GetTranslation(W.ENGLISH,    LANGUAGE.ENGLISH),    CONFIG.SCREEN_MID, CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*0), AssetManager.Comforta, genSet.paint); 	
		g.drawStringFont(LanguageManager.GetTranslation(W.JAPANESE,   LANGUAGE.JAPANESE),   CONFIG.SCREEN_MID, CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*1), AssetManager.Comforta, genSet.paint); 
		g.drawStringFont(LanguageManager.GetTranslation(W.SPANISH,    LANGUAGE.SPANISH),    CONFIG.SCREEN_MID, CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*2), AssetManager.Comforta, genSet.paint);  
		g.drawStringFont(LanguageManager.GetTranslation(W.GERMAN,     LANGUAGE.GERMAN),     CONFIG.SCREEN_MID, CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*3), AssetManager.Comforta, genSet.paint);  
		g.drawStringFont(LanguageManager.GetTranslation(W.KOREAN,     LANGUAGE.KOREAN),     CONFIG.SCREEN_MID, CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*4), AssetManager.Comforta, genSet.paint);  
		g.drawStringFont(LanguageManager.GetTranslation(W.PORTUGUESE, LANGUAGE.PORTUGUESE), CONFIG.SCREEN_MID, CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*5), AssetManager.Comforta, genSet.paint);  

		drawRoundRect(g, CONST[0][0],                CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*6), CONST[4][0], CONST[4][1], m_btnColor1, m_btnColorPressed1, OKAY_SAVE_LANG_BTN);  
		drawRoundRect(g, CONST[0][0]-6,                CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*6)+7, CONST[4][0], CONST[4][1], m_btnColor1, m_btnColorPressed1, OKAY_SAVE_LANG_BTN);  
		
		drawRoundRect(g, CONST[0][0]+CONST[4][0]+20, CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*6), CONST[4][0], CONST[4][1], m_btnColor1, m_btnColorPressed1, CANCEL_SAVE_LANG_BTN);  
		drawRoundRect(g, CONST[0][0]+CONST[4][0]+20+7, CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*6)+6, CONST[4][0], CONST[4][1], m_btnColor1, m_btnColorPressed1, CANCEL_SAVE_LANG_BTN);  
		 
		g.drawStringFont(LanguageManager.GetLocalized(W.SAVE),   CONST[0][0]+CONST[4][0]/2,                CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*6),  AssetManager.Comforta, genSet.paint);  
		g.drawStringFont(LanguageManager.GetLocalized(W.CANCEL), CONST[0][0]+CONST[4][0]+20+CONST[4][0]/2, CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*6),  AssetManager.Comforta, genSet.paint);  
		  
	}
 
	public void Updates(float p_deltaTime) 
	{ 

		if(m_backButton.isPressed())
		{ 
			backButton(); 
		}
		switch(m_state)
		{
			 
			case IDLE:     break;
			case ABOUT:  
				 if(!m_onShowLogoEffect)
				  {
					  m_onShowLogoEffect = m_rand.nextInt(200)==1;
					  if(m_onShowLogoEffect)
					  {
						  ShowLogoEffect();
					  }
				  }
				  else
				  {
					  if(m_logoEffect[4]<10)
						  m_logoEffect[4]+=1*p_deltaTime;
					  else
						  m_onShowLogoEffect=false;
				  }
				
				break;
			case ASK_TO_RESET:
				 if(m_askWindow.isClosed()) 
 				 {
 					 m_state = states.IDLE;
 				 }
				 break;
			case SELECT_LANG:  break;
		} 
		
	}

	@Override
	public void TouchUpdates(TouchEvent g) 
	{ 
		m_backButton.TouchUpdates(g);
		switch(m_state)
		{
			case ABOUT:AboutBtnUpdates(g); break;
			case IDLE: IdleButtonUpdates(g); break;
			case SELECT_LANG:LangBtnUpdates(g); break;
			case ASK_TO_RESET: m_askWindow.TouchUpdates(g);break;
			default:
				break;
		}

		if(g.type == TouchEvent.TOUCH_DOWN &&buttonPressed!=0)  
			AssetManager.click.play(1.0f);
	}

	public void AboutBtnUpdates(TouchEvent g)
	{
		
		if(g.type == TouchEvent.TOUCH_DOWN)
		{ 
			drawRoundRect(g,  Constants.DISCOVER_BTN, m_btnColor1, m_btnColorPressed1, DISCOVER_BTN);
			drawRoundRect(g,  Constants.RATE_BTN, m_btnColor1, m_btnColorPressed1, RATE_BTN); 
		}
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case DISCOVER_BTN:  genSet.game.GoToPublisherPage();   break; 
				case RATE_BTN: GameScreen.RateApp();  break;  
			}
			buttonPressed=0;
		} 
	}
	
	public void IdleButtonUpdates(TouchEvent g)
	{
		
		if(g.type == TouchEvent.TOUCH_DOWN)
		{
			drawRoundRect(g, CONST[5][0], CONST[5][1],              BUTTON_WIDTH, BUTTON_HEIGHT, Color.WHITE, Color.rgb(200, 200, 200), LANG_BTN);     
			drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*1, BUTTON_WIDTH, BUTTON_HEIGHT, Color.WHITE, Color.rgb(200, 200, 200), VIB_BTN);   
			drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*2, BUTTON_WIDTH, BUTTON_HEIGHT, Color.WHITE, Color.rgb(200, 200, 200), SOUND_BTN);     
			drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*3, BUTTON_WIDTH, BUTTON_HEIGHT, Color.WHITE, Color.rgb(200, 200, 200), MUSIC_BTN);  
			drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*4, BUTTON_WIDTH, BUTTON_HEIGHT, Color.WHITE, Color.rgb(200, 200, 200), ABOUT_BTN);   
//			drawRoundRect(g, (int)(Constants.BACK_BTN_CIR*0.2f), -(int)(Constants.BACK_BTN_CIR*0.8f), (int)(Constants.BACK_BTN_CIR*1.0f),(int)( Constants.BACK_BTN_CIR*1.5f),  Color.rgb(200, 200, 200), Color.rgb(200, 200, 200), BACK_BTN);     

		}
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case LANG_BTN:   
					m_languagePreviewIdx= GameScreen.GetLanguage().getIdx();
					m_state = states.SELECT_LANG;	 break; 
				case SOUND_BTN: GameScreen.ToggleSoundSettings();  break; 
				case MUSIC_BTN: GameScreen.ToggleMusicSettings();  break; 
				case VIB_BTN:  	GameScreen.ToggleVirbrateSettings();break;  
				case RESET_DATA:
					m_askWindow = new AskUserWindow(QUESTION.RESET_DATA);
					m_state = states.ASK_TO_RESET;
					break;
				case ABOUT_BTN: 
					m_state = states.ABOUT;
					break;	 
			}
			buttonPressed=0;
		} 
	}

	private void LangBtnUpdates(TouchEvent g)
	{

		if(g.type == TouchEvent.TOUCH_DOWN)
		{ 
			drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*0), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.ENGLISH.getIdx()?Color.rgb(200, 200, 200):Color.WHITE,Color.rgb(200, 200, 200),    LANG_BTN_1);  
			drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*1), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.JAPANESE.getIdx()?Color.rgb(200, 200, 200):Color.WHITE,Color.rgb(200, 200, 200),   LANG_BTN_2);  
			drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*2), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.SPANISH.getIdx()?Color.rgb(200, 200, 200):Color.WHITE,Color.rgb(200, 200, 200),    LANG_BTN_3);  
			drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*3), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.GERMAN.getIdx()?Color.rgb(200, 200, 200):Color.WHITE,Color.rgb(200, 200, 200),     LANG_BTN_4);  
			drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*4), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.KOREAN.getIdx()?Color.rgb(200, 200, 200):Color.WHITE,Color.rgb(200, 200, 200),     LANG_BTN_5);  
			drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*5), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.PORTUGUESE.getIdx()?Color.rgb(200, 200, 200):Color.WHITE,Color.rgb(200, 200, 200), LANG_BTN_6);  
			drawRoundRect(g, CONST[0][0],                                        CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*6), CONST[4][0], CONST[4][1], Color.WHITE,Color.rgb(200, 200, 200), OKAY_SAVE_LANG_BTN);  
			drawRoundRect(g, CONST[0][0]+(int)((CONFIG.SCREEN_WIDTH*0.7f)/2)+20, CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*6), CONST[4][0], CONST[4][1], Color.WHITE,Color.rgb(200, 200, 200), CANCEL_SAVE_LANG_BTN); 
		}
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
			case LANG_BTN_1: m_languagePreviewIdx = GameScreen.LANGUAGE.ENGLISH.getIdx();  break;
			case LANG_BTN_2: m_languagePreviewIdx = GameScreen.LANGUAGE.JAPANESE.getIdx();  break;
			case LANG_BTN_3: m_languagePreviewIdx = GameScreen.LANGUAGE.SPANISH.getIdx();  break;
			case LANG_BTN_4: m_languagePreviewIdx = GameScreen.LANGUAGE.GERMAN.getIdx();  break;
			case LANG_BTN_5: m_languagePreviewIdx = GameScreen.LANGUAGE.KOREAN.getIdx();  break;
			case LANG_BTN_6: m_languagePreviewIdx = GameScreen.LANGUAGE.PORTUGUESE.getIdx();  break;
			case OKAY_SAVE_LANG_BTN: 
				
				GameScreen.SetLanguage(GameScreen.LANGUAGE.values()[m_languagePreviewIdx]); 
				 m_state = states.IDLE;
				 break;
			case CANCEL_SAVE_LANG_BTN:  m_state = states.IDLE;	break; 
			}

			buttonPressed=0;
		} 
	}
	
	public boolean isIdled()
	{
		return m_state ==states.IDLE;
	}
	
	@Override
	public void backButton() 
	{ 
		switch(m_state)
		{ 
			case IDLE: m_isVisib=false;break;
			case SELECT_LANG: m_state = states.IDLE; break;
			case ASK_TO_RESET: m_state = states.IDLE;break;
			case ABOUT: m_state = states.IDLE;break;
			default:
				break;
		}
  
	}
	 

}
