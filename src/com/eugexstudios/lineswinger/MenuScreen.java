package com.eugexstudios.lineswinger;
     
import java.util.Random;

import android.graphics.Color;   

import com.eugexstudios.adprovider.AdProvider;  
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent; 
import com.eugexstudios.lineswinger.AskUserWindow.QUESTION; 
import com.eugexstudios.utils.ColorUtil;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.genSet;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;
public class MenuScreen extends Screen
{   
	 
		
     public	enum State { 
		  IDLE 
		, ASK_EXIT 
		, SETTINGS_MENU 
		, ENTRY  
		, AD_SHOW
		, AD_OPENLOAD
		, SAY_UPDATES
		, ON_SHOP
		 
	}; 

	final byte 
		  START_BTN            = 1
		, SETTINGS_BTN         = 2
		, REMOVE_ADS_BTN       = 3 
		, SHARE_APP_BTN        = 4
		, OPEN_FB_PAGE_BTN     = 5
		, SEND_FEEDBACK_BTN    = 6  
		, SEND_BETA_RESULT     = 9 
		, GOTO_PUBLISHER_PAGE  = 10
		, AD_BTN               = 11 
		, GIFT_BTN            = 12 
				, STORE_BTN         = 13
				;
	
	private final int AD_POS_Y       = 65;
	private final int AD_ICON_SIZE   = 95;
	private final int AD_MARGIN_SIZE = 6;
	 
	private final int                  ANIM_SIZE =5;  
	private AskUserWindow              m_askWindow;
	private SettingsWindow 			   m_settings;
	private static State		       m_state;     
	private AdProvider 				   m_adProvider;  
	private boolean    				   m_adShown;  
	private float					   m_anim[]; 
	private int                        m_yAdjust;
	private Random                     m_rand;
	private boolean 			       m_onBGEffect;
	private int[]                      m_bgEffect;
	private boolean                    m_onShowLogoEffect;
	private float[]                    m_logoEffect;
	private ShopScreen                 m_shopScreen;
	 
	public MenuScreen()
	{   
		 m_yAdjust         = 0; 
	     m_state           = State.ENTRY;   
	     resetAnim(); 
	     m_adProvider      = new AdProvider();   
	     m_adShown         = false;   
	     m_adProvider.RequestNativeAd(Constants.NATIVE_AD_MENU);  
	     m_adProvider.RequestOfferWallAd(); 
	     m_rand = new Random();
	     m_settings        = new SettingsWindow(); 
	     ShowLogoEffect();  
	      
	}  	 
	 
	@Override
	public void Paint(Graphics g)
	{  
		g.drawRect(Constants.SCREEN_RECT, ColorUtil.BG1_LAYER1);    
		if(m_onShowLogoEffect)
			g.drawRect(Constants.SCREEN_RECT, Color.argb(210, 193, 44, 170));   
		else
			g.drawRect(Constants.SCREEN_RECT, Color.argb(220 + (m_onBGEffect?m_bgEffect[1]:0), 193, 44, 170));   
		
	 	switch(m_state)
		{ 
			case ENTRY:			  PaintMenuIdle(g);    break;
	 		case IDLE: 			  PaintMenuIdle(g);    break;  
	 		case AD_SHOW: 	      PaintAdShow(g);      break;  
			case ASK_EXIT: 		  PaintAskToExit(g);   break;
			case AD_OPENLOAD:	  PaintMenuIdle(g);    break;
			case SETTINGS_MENU:	  PaintSettings(g);	   break;  
			case ON_SHOP: 		  PaintShop(g); break;
		    default: break; 
		}  
	}   
	
	private void PaintShop(Graphics g)
	{
		PaintMenuIdle(g);
		m_shopScreen.Paint(g); 
	}
	
	private void PaintSettings(Graphics g)
	{
		PaintMenuIdle(g);
		m_settings.Paint(g);
	}
	
	private void PaintAskToExit(Graphics g)
	{
		PaintMenuIdle(g); 
		m_askWindow.Paint(g);	 
	}
 
	private void GoToShop()
	{
		m_state      = State.ON_SHOP;
		m_shopScreen = new ShopScreen();
	}
	
	private void ShowBGEffect()
	{
		m_onBGEffect = true;
		m_bgEffect = new int[2];
		m_bgEffect[0] = (m_rand.nextInt(10)+15); //Duration of effect
		m_bgEffect[1] = 5+(m_rand.nextInt(30)); // Additional opacity for background effect 
	}
	
	private void ShowLogoEffect()
	{
		 m_onShowLogoEffect = true;
		 m_logoEffect = new float[5];
		 m_logoEffect[0] = m_rand.nextFloat()+(m_rand.nextInt(14)+3);
		 m_logoEffect[1] = m_rand.nextFloat()+(m_rand.nextInt(10)+1);
		 m_logoEffect[2] = m_rand.nextFloat()+(m_rand.nextInt(13)+3);
		 m_logoEffect[3] = m_rand.nextFloat()+(m_rand.nextInt(10)+1);
		 
		 m_logoEffect[0] = m_rand.nextBoolean()? - m_logoEffect[0]: m_logoEffect[0];
		 m_logoEffect[1] = m_rand.nextBoolean()? - m_logoEffect[1]: m_logoEffect[1];
		 m_logoEffect[2] = m_rand.nextBoolean()? - m_logoEffect[2]: m_logoEffect[2];
		 m_logoEffect[3] = m_rand.nextBoolean()? - m_logoEffect[3]: m_logoEffect[3];
	}
	
	private void PaintLogoEffect(Graphics g)
	{
		g.drawImage(AssetManager.sprites,(int)(60+m_logoEffect[0]),(int)(183+m_logoEffect[1]) ,0,157+300,614,300); // LOGO
		g.drawImage(AssetManager.sprites,(int)(60+m_logoEffect[2]),(int)(183+m_logoEffect[3]) ,0,157+300*2,614,300); // LOGO
		g.drawImage(AssetManager.sprites,(int)(60+m_logoEffect[1]),(int)(183+m_logoEffect[2]) ,0,157,614,300); // LOGO
	}
	
	private void PaintMenuIdle(Graphics g)
	{      
//		if(m_adProvider.isOfferWallReady())
//		{
//			g.drawCircle(CONFIG.SCREEN_WIDTH-10,  m_yAdjust+Constants.ScaleY(0.2f), 5, Color.RED);
//		}
		
		if(m_adShown)
		{
			drawRoundRect(g, 15-50,  m_yAdjust+AD_POS_Y-AD_MARGIN_SIZE-AD_ICON_SIZE/2-AD_MARGIN_SIZE, 50+AD_ICON_SIZE+AD_MARGIN_SIZE*2,  AD_ICON_SIZE+AD_MARGIN_SIZE*2,   ColorUtil.SetOpacity(0.4f , Color.DKGRAY), ColorUtil.SetOpacity(0.65f , Color.DKGRAY),  AD_BTN);
		    g.drawImage(m_adProvider.GetNativeAd2ndBitmap(), 15,  m_yAdjust+AD_POS_Y, (float)((float)AD_ICON_SIZE/(float)m_adProvider.GetNativeAd2ndBitmap().getWidth()));
	 	}  

		if(m_onShowLogoEffect)
		{
			PaintLogoEffect(g);
		}
		else
		{
			g.drawImage(AssetManager.sprites,60,183 ,0,157,614,300); // LOGO
		}

		drawRoundRect(g, 8,9, Constants.START_BTN_DATA, Color.argb(110,88,20,92), Color.argb(150,88,20,92),  START_BTN);
		drawRoundRect(g, Constants.START_BTN_DATA, Color.argb(140,88,20,92), Color.argb(160,88,20,92),  START_BTN);
		g.drawImage(AssetManager.sprites,0, Constants.START_BTN_DATA.height/2,Constants.START_BTN_IMG); // play icon 

		drawRoundRect(g, -3,-4, Constants.GIFT_BTN_DATA, Color.argb(105,88,20,92), Color.argb(150,88,20,92),  GIFT_BTN);
		drawRoundRect(g, Constants.GIFT_BTN_DATA, Color.argb(150,88,20,92), Color.argb(180,88,20,92),  GIFT_BTN);
		g.drawImage(AssetManager.sprites, Constants.GIFT_BTN_IMG);  
		
		drawRoundRect(g, -4,3, Constants.SETTINGS_BTN_DATA, Color.argb(105,88,20,92), Color.argb(150,88,20,92),  SETTINGS_BTN);
		drawRoundRect(g, Constants.SETTINGS_BTN_DATA, Color.argb(150,88,20,92), Color.argb(180,88,20,92),  SETTINGS_BTN);
		g.drawImage(AssetManager.sprites, Constants.SETTINGS_BTN_IMG);  
		
		drawRoundRect(g, 4,3, Constants.STORE_BTN_DATA, Color.argb(105,88,20,92), Color.argb(150,88,20,92),  STORE_BTN);
		drawRoundRect(g, Constants.STORE_BTN_DATA, Color.argb(150,88,20,92), Color.argb(180,88,20,92),  STORE_BTN);
		g.drawImage(AssetManager.sprites, Constants.STORE_BTN_DATA.width/2, Constants.STORE_BTN_DATA.height/2,Constants.STORE_BTN_IMG);  
		
		
		
	}
	
	private void PaintAdShow(Graphics g)
	{
		PaintMenuIdle(g);  
		int animX= -(int)((1-m_anim[0])*(AD_ICON_SIZE+50));
		drawRoundRect(g, 15-50+animX, m_yAdjust+AD_POS_Y-AD_MARGIN_SIZE-AD_ICON_SIZE/2-AD_MARGIN_SIZE, 50+AD_ICON_SIZE+AD_MARGIN_SIZE*2,  AD_ICON_SIZE+AD_MARGIN_SIZE*2,   ColorUtil.SetOpacity(0.4f , Color.DKGRAY), ColorUtil.SetOpacity(0.65f , Color.DKGRAY),  AD_BTN);
		g.drawImage(m_adProvider.GetNativeAd2ndBitmap(), 15+animX, m_yAdjust+AD_POS_Y, (float)((float)AD_ICON_SIZE/(float)m_adProvider.GetNativeAd2ndBitmap().getWidth()));
	 	  	
	}
	  
	private void resetAnim()
	{
		m_anim = new float[ANIM_SIZE];
		for(int idx=0;idx<m_anim.length;idx++)
		{
			m_anim[idx] = 0;
		}
	}
 
	public static void GoToIdle()
	{ 
	    m_state = State.IDLE; 
	}
	
    public static State GetState()
	{ 
    	return m_state;
	}
    
	private void MenuButtonUpdates(TouchEvent g)
	{  
		if(g.type == TouchEvent.TOUCH_DOWN)
		{ 
			if(m_adShown)
			{   	
				drawRoundRect(g, 15-50,  m_yAdjust+AD_POS_Y-AD_MARGIN_SIZE-AD_ICON_SIZE/2-AD_MARGIN_SIZE, 50+AD_ICON_SIZE+AD_MARGIN_SIZE*2,  AD_ICON_SIZE+AD_MARGIN_SIZE*2,   ColorUtil.SetOpacity(0.4f , Color.DKGRAY), ColorUtil.SetOpacity(0.65f , Color.DKGRAY),  AD_BTN);
				   
			 } 
			drawRoundRect(g, Constants.START_BTN_DATA,    Color.argb(100,88,20,92), Color.rgb(88,20,92),       START_BTN); 
			drawRoundRect(g, Constants.SETTINGS_BTN_DATA, Color.argb(130,88,20,92), Color.argb(160,88,20,92),  SETTINGS_BTN);
			drawRoundRect(g, Constants.GIFT_BTN_DATA,     Color.argb(130,88,20,92), Color.argb(160,88,20,92),  GIFT_BTN);
			drawRoundRect(g, Constants.STORE_BTN_DATA,    Color.argb(140,88,20,92), Color.argb(160,88,20,92),  STORE_BTN);
		 }
		
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
			case STORE_BTN:GoToShop(); break;
			case GIFT_BTN: 
				if(m_adProvider.isOfferWallReady())
				{
					m_adProvider.ShowOfferWall(
							new AdDisplayListener() {
							    @Override
							    public void adHidden(Ad ad) {
							    }
							    @Override
							    public void adDisplayed(Ad ad) {
							    }
							    @Override
							    public void adClicked(Ad ad) {
							    }
							    @Override
							    public void adNotDisplayed(Ad ad) {
							    }
							}
							);
				}
				break;
			    case AD_BTN:   
					m_state = State.AD_OPENLOAD;   
				    genSet.game.ShowDialog("Loading. Please wait...");
			    	m_adProvider.SendClick(); 
			    	buttonPressed = 0;
			    	break;
				case START_BTN:    GameScreen.GoToPlay(); break;
				case SETTINGS_BTN: GoToSettings(); break;
				case REMOVE_ADS_BTN: 
					genSet.game.SendBetaDatas();
//					GameScreen.RemoveAdRequest();  
				break; 
				case SHARE_APP_BTN: genSet.game.ShareApp(0);break;
				case OPEN_FB_PAGE_BTN: genSet.game.OpenFacebookPage(); break;
				case SEND_FEEDBACK_BTN: genSet.game.SendEmailFeedback(); break; 
				
			}  
			
			buttonPressed=0;
		} 
	} 

	private void WaitForBanner()
	{
		if(m_yAdjust==0&&genSet.game.IsBannerAdEnabled())
	    {
	    	m_yAdjust = genSet.game.GetBannerHeight()-40;
	    }
	}

	private void IdleUpdates(float p_deltaTime)
	{
		  WaitForBanner();  
		  if(!m_onBGEffect)
		  {
			  m_onBGEffect = m_rand.nextInt(1000)==1;
			  if(m_onBGEffect)
			  {
				  ShowBGEffect();
			  }
		  }
		  else
		  {
			  if(m_bgEffect[0]>0)
				  m_bgEffect[0]-=1*p_deltaTime;
			  else
				  m_onBGEffect=false;  
		  }
		  
		  if(!m_onShowLogoEffect)
		  {
			  m_onShowLogoEffect = m_rand.nextInt(200)==1;
			  if(m_onShowLogoEffect)
			  {
				  ShowLogoEffect();
			  }
		  }
		  else
		  {
			  if(m_logoEffect[4]<10)
				  m_logoEffect[4]+=1*p_deltaTime;
			  else
				  m_onShowLogoEffect=false;
		  }
	}
	
	private void SettingsUpdates(float p_deltaTime)
	{
		m_settings.Updates(p_deltaTime);
		IdleUpdates(p_deltaTime);

		if(m_settings.isVisib()==false)
		{
			m_state = State.ENTRY;
		}
	}
	
	private void GoToSettings()
	{
		m_state = State.SETTINGS_MENU;
		m_settings = new SettingsWindow();  
	}
	
 	@Override
	public void Updates(float p_deltaTime) 
 	{  
 		switch(m_state)
 		{
 		    case IDLE:          IdleUpdates(p_deltaTime);                       break;
 		    case AD_SHOW:       adShowAnimUpdates(p_deltaTime);	           break;
			case ENTRY: 		menuEntryAnimUpdate(p_deltaTime);    	   break;  
 			case ASK_EXIT:	    AskExitUpdates(); 						   break; 
			case SETTINGS_MENU: SettingsUpdates(p_deltaTime);			  break;
			case ON_SHOP:       m_shopScreen.Updates(p_deltaTime);    break;
 			default: break;
 		}
  
// 		if(m_adShown==false&&m_state==State.IDLE&&m_adProvider.HasNativeAd()&& m_adProvider.GetNativeAd2ndBitmap()!=null&& m_state!=State.AD_SHOW)
//		{ 
//			m_state = State.AD_SHOW;
//			resetAnim();
//			m_adProvider.SendImpression(); 
//		} 
	}
 	 
 	private void AskExitUpdates()
 	{
 		if(m_askWindow.isClosed()) 
		{
			m_state = State.IDLE;
		}
 	}
 	 
	private void adShowAnimUpdates(float p_deltaTime)
 	{
 		// 0- horizontal anim 
 		
 		if(m_anim[0]<1)
 		{
 			m_anim[0]+=0.03f  * p_deltaTime;
 			
 			if(m_anim[0]>=1)
 			{
 				m_state = State.IDLE;
 				m_adProvider.SendImpression();
 				m_adShown = true;
 			}
 		}
 		 
 	}
 	
	private void menuEntryAnimUpdate(float p_deltaTime)
 	{
 		WaitForBanner(); 
 		// 0- 'Color' and 'Wheel' horizontal entry
 		// 1 - Start button vertical Entry
 		
 		if(m_anim[0]<1)
 		{
 			m_anim[0]+=0.024f  * p_deltaTime;
 			
 			if(m_anim[0]>1)
 				m_anim[0]=1;
 		}
 		if(m_anim[0]>0.5f)
 		{
 			if(m_anim[1]<1)
 		 		m_anim[1]+=0.048f  * p_deltaTime; 

 			if(m_anim[1]>=1)
 			{
 				m_anim[1]=1; 
 			}
 		}
 		
 		if(m_anim[1]>0.3f)
 		{
 			if(m_anim[2]<1)
 		 		m_anim[2]+=0.04f  * p_deltaTime; 

 			if(m_anim[2]>=1)
 			{
 				m_anim[2]=1;
 				m_state = State.IDLE;
 			}
 		}
 	}
  
	@Override
	public void TouchUpdates(TouchEvent g)
	{ 
		 
		switch(m_state)
		{
			case IDLE:	          MenuButtonUpdates(g);  break;   
			case ON_SHOP: 		  m_shopScreen.TouchUpdates(g); break;
			case SETTINGS_MENU:	 m_settings.TouchUpdates(g);	 break; 
			case ENTRY:			 break; 
			
			case ASK_EXIT: 		 m_askWindow.TouchUpdates(g);  break; 
			default: break;  
			
		} 

		
		if(g.type == TouchEvent.TOUCH_UP)
		buttonPressed =0;	
		else if(buttonPressed!=0 &&g.type == TouchEvent.TOUCH_DOWN)
		{ 
			AssetManager.click.play(1.0f);
		}
	} 
	 
	@Override
	public void backButton() {

		switch(m_state)
		{
			case IDLE:	          
				m_askWindow = new AskUserWindow(QUESTION.EXIT);
				m_state = State.ASK_EXIT; 	
			break; 
			case ASK_EXIT:	        m_askWindow.backButton(); break;  
			case SETTINGS_MENU:	  
				if(m_settings.isIdled())
				{ 
					m_state = State.ENTRY;	
				}
				else
					m_settings.backButton(); 
				
				break; 
			case ENTRY:			 break; 
			case AD_OPENLOAD:	 GoToIdle(); break;
			case ON_SHOP: m_shopScreen.backButton();break;
			default: break;  
		} 
		 AssetManager.click.play(1.0f);
		
	}

}
