package com.eugexstudios.lineswinger;

/*EugeX framework
 *April 12, 2015
 *Eugene C. Caranto
 * **/
//


/*
 * PROJECT NAME: LINE SWINGER
 * DATE STARTED: 09/17/2018 1:15pm
 * 
 * 
 * */
 

import java.util.List;   
import java.util.Vector; 

import android.graphics.Color;
import android.graphics.Paint;    
import android.graphics.Paint.Align;  

import com.eugexstudios.framework.Game;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Image;
import com.eugexstudios.framework.ImageData;
import com.eugexstudios.framework.Screen;
import com.eugexstudios.framework.Input.TouchEvent; 
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Debugger;
import com.eugexstudios.utils.EugexStudiosSplash; 
import com.eugexstudios.utils.LanguageManager;
import com.eugexstudios.utils.SharedPref;
import com.eugexstudios.utils.genSet; 

public class GameScreen extends Screen {
	enum androidState {
		Ready, Running, Paused, GameOver
	}

	public static enum LANGUAGE 
	{
		  ENGLISH (0)
		, JAPANESE (1)
		, SPANISH (2)
		, GERMAN(3)
		, KOREAN(4)
		, PORTUGUESE (5);
		
		private final int idx;
	    private LANGUAGE(int p_idx)
	    {
	        this.idx = p_idx;
	    }

	    public int getIdx() 
	    {
	        return idx;
	    }
	}
  
	public enum AppState {
		onSplashscreen, onMenu, onPlay,   onLevelSelect, SHOW_EXIT_AD,  stateSlot_7, stateSlot_8,
	}
	androidState state = androidState.Ready;
	
	private static EugexStudiosSplash  m_eugexSplashScreen;
	private static MenuScreen		   m_menuScreen;
	private static PlayScreen		   m_playScreen;  
	private static AppState 		   m_currentState;   
	private static boolean			   m_vibrateOn;
	private static boolean			   m_soundOn;
	private static boolean			   m_musicOn;
	private static LANGUAGE            m_lang; 
	private static int     			   m_gamesPlayed; 
	private static boolean     		   m_instructionShowed; 
	
	public GameScreen(Game game) 
	{ 
		super(game);  
		genSet.paint     = new Paint(); 
		genSet.game      = game;  
		m_gamesPlayed    = 0;
		m_instructionShowed=false;
//		m_AdFailedOnLast = false;  

//		genSet.game.SetSharedPrefInt(SharedPref.SWINGER_USED, 6);  
//		genSet.game.SetSharedPrefInt(SharedPref.GEMS_COLLECTED, 3000);     
 		if( game.GetSharedPrefBoolean(SharedPref.HAS_GAME_DATA)==false)
 		{ 
 			SetPrefDataForInitInstall();
 		}
 		else
 		{  
 			LoadPrefData();  
		    genSet.game.SetSharedPrefInt(SharedPref.APP_OPENED_CTR, genSet.game.GetSharedPrefInt(SharedPref.APP_OPENED_CTR)+1);
		    if(!genSet.game.GetVersionName().equals(genSet.game.GetSharedPrefString(SharedPref.LAST_GAME_VERSION )))
		    { 
			    genSet.game.SetSharedPrefString(SharedPref.LAST_GAME_VERSION, genSet.game.GetVersionName()); 
//			    genSet.ShowToast("Version Changed");
		    }   
 		}  

 		 if(Debugger.has_no_splash && !Debugger.DEMO_MODE)
 		 {
 	 		GoToMenu(); 
 	 	 }
 		 else
 		 {
 			 GoToSplashScreen();
 		 } 
 		 if(Debugger.play_immediate && !Debugger.DEMO_MODE)
 		 { 
 	 		GoToPlay();  
 		 }

// 		genSet.ShowToast(GetGamesPlayed()+""); 
//		genSet.ShowToast(CONFIG.SCREEN_WIDTH + "," + CONFIG.SCREEN_HEIGHT + ": " + Constants.m_reso.toString()); 
	}
	 public static void ShareApp(int p_score)
	 {
		 genSet.game.ShareApp(p_score);
	 }
	 public static  void PlayAddScore()
	 {
		 m_playScreen.addScore();
	 }
  
	public static void SetPrefDataForInitInstall()
	{ 
		genSet.game.SetSharedPrefBoolean(SharedPref.HAS_GAME_DATA, true);  
		genSet.game.SetSharedPrefInt(SharedPref.APP_OPENED_CTR, 1); 
		genSet.game.SetSharedPrefInt(SharedPref.BEST_SCORE, 0); 
		genSet.game.SetSharedPrefBoolean(SharedPref.MUSIC_ON, true);
		genSet.game.SetSharedPrefBoolean(SharedPref.SOUND_ON, true); 
		genSet.game.SetSharedPrefBoolean(SharedPref.VIBRATE_ON, true);  
		genSet.game.SetSharedPrefInt(SharedPref.LANGUAGE_USED,  LANGUAGE.ENGLISH.getIdx());  
	 	genSet.game.SetSharedPrefString(SharedPref.LAST_GAME_VERSION,  genSet.game.GetVersionName());   
	 	 
		genSet.game.SetSharedPrefInt(SharedPref.SWINGER_USED, 0);  
	 	genSet.game.SetSharedPrefString(SharedPref.SWINGER_PURCH,  "1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"); //8
		genSet.game.SetSharedPrefInt(SharedPref.GEMS_COLLECTED, 0);  
		LoadPrefData(); 
	} 

	private static void LoadPrefData()
	{ 
		m_soundOn      = genSet.game.GetSharedPrefBoolean(SharedPref.SOUND_ON);
		m_musicOn      = genSet.game.GetSharedPrefBoolean(SharedPref.MUSIC_ON);
		m_vibrateOn    = genSet.game.GetSharedPrefBoolean(SharedPref.VIBRATE_ON);   
		m_lang         = LANGUAGE.values()[genSet.game.GetSharedPrefInt(SharedPref.LANGUAGE_USED)]; 	 
	}
	  
	public static ImageData GetSwingerImageData(int p_idx)
	{ 
		return GetSwingerImageData(p_idx,0);
	}
	public static void SetSwingerUsed(int p_idx)
	{
	    genSet.game.SetSharedPrefInt(SharedPref.SWINGER_USED, p_idx);
	}
	
	public static void updateSwingerPurch(boolean [] p_isPurch)
	{
		String l_isPurch ="";
		for(int idx=0;idx<p_isPurch.length;idx++)
		{
			l_isPurch+= p_isPurch[idx]?"1,":"0,";
		}
		 genSet.game.SetSharedPrefString(SharedPref.SWINGER_PURCH, l_isPurch); 
	}
	
	public static ImageData GetSwingerImageData(int p_idx, int p_color)
	{
	    	return new ImageData(0,0,p_idx*61,p_color*61,61,61);
//	    	switch(p_idx)
//	    	{
//	    	case 3:  return new ImageData(0,0,p_idx*61,p_color*61,61,61); 
//	    	
//	    	default: return new ImageData(0,0,p_idx*61-1,p_color*61,61+2,61); 
//	    	} 
	}
	public static int GetGemsCollected()
	{
		return genSet.game.GetSharedPrefInt(SharedPref.GEMS_COLLECTED);
	}
	public static int GetGamesPlayed()
	{
		return genSet.game.GetSharedPrefInt(SharedPref.APP_OPENED_CTR);
	}
	public static void UseGems(int p_gems)
	{
		genSet.game.SetSharedPrefInt(SharedPref.GEMS_COLLECTED, genSet.game.GetSharedPrefInt(SharedPref.GEMS_COLLECTED)-p_gems);
	}
	public static void AddGems(int p_gems)
	{
		genSet.game.SetSharedPrefInt(SharedPref.GEMS_COLLECTED, genSet.game.GetSharedPrefInt(SharedPref.GEMS_COLLECTED)+p_gems);
	}
	public static int GetSwingerUsed()
	{
		return genSet.game.GetSharedPrefInt(SharedPref.SWINGER_USED);
	}
	 
	public static int GetBestScore()
	{
		return genSet.game.GetSharedPrefInt(SharedPref.BEST_SCORE);
	}
	public static boolean[] GetSwingerPurchaseData()
	{
		String l_swingerPurchStr = genSet.game.GetSharedPrefString(SharedPref.SWINGER_PURCH);
		String [] l_isPurchStr = l_swingerPurchStr.split(",");
		boolean [] l_isPurch= new boolean[l_isPurchStr.length];
		for(int idx=0;idx<l_isPurchStr.length;idx++)
		{
			l_isPurch[idx] = l_isPurchStr[idx].equals("1");
		}
		return l_isPurch;
	}
	public static int[] GetMerchPrice()
	{
		return new int[]{ 0,  10, 20
						, 10, 15, 15
						, 15, 35, 10
						, 15, 35, 50
						, 35, 35, 50
						,  0,  0,  0
						,  0,  0,  0
						,  0,  0,  0
						,  0,  0,  0
						,  0,  0,  0
						,  0,  0,  0
						,  0,  0,  0
						,  0,  0,  0
						,  0,  0,  0
						,  0,  0,  0
						,  0,  0,  0
						,  0,  0,  0
						,  0,  0,  0
						,  0,  0,  0
						};
		
	}
	public static void RateApp()
	{ 		
		genSet.game.RateApp();
	} 
	
	public static void SetLanguage(LANGUAGE p_lang)
	{
		m_lang         = p_lang;
		genSet.game.SetSharedPrefInt(SharedPref.LANGUAGE_USED, m_lang.getIdx()); 
	}
	
	public static void SetBestScore(int p_bestScore)
	{  
		genSet.game.SetSharedPrefInt(SharedPref.BEST_SCORE,p_bestScore);
	} 
	
	public static void ToggleSoundSettings()
	{ 
		m_soundOn=!m_soundOn; 
		genSet.game.SetSharedPrefBoolean(SharedPref.SOUND_ON,m_soundOn);
	} 
	public static void ToggleMusicSettings()
	{ 
		m_musicOn=!m_musicOn; 
		genSet.game.SetSharedPrefBoolean(SharedPref.MUSIC_ON,m_musicOn);
	} 
	public static void ToggleVirbrateSettings()
	{ 
		m_vibrateOn=!m_vibrateOn; 
		genSet.game.SetSharedPrefBoolean(SharedPref.VIBRATE_ON,m_vibrateOn);
	} 
	
	public static boolean IsSoundOn()
	{
		return m_soundOn;
	}
	
	public static boolean IsMusicOn()
	{
		return m_musicOn;
	}	
	
	public static boolean IsVibrateOn()
	{
		return m_vibrateOn;
	}
	
	public static LANGUAGE GetLanguage()
	{
		return m_lang; 
	}
	 
	public static String GetLanguageCode()
	{
		switch(m_lang)
		{
		case ENGLISH: return "en";
		case JAPANESE: return "ja";
		case SPANISH: return "es";
		case GERMAN: return "de";
		case KOREAN: return "ko";
		case PORTUGUESE: return "pt";
		
		}
		return "en";
	}
	public static LanguageManager.W GetLanguageT()
	{
		switch(m_lang)
		{
		case ENGLISH: return LanguageManager.W.ENGLISH;
		case JAPANESE: return LanguageManager.W.JAPANESE;
		case SPANISH: return LanguageManager.W.SPANISH;
		case GERMAN: return LanguageManager.W.GERMAN;
		case KOREAN: return LanguageManager.W.KOREAN;
		case PORTUGUESE: return LanguageManager.W.PORTUGUESE; 
		
		}
		return null;
	} 
 
	  
	public static void RestartGame() 
	{
		SetState(AppState.onPlay);  
		m_gamesPlayed++;
//		boolean hasAd=m_gamesPlayed>=Constants.SHOW_AD_AFTER_N_PLAY || m_AdFailedOnLast;
		
		if(m_gamesPlayed==Constants.SHOW_AD_AFTER_N_PLAY)
		{ 
			m_gamesPlayed=0;
		}
		m_playScreen.RestartGame( );  
	}
	 
	public static void ResetAllData()
	{  
		SetPrefDataForInitInstall();
		genSet.ShowToast("Data successfully cleared.");
	}
	
	public static void ContinueGame()
	{   
		SetState(AppState.onPlay);  
	}
   
	public void gameUpdates(float p_deltaTime) 
	{  					 
		switch (m_currentState) 
		{

			case onSplashscreen: m_eugexSplashScreen.Updates(p_deltaTime);  break;
			case onMenu:        m_menuScreen.Updates(p_deltaTime); break;
			case onPlay:	    m_playScreen.Updates(p_deltaTime); 	break;  
			default: break;
		}
	}
 
	@Override
	public void paint(float deltaTime)
	{ 
		Graphics g = game.getGraphics();   
		switch (m_currentState)
		{
			case onSplashscreen: m_eugexSplashScreen.Paint(g);  break;
			case onMenu:	     m_menuScreen.Paint(g);    	break; 
			case onPlay:         m_playScreen.Paint(g);  break;  
			case SHOW_EXIT_AD:break; 
			case stateSlot_7:break;
			case stateSlot_8:break;
			default: break;
		}  
 
		if(Debugger.show_fps&&!Debugger.DEMO_MODE )
		{      
			genSet.setTextProperties( 35, Color.RED, Align.LEFT); 
			g.drawString( "FPS: " + genSet.game.getFPS() +"" , 2, 50+36*3, genSet.paint);  
			 
		} 
 
//		if(Debugger.show_touches&&!Debugger.DEMO_MODE &&m_lastTouchEvent!=null)
//		{      
//			genSet.setTextProperties( 35, Color.RED, Align.LEFT);
//			g.drawString(m_lastTouchEvent.type+"", 50, 50+36*0, genSet.paint); 
//			g.drawString(m_lastTouchEvent.x + "," + m_lastTouchEvent.y, 50, 50+36*1, genSet.paint); 
//		} 
	}
	TouchEvent m_lastTouchEvent;
	private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
		int len = touchEvents.size();

		for (int i = 0; i < len; i++)
		{
			TouchEvent event = touchEvents.get(i);
			switch (m_currentState) 
			{
				case onMenu: 	    m_menuScreen.TouchUpdates(event); 	     break; 
				case onPlay:        m_playScreen.TouchUpdates(event);	     break;  
				case SHOW_EXIT_AD: 	break; 
				case stateSlot_7: 	break;
				case stateSlot_8: 	break;
				default: break;  
				
			}  
			
//			if(event.type == TouchEvent.TOUCH_DOWN)
//			{
//	 			game.hideSystemUI();
//			}
//				m_postool.positioningToolTouch(event);
			m_lastTouchEvent = event;
		}
	} 
	public static void RemoveAdRequest()
	{
		
	}
	
	public static void ResumeGame()
	{
		SetState(AppState.onPlay);
	}

	public static void CloseGame() 
	{
		android.os.Process.killProcess(android.os.Process.myPid());
	} 
	
	public static void GoToPlay() 
	{
		SetState(AppState.onPlay);
		if(m_instructionShowed==false)
		{
			if(GetGamesPlayed()==1)
			{ 
				m_playScreen=new PlayScreen(InstructionScreen.type.TAP);
				m_instructionShowed=true;
			}
			else // Sure to. Wag mo burahin. Parang mali diba? Pero trust me. - Eugene (Sept 24, 2018)
			{
				m_playScreen=new PlayScreen();
			}
		}
		else
		{
			m_playScreen=new PlayScreen();
		}
	}
	public static void GoToMenu() 
	{ 
		SetState(AppState.onMenu);
		m_menuScreen = new MenuScreen();
//		genSet.game.ShowBannerAd(); 
		genSet.game.HideBannerAd(); 
	}
	public static void GoToSplashScreen() 
	{
		SetState(AppState.onSplashscreen);
		m_eugexSplashScreen = new EugexStudiosSplash();
	}

	@Override
	public void backButton() 
	{ 
		switch (m_currentState)
		{
			case onMenu:  	        m_menuScreen.backButton();  	break;
			case onPlay:  		    m_playScreen.backButton(); break;  
			case stateSlot_7: break;
			case stateSlot_8: break;
			default: break;
		} 
	}
	 
	public static void SetState(AppState newState) 
	{ 
		m_currentState = newState;
	}	public static AppState GetState() 
	{ 
		return m_currentState ;
	}
	

	public int[] paintButton(int g, Image buttonImage, int xPos, int yPos, int sourceX, int sourceY, int l, int w, int buttonValue, int d1, int d2) {

		return new int[] { xPos, yPos, l, w, buttonValue };
	}
 
	public boolean inBounds(TouchEvent event, int x, int y, int width, int height) 
	{
		if (event.x > x && event.x < x + width - 1 && event.y > y && event.y < y + height - 1)
			return true;
		else
			return false;
	}

	@Override
	public void pause() {
		if (state == androidState.Running)
			state = androidState.Paused;

		// if(gameState!=onPlayOver)
		// pauseGame();

	}

	@Override
	public void resume() 
	{
		if (state == androidState.Paused)
			state = androidState.Running;

	}

	public void update(float deltaTime) 
	{
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		if (state == androidState.Ready)
			updateReady(touchEvents);
		if (state == androidState.Running)
			updateRunning(touchEvents, deltaTime);
		if (state == androidState.Paused)
			updateRunning(touchEvents, deltaTime);


		gameUpdates(deltaTime);
	}

	private void updateReady(List<TouchEvent> touchEvents)
	{
		if (touchEvents.size() > 0)
			state = androidState.Running;
	} 
	
	@Override
	public void dispose()
	{ 
		game.showToast("dispose!"); 
	}
}