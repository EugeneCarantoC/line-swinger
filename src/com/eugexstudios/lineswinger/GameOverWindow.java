package com.eugexstudios.lineswinger;

import android.graphics.Color; 
import android.graphics.Paint.Align;

import com.eugexstudios.adprovider.AdProvider; 
import com.eugexstudios.adprovider.StartApp.VideoAdType;
import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.lineswinger.AskUserWindow.QUESTION;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.genSet;

public class GameOverWindow  extends Screen{
	
	private final byte 
	  RESTART_BTN  = 1 
	, CONTINUE_BTN = 2  
	, HOME_BTN 	   = 3 
	, SHARE_BTN    = 4 
    , AD_BTN  = 8
	; 
	
	public enum State 
	{ 
		  IDLE    
		, AD_OPENLOAD
		, CONTINUE_ASK
		, AD_SHOW
	}
	 
 
	private State 				       m_state;    
	private static AdProvider 		   m_adProvider; 
	private int 				       m_score;
	private boolean 				   m_newBestScore;
	private AskUserWindow			   m_askUserWindow; 

	private int 				   m_bestScore;
	public GameOverWindow(int p_score, boolean p_newBestScore)
	{ 
	    m_adProvider = new AdProvider(); 
	    m_adProvider.RequestNativeAd(Constants.NATIVE_AD_GAMEOVER_2);  
		m_adProvider.RequestVideoAd(Constants.REWARD_VIDEO_AD_CONT,VideoAdType.RewardToContinue);
		 
		m_askUserWindow = new AskUserWindow(QUESTION.CONTINUE_GAME);
		m_score = p_score;
		
		m_newBestScore =p_newBestScore;
		m_bestScore = GameScreen.GetBestScore();
	}
	
	State getState()
	{
		return m_state;
	}
	
	void GoToIdle()
	{
		m_state = State.IDLE;
	}
	
	public void Show(boolean p_canContinue)
	{
		m_state = State.IDLE;
		
		if(m_adProvider.isVideoAdAvailable()&&p_canContinue)
		{
			m_state         = State.CONTINUE_ASK;
			m_askUserWindow = new AskUserWindow(QUESTION.CONTINUE_GAME);
		} 
	}
	
	@Override
	public void Paint(Graphics g) { 
		switch(m_state)
		{ 
			case IDLE:          PaintIdle(g);              break; 
			case CONTINUE_ASK:  m_askUserWindow.Paint(g);  break;  
			case AD_OPENLOAD:   PaintIdle(g);              break;
			default: break;
		}
	}
	
  
	
	public void PaintIdle(Graphics g)
	{
		g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(150, 237, 28, 36)); //Red 
		g.drawImage(AssetManager.sprites, Constants.GAMEOVER_IMG); 
		
		g.drawImage(AssetManager.sprites, buttonPressed==RESTART_BTN?-3:0, buttonPressed==RESTART_BTN?6:0, Constants.RESTART_IMG); 
		g.drawImage(AssetManager.sprites, buttonPressed==HOME_BTN?-3:0,    buttonPressed==HOME_BTN?6:0, Constants.HOME_IMG); 
		g.drawImage(AssetManager.sprites, buttonPressed==SHARE_BTN?-3:0,   buttonPressed==SHARE_BTN?6:0, Constants.SHARE_IMG);
		
		if(m_newBestScore)
		{
			 g.drawImage(AssetManager.sprites, Constants.NEW_BEST_SCORE);
		 }
		else
		{
			genSet.setTextProperties(40, Color.WHITE, Align.CENTER);  
			g.drawStringFont("SCORE: " + m_score, CONFIG.SCREEN_MID, 560, AssetManager.Comforta, genSet.paint); 
		}
		genSet.setTextProperties(30, Color.WHITE, Align.CENTER);  
		g.drawStringFont("BEST SCORE: " + m_bestScore, CONFIG.SCREEN_MID, 610, AssetManager.Comforta, genSet.paint); 
	}
	
	 
 
	public void SetState(State p_state)
	{
		m_state= p_state; 
	}
	
	 
	@Override
	public void Updates(float p_deltaTime)
	{
		switch(m_state)
		{
			case IDLE: break;  
			case CONTINUE_ASK: ContinueAskUpdates(p_deltaTime);     break;
			default: 	break;
		}
		 
	} 
	
	@Override
	public void TouchUpdates(TouchEvent g)
	{
		switch(m_state)
		{
		    case AD_SHOW:     GameOverButtonUpdates(g);         break;
			case IDLE:        GameOverButtonUpdates(g);         break;
			case CONTINUE_ASK: m_askUserWindow.TouchUpdates(g); break; 
			default:break;
		}
	}
	
	private void ContinueAskUpdates(float p_deltaTime)
	{
		m_askUserWindow.Updates(p_deltaTime);
		if(m_askUserWindow.isClosed())
		{
			m_state = State.IDLE; 
		}
	}
	
	public  static void ContinueGame()
	{
		m_adProvider.ShowVideoAds(); 
	}
	 
	  
	public void GameOverButtonUpdates(TouchEvent g) 
	{
		if(g.type == TouchEvent.TOUCH_DOWN)
		{

			 drawRoundRect(g,Constants.RESTART_IMG, Color.BLACK, Color.BLACK, RESTART_BTN);
			 drawRoundRect(g,Constants.HOME_IMG, Color.BLACK, Color.BLACK, HOME_BTN);
			 drawRoundRect(g,Constants.SHARE_IMG, Color.BLACK, Color.BLACK, SHARE_BTN);	  }
		
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case AD_BTN:     
					m_adProvider.SendClick();
					m_state = State.AD_OPENLOAD; 
				    genSet.game.ShowDialog("Loading. Please wait...");
					buttonPressed = 0;
					break;
				case HOME_BTN:     GameScreen.GoToMenu();        break; 
				case RESTART_BTN:  GameScreen.GoToPlay(); break; 
				case SHARE_BTN:    GameScreen.ShareApp(m_score);break;
				case CONTINUE_BTN: break;	  
			}
			
			buttonPressed=0;
		} 
		else if(buttonPressed!=0 &&g.type == TouchEvent.TOUCH_DOWN)
		{ 
			AssetManager.click.play(1.0f);
		}
	} 
 
	
	@Override
	public void backButton()
	{
		switch(m_state)
		{
			case CONTINUE_ASK:  
				m_state = State.IDLE; 
				break; 
			case IDLE: GameScreen.GoToMenu();break; 
			case AD_OPENLOAD:  
				m_state = State.IDLE; 
			    genSet.game.HideDialog();
				break;
			default: break;
		}
	} 
}
