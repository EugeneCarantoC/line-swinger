package com.eugexstudios.lineswinger;

import android.graphics.Color;
import android.graphics.Paint.Align; 

import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.LanguageManager;
import com.eugexstudios.utils.SharedPref;
import com.eugexstudios.utils.genSet;
import com.eugexstudios.utils.LanguageManager.W;

public class AskUserWindow extends Screen{
	
	public enum QUESTION {
		  EXIT
		, EXIT_RATE
		, RATE_OLD
		, SHARE
		, RESET_DATA
		, GET_STARS
		, CONTINUE_GAME
		, CONGRATS_NOTIF
		};
		
   private final byte 
		  YES_BTN         = 1
		, NO_BTN          = 2
	    , RATE_APP_BTN    = 3 
	    , CONTINUE_BTN    = 4
	    , CLOSE_CONT_BTN  = 5
	    , OKAY_BTN        = 6
	    , STAR_BTN        = 100
		;
   
	private QUESTION			       m_qType; 
	private boolean					   m_isClose; 
	private int                        m_rateGiven;
	private float                      m_continueTimer;
	
	public AskUserWindow(QUESTION p_qType) 
	{ 
		m_qType     = p_qType;
		m_isClose   = false;
		m_rateGiven = 0;
		m_continueTimer=0;
	}
	 
	public boolean isClosed()
	{
		return m_isClose;
	}
	
	@Override
	public void Paint(Graphics g) 
	{ 
		switch(m_qType)
		{
			case EXIT:   	     PaintAskToExit(g);	      break;
			case EXIT_RATE:      PaintAskToRateOnExit(g); break;
			case RATE_OLD:       PaintAskToRate(g);	      break; 
			case RESET_DATA:     PaintToReset(g);	      break;
			case GET_STARS:      PaintGetStars(g);        break;
			case CONTINUE_GAME:  PaintAskToContGame(g);   break;
			case CONGRATS_NOTIF: PaintCongrats(g);        break;
			case SHARE:      break;
		default:
			break;
		}
	}

	@Override
	public void Updates(float p_deltaTime) 
	{ 
		switch(m_qType)
		{
			case CONTINUE_GAME: contGameUpdates(p_deltaTime); break;
			case EXIT:           break;
			case SHARE:          break;
			case GET_STARS:      break;
			case EXIT_RATE:      break;
			case RESET_DATA:     break;
			case CONGRATS_NOTIF: break;
			default: break;
		}
		
	}

	@Override
	public void TouchUpdates(TouchEvent g) 
	{ 
		switch(m_qType)
		{ 
			case EXIT:		AskExitBtnTouchUpdates(g);     break; 
			case EXIT_RATE: AskToRateOnExitTouchUpdate(g); break;
			case RATE_OLD: 		AskToRateTouchUpdate(g);	   break; 
			case SHARE: break;
			case RESET_DATA: AskResetTouchUpdates(g);break;
			case GET_STARS:  GetStarsTouchUpdate(g); break;
			case CONTINUE_GAME: AskContinueTouchUpdates(g);break;
			case CONGRATS_NOTIF:CongratsNotifUpdates(g);  break;
		}	
		
		if(buttonPressed!=0 &&g.type == TouchEvent.TOUCH_DOWN)
		{ 
			AssetManager.click.play(1.0f);
		}
	}

	private void contGameUpdates(float p_deltaTime) 
	{

		if(m_continueTimer<1)
		{
			m_continueTimer+= 0.005*p_deltaTime;
			if(m_continueTimer>=1)
				m_continueTimer=1;
		}
	}
	private void GetStarsTouchUpdate(TouchEvent g)
	{
		
		if(g.type == TouchEvent.TOUCH_DOWN)
		{
			for(int idx=0;idx<5;idx++)
			{
				drawRoundRect(g,  203 + idx*68-12, 650-45/2-20, 65,65, Color.GRAY, Color.rgb(190, 190, 190), STAR_BTN+idx);  
			}
		 	
			if(buttonPressed>=STAR_BTN)
			{
				int rate = buttonPressed-STAR_BTN+1;
				m_rateGiven= rate; 
			}
			else if(buttonPressed!=0)
			{
		 		buttonPressed=0;
		 	}
		}
		if(g.type == TouchEvent.TOUCH_UP)
		{
			if(buttonPressed>=STAR_BTN)
			{
				int rate = buttonPressed-STAR_BTN+1;
				m_rateGiven= rate;
				
				if(rate<=2)
				{
					m_isClose = true;
					genSet.game.SendEmailFeedback();
				}
				else
				{
					m_isClose = true;
					genSet.game.RateApp();
				}
				
			}
			else if(buttonPressed!=0)
		 		buttonPressed=0;
		}
	}
	@Override
	public void backButton() 
	{	
		switch(m_qType)
		{
			case EXIT: 	         m_isClose = true; break;
			case EXIT_RATE:      m_isClose = true; break;
			case GET_STARS:      m_isClose = true; break;
			case CONTINUE_GAME:  m_isClose = true; break;
			case CONGRATS_NOTIF: m_isClose = true; break;
			case SHARE:                            break;
			case RESET_DATA:                       break;
			default: 	break;
		} 
	} 

	private void PaintCongrats(Graphics g)
	{
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(66, 0, 0, 0));
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(130, 255,255, 255)); 
		
		g.drawRoundRect((int)(CONFIG.SCREEN_WIDTH*0.05f),  (int)(CONFIG.SCREEN_HEIGHT*0.2367f), (int)(CONFIG.SCREEN_WIDTH*0.9f), (int)(CONFIG.SCREEN_HEIGHT*0.40f), 40, 40, Color.WHITE);

		genSet.setTextProperties(Constants.CONGRATS_LS, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont(W.CONGRATS, CONFIG.SCREEN_MID,  (int)(CONFIG.SCREEN_HEIGHT*0.33f), AssetManager.Comforta,genSet.paint);
		
		genSet.setTextProperties(40, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont("You have unlocked all levels!", CONFIG.SCREEN_MID,  (int)(CONFIG.SCREEN_HEIGHT*0.39f), AssetManager.Comforta,genSet.paint); 

		genSet.setTextProperties(37, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont("Stay tuned for the next", CONFIG.SCREEN_MID,  (int)(CONFIG.SCREEN_HEIGHT*0.45f), AssetManager.Comforta,genSet.paint);
		g.drawStringFont("update of Color Twirl", CONFIG.SCREEN_MID,  (int)(CONFIG.SCREEN_HEIGHT*0.485f), AssetManager.Comforta,genSet.paint);
		
		drawRoundRect(g,(int)(CONFIG.SCREEN_WIDTH*0.075f),(int)(CONFIG.SCREEN_HEIGHT*0.473f), (int)(CONFIG.SCREEN_WIDTH*0.85f), (int)(CONFIG.SCREEN_HEIGHT* 0.1f), Color.DKGRAY, Color.rgb(190, 190, 190), OKAY_BTN); 
		genSet.setTextProperties(57, Color.WHITE, Align.CENTER); 
		g.drawStringFont(W.OKAY, CONFIG.SCREEN_MID,	 (int)(CONFIG.SCREEN_HEIGHT*0.59f) , AssetManager.Comforta,genSet.paint); 

	}
	private void PaintAskToExit(Graphics g)
	{ 
		g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(55, 0, 0, 0)); //BLACK 
		g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(75, 88, 20, 92)); //Red  
 		drawRoundRect(g,  (int)(CONFIG.SCREEN_WIDTH*0.0375f),(int)(CONFIG.SCREEN_HEIGHT*0.19f), (int)(CONFIG.SCREEN_WIDTH*(1-(0.0375f*2))), (int)(CONFIG.SCREEN_HEIGHT* 0.29f), Color.WHITE, Color.rgb(190, 190, 190), -1); 
				
		genSet.setTextProperties(Constants.ASK_EXIT_LS, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont(W.ASK_EXIT, CONFIG.SCREEN_WIDTH/2, (int)(CONFIG.SCREEN_HEIGHT*0.355f)+(int)(CONFIG.SCREEN_HEIGHT*0.078125f), AssetManager.Comforta,genSet.paint);

		drawRoundRect(g, 4,-5, Constants.YES_EXIT, Color.argb(120,88,20,92), Color.argb(160,88,20,92), YES_BTN); 
		drawRoundRect(g, Constants.YES_EXIT, Color.argb(160,88,20,92), Color.argb(162,88,20,92), YES_BTN);  

		drawRoundRect(g, 4,5, Constants.NO_EXIT, Color.argb(120,88,20,92), Color.argb(160,88,20,92), NO_BTN); 
		drawRoundRect(g,  Constants.NO_EXIT, Color.argb(160,88,20,92), Color.argb(162,88,20,92), NO_BTN);   

		genSet.setTextProperties(Constants.ASK_EXIT_LS, Color.WHITE, Align.CENTER); 
		g.drawStringFont(W.YES, (int)(CONFIG.SCREEN_WIDTH*0.07f)+(int)(CONFIG.SCREEN_WIDTH*0.42f)/2,								  (int)(CONFIG.SCREEN_HEIGHT*0.4365f)+ (int)(CONFIG.SCREEN_HEIGHT*0.132f), AssetManager.Comforta,genSet.paint); 
		g.drawStringFont(W.NO,  (int)(CONFIG.SCREEN_WIDTH*0.07f)+(int)(CONFIG.SCREEN_WIDTH*0.42f)/2+(int)(CONFIG.SCREEN_WIDTH*0.44f), (int)(CONFIG.SCREEN_HEIGHT*0.4365f)+ (int)(CONFIG.SCREEN_HEIGHT*0.132f), AssetManager.Comforta,genSet.paint);
	}
	 
	private void PaintAskToContGame(Graphics g)
	{ 
		g.drawRect(Constants.SCREEN_RECT, Color.argb(155, 255,255, 255));  

		genSet.setTextProperties(Constants.CONT_GAME_LS,Color.argb(25, 0, 0, 0), Align.CENTER);  
		g.drawStringFont(W.CONT_GAME, CONFIG.SCREEN_WIDTH/2+4, (int)(CONFIG.SCREEN_HEIGHT*0.35f)+(int)(CONFIG.SCREEN_HEIGHT*0.075f)+4, AssetManager.Comforta,genSet.paint);
		genSet.setTextProperties(Constants.CONT_GAME_LS,Color.DKGRAY, Align.CENTER);  
		g.drawStringFont(W.CONT_GAME, CONFIG.SCREEN_WIDTH/2, (int)(CONFIG.SCREEN_HEIGHT*0.35f)+(int)(CONFIG.SCREEN_HEIGHT*0.075f), AssetManager.Comforta,genSet.paint);
	
//		drawRoundRect(g, CONFIG.SCREEN_MID-155, 740-155*2,  155*2, 155*2, Color.argb(35, 0, 0, 0), Color.argb(0, 0, 0, 0), CONTINUE_BTN); 
		g.drawCircle(CONFIG.SCREEN_MID+6, 740+4, 135, Color.argb(25, 0, 0, 0)); 
		g.drawCircle(CONFIG.SCREEN_MID,   740,   135, CONTINUE_BTN==buttonPressed?Color.LTGRAY:Color.DKGRAY);  
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_MID -116/2, 740-119/2,  5,324,124,119); // 'play' icon 
		 
//		drawRoundRect(g, CONFIG.SCREEN_MID-75, 970-75*2,  75*2, 75*2, Color.argb(35, 0, 0, 0), Color.argb(0, 0, 0, 0), CLOSE_CONT_BTN); 
 
		g.drawCircle(CONFIG.SCREEN_MID+6, 970+4, 65, Color.argb(25, 0, 0, 0)); 
		
		g.drawCircle(CONFIG.SCREEN_MID,   970,   65,
				         m_continueTimer<1?
						Color.LTGRAY:
						CLOSE_CONT_BTN==buttonPressed?Color.LTGRAY:Color.DKGRAY
						);  
		
		if(m_continueTimer<1)
		{
			g.drawArc(CONFIG.SCREEN_MID,      970,   60, 0, (int)((1-m_continueTimer)*360), Color.LTGRAY); 
			g.drawCircle(CONFIG.SCREEN_MID,   970,   40, Color.LTGRAY); 
		}
		genSet.setTextProperties( 55,Color.argb(235, 0, 0, 0), Align.CENTER);  
		g.drawStringFont("X", CONFIG.SCREEN_MID, 992, AssetManager.Comforta,genSet.paint);
		

	}
	private void PaintGetStars(Graphics g)
	{ 
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(66, 0, 0, 0));
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(130, 255,255, 255)); 
		 
 		drawRoundRect(g,  (int)(CONFIG.SCREEN_WIDTH*0.055f),(int)(CONFIG.SCREEN_HEIGHT*0.20f), (int)(CONFIG.SCREEN_WIDTH*(1-(0.055f*2))), (int)(CONFIG.SCREEN_HEIGHT* 0.27f), Color.WHITE, Color.rgb(190, 190, 190), -1); 

		genSet.setTextProperties( 45, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont("Did you like Color Twirl?", CONFIG.SCREEN_WIDTH/2, (int)(CONFIG.SCREEN_HEIGHT*0.445f), AssetManager.Comforta,genSet.paint);
//		g.drawLine(new Point(CONFIG.SCREEN_MID,0), new Point(CONFIG.SCREEN_MID,CONFIG.SCREEN_HEIGHT), Color.RED);

		for(int idx=0;idx<5;idx++)
		{
		//	drawRoundRect(g,  203 + idx*68-12, 650-45/2-20, 65,65, Color.GRAY, Color.rgb(190, 190, 190), STAR_BTN+idx); 
			if(idx<m_rateGiven)
			{
				g.drawImage(AssetManager.sprites, 203 + idx*68, 650,347,400,45,45);
			}
			else
			{ 
				g.drawImage(AssetManager.sprites, 203 + idx*68, 650,402,400,45,45);	
			}
			
		}
			 
	 	}
	private void PaintToReset(Graphics g)
	{ 
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(66, 0, 0, 0));
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(130, 255,255, 255)); 
		 
 		drawRoundRect(g,  (int)(CONFIG.SCREEN_WIDTH*0.0375f),(int)(CONFIG.SCREEN_HEIGHT*0.19f), (int)(CONFIG.SCREEN_WIDTH*(1-(0.0375f*2))), (int)(CONFIG.SCREEN_HEIGHT* 0.29f), Color.WHITE, Color.rgb(190, 190, 190), -1); 
				
		if(LanguageManager.GetLocalized(W.ASK_RESET_L1).equals(""))
		{
			genSet.setTextProperties( 41, Color.DKGRAY, Align.CENTER);  
			g.drawStringFont(W.ASK_RESET_L2, CONFIG.SCREEN_WIDTH/2, (int)(CONFIG.SCREEN_HEIGHT*0.40f), AssetManager.Comforta,genSet.paint); 
		}
		else
		{ 
			genSet.setTextProperties( 39, Color.DKGRAY, Align.CENTER);  
			g.drawStringFont(W.ASK_RESET_L1, CONFIG.SCREEN_WIDTH/2, (int)(CONFIG.SCREEN_HEIGHT*0.41f), AssetManager.Comforta,genSet.paint);
			g.drawStringFont(W.ASK_RESET_L2, CONFIG.SCREEN_WIDTH/2, (int)(CONFIG.SCREEN_HEIGHT*0.452f), AssetManager.Comforta,genSet.paint);
		}
		drawRoundRect(g,        (int)(CONFIG.SCREEN_WIDTH*0.07f),								     								       (int)(CONFIG.SCREEN_HEIGHT*0.439f), (int)(CONFIG.SCREEN_WIDTH*0.42f), (int)(CONFIG.SCREEN_HEIGHT* 0.112f), Color.DKGRAY, Color.rgb(190, 190, 190), YES_BTN); 
		drawRoundRect(g,        (int)(CONFIG.SCREEN_WIDTH*0.07f)+(int)(CONFIG.SCREEN_WIDTH*0.445f), 				  			           (int)(CONFIG.SCREEN_HEIGHT*0.439f), (int)(CONFIG.SCREEN_WIDTH*0.42f), (int)(CONFIG.SCREEN_HEIGHT* 0.112f), Color.DKGRAY, Color.rgb(190, 190, 190), NO_BTN);  

		genSet.setTextProperties(55, Color.WHITE, Align.CENTER); 
		g.drawStringFont(W.YES, (int)(CONFIG.SCREEN_WIDTH*0.07f)+(int)(CONFIG.SCREEN_WIDTH*0.42f)/2,								  (int)(CONFIG.SCREEN_HEIGHT*0.4365f)+ (int)(CONFIG.SCREEN_HEIGHT*0.132f), AssetManager.Comforta,genSet.paint); 
		g.drawStringFont(W.NO,  (int)(CONFIG.SCREEN_WIDTH*0.07f)+(int)(CONFIG.SCREEN_WIDTH*0.42f)/2+(int)(CONFIG.SCREEN_WIDTH*0.44f), (int)(CONFIG.SCREEN_HEIGHT*0.4365f)+ (int)(CONFIG.SCREEN_HEIGHT*0.132f), AssetManager.Comforta,genSet.paint);
	}
	
	public void AskResetTouchUpdates(TouchEvent g)
	{  
		drawRoundRect(g,        (int)(CONFIG.SCREEN_WIDTH*0.07f),								     								       (int)(CONFIG.SCREEN_HEIGHT*0.439f), (int)(CONFIG.SCREEN_WIDTH*0.42f), (int)(CONFIG.SCREEN_HEIGHT* 0.112f), Color.DKGRAY, Color.rgb(170, 170, 170), YES_BTN); 
		drawRoundRect(g,        (int)(CONFIG.SCREEN_WIDTH*0.07f)+(int)(CONFIG.SCREEN_WIDTH*0.445f), 				  			           (int)(CONFIG.SCREEN_HEIGHT*0.439f), (int)(CONFIG.SCREEN_WIDTH*0.42f), (int)(CONFIG.SCREEN_HEIGHT* 0.112f), Color.DKGRAY, Color.rgb(170, 170, 170), NO_BTN);  
		
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case YES_BTN: GameScreen.ResetAllData();  m_isClose=true;  	break;
				case NO_BTN:  m_isClose = true; break;
	 		}
			
			if(buttonPressed!=0)
				buttonPressed=0;
		}
	} 
	
	public void CongratsNotifUpdates(TouchEvent g)
	{   
		if(g.type == TouchEvent.TOUCH_DOWN)
		{
			drawRoundRect(g,(int)(CONFIG.SCREEN_WIDTH*0.075f),(int)(CONFIG.SCREEN_HEIGHT*0.473f), (int)(CONFIG.SCREEN_WIDTH*0.85f), (int)(CONFIG.SCREEN_HEIGHT* 0.1f), Color.DKGRAY, Color.rgb(190, 190, 190), OKAY_BTN); 
				 
		}
		
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case OKAY_BTN:   m_isClose=true;   break;  
	 		} 
			if(buttonPressed!=0)
				buttonPressed=0;
		}
	} 
	
	public void AskContinueTouchUpdates(TouchEvent g)
	{   
		if(g.type == TouchEvent.TOUCH_DOWN)
		{
			drawRoundRect(g, CONFIG.SCREEN_MID-155, 740-155*2,  155*2, 155*2, Color.argb(50, 0, 0, 0), Color.argb(0, 0, 0, 0), CONTINUE_BTN); 
			
			if(m_continueTimer>=1)
			{
				drawRoundRect(g, CONFIG.SCREEN_MID-75, 970-75*2,  75*2, 75*2, Color.argb(35, 0, 0, 0), Color.argb(0, 0, 0, 0), CLOSE_CONT_BTN); 
			}
		}
		
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case CLOSE_CONT_BTN: m_isClose = true;              break; 
				case CONTINUE_BTN:   GameOverWindow.ContinueGame(); break; 
	 		}
			
			if(buttonPressed!=0)
				buttonPressed=0;
		}
	} 
	public void AskExitBtnTouchUpdates(TouchEvent g)
	{  
//		drawRoundRect(g,        (int)(CONFIG.SCREEN_WIDTH*0.07f),								     								       (int)(CONFIG.SCREEN_HEIGHT*0.439f), (int)(CONFIG.SCREEN_WIDTH*0.42f), (int)(CONFIG.SCREEN_HEIGHT* 0.112f), Color.DKGRAY, Color.rgb(170, 170, 170), YES_BTN); 
//		drawRoundRect(g,        (int)(CONFIG.SCREEN_WIDTH*0.07f)+(int)(CONFIG.SCREEN_WIDTH*0.445f), 				  			           (int)(CONFIG.SCREEN_HEIGHT*0.439f), (int)(CONFIG.SCREEN_WIDTH*0.42f), (int)(CONFIG.SCREEN_HEIGHT* 0.112f), Color.DKGRAY, Color.rgb(170, 170, 170), NO_BTN);  

		drawRoundRect(g, Constants.YES_EXIT, Color.argb(110,88,20,92), Color.argb(150,88,20,92), YES_BTN);  
		drawRoundRect(g,  Constants.NO_EXIT, Color.argb(110,88,20,92), Color.argb(150,88,20,92), NO_BTN); 
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case YES_BTN:  
					if(genSet.game.GetSharedPrefInt(SharedPref.APP_OPENED_CTR)==4|| genSet.game.GetSharedPrefInt(SharedPref.APP_OPENED_CTR)==20)
					{
						m_qType = QUESTION.EXIT_RATE;
					}
					else 
					{   
						genSet.game.StartAppExitAd(); 
					} 
				break;
				case NO_BTN:  m_isClose = true; break;
	 		}
			
			if(buttonPressed!=0)
				buttonPressed=0;
		}
	} 
	
	private void PaintAskToRate(Graphics g)
	{ 
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(66, 0, 0, 0));
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(130, 255,255, 255)); 

		g.drawRoundRect((int)(CONFIG.SCREEN_WIDTH*0.05f),  (int)(CONFIG.SCREEN_HEIGHT*0.31f), (int)(CONFIG.SCREEN_WIDTH*(1-0.1f)) , (int)(CONFIG.SCREEN_HEIGHT*0.27f)+(int)(CONFIG.SCREEN_HEIGHT*0.1f) ,30,30, Color.WHITE); 
  			
		genSet.setTextProperties( 50, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont("Give us feedback :)", CONFIG.SCREEN_WIDTH/2, (int)((CONFIG.SCREEN_HEIGHT*0.325f) + 50 * 1.5f), AssetManager.Comforta,genSet.paint);

		genSet.setTextProperties( 36, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont("Don't forget to leave us a rating!", CONFIG.SCREEN_WIDTH/2, (int)((CONFIG.SCREEN_HEIGHT*0.4f) + 50 * 1.5f), AssetManager.Comforta,genSet.paint);
 		
		g.drawStringFont( "Thanks for playing Color Twirl!",        CONFIG.SCREEN_WIDTH/2, (int)((CONFIG.SCREEN_HEIGHT*0.455f) + 50 * 1.5f), AssetManager.Comforta,genSet.paint);  
			 
		drawRoundRect(g,  (int)(CONFIG.SCREEN_WIDTH*0.07f),	  (int)(CONFIG.SCREEN_HEIGHT*0.5f), (int)(CONFIG.SCREEN_WIDTH*0.86f), (int)(CONFIG.SCREEN_HEIGHT* 0.112f), Color.DKGRAY, Color.rgb(170, 170, 170), RATE_APP_BTN); 
 
		genSet.setTextProperties(55, Color.WHITE, Align.CENTER); 
		g.drawStringFont("RATE APP",  CONFIG.SCREEN_WIDTH/2,	 (int)(CONFIG.SCREEN_HEIGHT*0.635f)  , AssetManager.Comforta,genSet.paint); 
	}
	
	private void PaintAskToRateOnExit(Graphics g)
	{ 
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(66, 0, 0, 0));
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(130, 255,255, 255)); 

		g.drawRoundRect((int)(CONFIG.SCREEN_WIDTH*0.05f),  (int)(CONFIG.SCREEN_HEIGHT*0.25f), (int)(CONFIG.SCREEN_WIDTH*(1-0.1f)) , (int)(CONFIG.SCREEN_HEIGHT*0.25f)+(int)(CONFIG.SCREEN_HEIGHT*0.215f) ,30,30, Color.WHITE); 
  			
		genSet.setTextProperties( 50, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont("Wait a minute!", CONFIG.SCREEN_WIDTH/2, (int)((CONFIG.SCREEN_HEIGHT*0.26f) + 50 * 1.5f), AssetManager.Comforta,genSet.paint);

		genSet.setTextProperties( 36, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont("Thanks for playing Color Twirl!", CONFIG.SCREEN_WIDTH/2, (int)((CONFIG.SCREEN_HEIGHT*0.32f) + 50 * 1.5f), AssetManager.Comforta,genSet.paint);
 		
		g.drawStringFont("If you have some time left,",        CONFIG.SCREEN_WIDTH/2, (int)((CONFIG.SCREEN_HEIGHT*0.375f) + 50 * 1.5f), AssetManager.Comforta,genSet.paint);
		g.drawStringFont("don't forget to leave us a rating!", CONFIG.SCREEN_WIDTH/2, (int)((CONFIG.SCREEN_HEIGHT*(0.375f+0.04f*1)) + 50 * 1.5f), AssetManager.Comforta,genSet.paint);
		g.drawStringFont("Thank you! :)",              CONFIG.SCREEN_WIDTH/2, (int)((CONFIG.SCREEN_HEIGHT*(0.375f+0.04f*3)) + 50 * 1.5f), AssetManager.Comforta,genSet.paint);
			 
		drawRoundRect(g,  (int)(CONFIG.SCREEN_WIDTH*0.07f),	  (int)(CONFIG.SCREEN_HEIGHT*0.539f), (int)(CONFIG.SCREEN_WIDTH*0.86f), (int)(CONFIG.SCREEN_HEIGHT* 0.112f), Color.DKGRAY, Color.rgb(170, 170, 170), RATE_APP_BTN); 
 
		genSet.setTextProperties(55, Color.WHITE, Align.CENTER); 
		g.drawStringFont("RATE APP",  CONFIG.SCREEN_WIDTH/2,	 (int)(CONFIG.SCREEN_HEIGHT*0.67f)  , AssetManager.Comforta,genSet.paint); 
	 }

 
	public void AskToRateTouchUpdate(TouchEvent g)
	{   
		drawRoundRect(g,  (int)(CONFIG.SCREEN_WIDTH*0.07f),	  (int)(CONFIG.SCREEN_HEIGHT*0.5f), (int)(CONFIG.SCREEN_WIDTH*0.86f), (int)(CONFIG.SCREEN_HEIGHT* 0.112f), Color.DKGRAY, Color.rgb(170, 170, 170), RATE_APP_BTN); 
		 
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case RATE_APP_BTN:  GameScreen.RateApp(); m_isClose=true; break;  
			} 
			if(buttonPressed!=0)
				buttonPressed=0;
		} 
	}	
	
	public void AskToRateOnExitTouchUpdate(TouchEvent g)
	{   
		drawRoundRect(g,  (int)(CONFIG.SCREEN_WIDTH*0.07f),	  (int)(CONFIG.SCREEN_HEIGHT*0.539f), (int)(CONFIG.SCREEN_WIDTH*0.86f), (int)(CONFIG.SCREEN_HEIGHT* 0.112f), Color.DKGRAY, Color.rgb(170, 170, 170), RATE_APP_BTN);  
		 
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case RATE_APP_BTN:  GameScreen.RateApp(); m_isClose=true; break;  
			} 

			if(buttonPressed!=0)
				buttonPressed=0;
		} 
	 
	}	
}
