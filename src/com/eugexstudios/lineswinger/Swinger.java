package com.eugexstudios.lineswinger;

import java.util.Random;
import java.util.Vector; 

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;   

import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.ImageData;
import com.eugexstudios.framework.Input.TouchEvent; 
import com.eugexstudios.utils.Constants;

public class Swinger {
	
	private enum DIR {LEFT, RIGHT};
	private DIR                                  m_swingDirection;
	private DIR                                  m_lastDir; 
	private float 	                             DEFAULT_ROTATION_SPEED = 30;
	private float                                FALL_ROTATION_SPEED = 0.016f;
	private float                                m_fallRotate ;
	private int                                  FUEL_Y = 31; 
	private int                                  FUEL_X = 60; 
	private float                                COMBUSTION_X = 2.3f; 
	private float                                COMBUSTION_Y = 3.32f; 
//	private int                                  FUEL_Y = 1; 
//	private int                                  FUEL_X = 1; 
//	private float                                COMBUSTION_X = 0.001f; 
//	private float                                COMBUSTION_Y = 0.001f; 
	private float                                m_xPos;
	private float                                m_yPos;
	private float                                c_leftMost;
	private float                                c_rightMost; 
	private boolean                              m_onSwing;
	private float                                m_liftPower;
	private float                                m_fuelX; 
	private float                                l_accelY;
	private int                                  m_rad = 20;
	private Rect                                 m_bounds; 
	private Rect                                 m_sloMoBounds; 
	private Rect                                 m_leftFrontBounds; 
	private Rect                                 m_rightFrontBounds; 
	private Vector<int[]>                        m_track;
	private int                                  m_color;
	private int                                  m_limitY;
	private float                                m_pushY;  
	private boolean					             m_shattering; 
	private Vector<DotShatter>	                 m_dotShatter;
	private ImageData                			 m_imageData;
	private float                                m_rotation;
	private float                                m_rotateSpeed; 
	private boolean                              m_spinEnabled;
	private float                                m_postSwingSpin;
	
	private Rect                                 m_swingBounds; 
	public Swinger(ImageData p_imageData, boolean p_onRight, int p_color, int p_limitY)
	{  
		m_imageData   = p_imageData;
		m_track       = new Vector<int[]>();
		c_leftMost    = Constants.ScaleX(0.05f);
		c_rightMost   = Constants.ScaleX(0.848f);
		m_yPos        = Constants.ScaleY(0.45f);
		m_xPos        = p_onRight?c_rightMost:c_leftMost; 
		m_lastDir     = p_onRight?DIR.RIGHT:DIR.LEFT;
		m_liftPower   = 10f;
		m_onSwing     = false;
		m_color       = p_color; 
		m_shattering  = false; 
		m_dotShatter  = new Vector<DotShatter>(); 
		m_limitY      = p_limitY;
		  
		Random l_rand = new Random();
		m_rotation    = l_rand.nextInt(30)-15;  
		m_fallRotate=0;
		m_spinEnabled=true;
		if(GameScreen.GetSwingerUsed()==0)
		{
			//No rotation effect if default is used
			m_spinEnabled=false;
			DEFAULT_ROTATION_SPEED=0;
			FALL_ROTATION_SPEED=0; 
		} 

		m_swingBounds = null; 
	}
	public boolean isOnSwing()
	{
		return m_onSwing;
	}
	
	public void Paint(Graphics g)
	{
//		for(int idx=0;idx<m_track.size();idx++)
//		{
//			g.drawCircle((int)(m_track.get(idx)[0]),(int)(m_track.get(idx)[1]), m_rad, Color.DKGRAY); 
//		}
		if(m_shattering)
		{
			PaintShattered(g);
		}
		else
		{  
 
//			g.drawRect(m_leftFrontBounds,Color.BLACK);
//			g.drawRect(m_rightFrontBounds,Color.RED);
//			g.drawRect(m_sloMoBounds,Color.BLACK);
//			g.drawRect(m_bounds,Color.GRAY);
//			g.drawCircle((int)(m_xPos)+3,(int)(m_yPos)+6, m_rad, m_shadow); 
//			g.drawCircle((int)(m_xPos),(int)(m_yPos), m_rad, m_color);  
			g.drawImage(AssetManager.swingers, m_xPos , m_yPos ,m_imageData,(int)m_rotation); 
		}  
//		g.drawLine(0, (int)(m_bounds.top+m_bounds.height ()/2), CONFIG.SCREEN_WIDTH, (int)(m_bounds.top+m_bounds.height ()/2), Color.RED);
//		g.drawLine((int)(m_bounds.left+m_bounds.width()/2), 0, (int)(m_bounds.left+m_bounds.width()/2),  CONFIG.SCREEN_HEIGHT, Color.RED);
//
//		genSet.setTextProperties(30, Color.argb(255, 0, 0, 0), Align.LEFT); 
//		g.drawString(m_rotation+"", (int)(m_xPos),(int)(m_yPos), genSet.paint);

//		if(m_swingBounds!=null) 
//			g.drawRect(m_swingBounds, Color.WHITE);
	}	
	
	private void StopSwing()
	{  
		m_onSwing = false; 
		if(m_spinEnabled==false)return;
		m_postSwingSpin =360*4; 
		m_rotateSpeed = 8f;
		m_fallRotate=0;
	}
	public float Update(float p_deltaTime, float p_gravity,boolean p_started)
	{ 
		m_pushY = 0;
		m_yPos += p_deltaTime*p_gravity;
	 
		if(m_postSwingSpin>0 && m_rotateSpeed>0&& m_onSwing==false)
		{
			m_postSwingSpin-=m_rotateSpeed*p_deltaTime;
			rotateUpdate(p_deltaTime);
			m_rotateSpeed -= 0.35f * p_deltaTime;
		}
		
		if(m_onSwing && m_fuelX>0)
		{ 
			if(m_track.size()>30)
				m_track.remove(0);
			m_track.add(new int[]{(int)(m_xPos+24),(int)(m_yPos+24)});
			
			rotateUpdate(p_deltaTime);
			if(m_swingDirection == DIR.LEFT && m_xPos>c_leftMost )
			{
				m_xPos -=p_deltaTime*m_fuelX;
				m_fuelX-=p_deltaTime*COMBUSTION_X; 
				if(m_xPos<=c_leftMost)
				{
					StopSwing();
					m_xPos    = c_leftMost;
					GameScreen.PlayAddScore(); 
				}
			}
			else if(m_swingDirection == DIR.RIGHT  && m_xPos<c_rightMost)
			{
				m_xPos +=p_deltaTime*m_fuelX;
				m_fuelX-=p_deltaTime*COMBUSTION_X;
				if(m_xPos>=c_rightMost)
				{
					m_xPos    = c_rightMost;
					StopSwing();
					GameScreen.PlayAddScore(); 
				}
			} 
			
			if(m_fuelX<0)
			{
				m_fuelX =0;
				StopSwing();
				GameScreen.PlayAddScore(); 
			}
			else{
				if(m_liftPower>-5f)
				{
					l_accelY = p_deltaTime*m_liftPower;
					m_pushY -= l_accelY; 
					m_liftPower-=p_deltaTime*COMBUSTION_Y;
				}
			} 
		}
		else
		{

		} 
		
		if(m_onSwing&&m_yPos+m_pushY<m_limitY)
		{
			m_pushY = m_limitY-(float)(m_yPos+m_pushY); 
			m_yPos = m_limitY;
		}
		else if(p_started)
		{
			m_yPos+=m_pushY;  
			rotateUpdate(p_deltaTime,m_fallRotate); 
			m_fallRotate+=FALL_ROTATION_SPEED*p_deltaTime;
			m_pushY=0;
		}
 
		RecalcBounds(); 
		return m_pushY;
		 
	}
	private void RecalcBounds()
	{  
//		m_bounds = new Rect(
//					 (int)(m_xPos+35-m_rad)-14
//					,(int)(m_yPos+32-m_rad)
//					,(int)(m_xPos+35+ m_rad)+16
//					,(int)(m_yPos+32+m_rad+4));  // Stable
		m_bounds = new Rect(
		 (int)(m_xPos-m_rad)+24
		,(int)(m_yPos-m_rad)+24
		,(int)(m_xPos+m_rad*2)+24
		,(int)(m_yPos+m_rad*2)+24
		);  

		
		m_sloMoBounds  = new Rect(
				 (int)(m_xPos+35-m_rad)-10
				,(int)(m_yPos-m_rad -4)
				,(int)(m_xPos+35+ m_rad+10)
				,(int)(m_yPos+m_rad-22));

		m_leftFrontBounds = new Rect(
				 (int)(m_xPos+35-m_rad-35) 
				,(int)(m_yPos+32-m_rad)
				,(int)(m_xPos+35-m_rad-35)+20
				,(int)(m_yPos+32+m_rad+4)); 
		

		m_rightFrontBounds = new Rect(
				 (int)(m_xPos+35-m_rad+65)
				,(int)(m_yPos+32-m_rad)
				,(int)(m_xPos+35-m_rad+65)+20
				,(int)(m_yPos+32+m_rad+4)); 
		
		
		if(m_track.size()>0) 
		{
			int l_x1 = (int)m_xPos;
			int l_y1 = (int)m_yPos;
			
			int l_x2 = m_track.get(m_track.size()-1)[0];
			int l_y2 = m_track.get(m_track.size()-1)[1];

			int x= l_x1>l_x2? l_x2:l_x1; 
			int y= l_y1>l_y2? l_x2:l_y1; 
			int width =  Math.abs(l_x1-l_x2);
			int height = Math.abs(l_y1-l_y2); 
			m_swingBounds = new Rect( x,y,x+width,y+height  ); 
		}
		  
	} 
	
	public void TouchUpdates(TouchEvent g)
	{  
		if(g.type == TouchEvent.TOUCH_DOWN)
		{ 
			swing();
		} 
	}
	
	public boolean isGoingLeft()
	{
		return m_swingDirection == DIR.LEFT;
	}
	public boolean isGoingRight()
	{
		return m_swingDirection == DIR.RIGHT;
	}
	public boolean leftCollides(Rect p_bounds)
	{
		return m_leftFrontBounds.intersect(p_bounds);
	} 
	public boolean rightCollides(Rect p_bounds)
	{
		return m_rightFrontBounds.intersect(p_bounds);
	}
	public boolean collides(Rect p_bounds)
	{
		if(m_swingBounds==null)
		{ 
			return m_bounds.intersect(p_bounds);
		}
		return m_swingBounds.intersect(p_bounds)||  m_bounds.intersect(p_bounds);
	}
	public boolean collidesSlomo(Rect p_bounds)
	{
		return m_sloMoBounds.intersect(p_bounds);
	}
	
	public void hit()
	{ 
//	 genSet.game.Vibrate((long)(25));
	}
	 
	private void swing()
	{
		m_onSwing=true;
		m_rotateSpeed=DEFAULT_ROTATION_SPEED;
		AssetManager.shoot_sound.play();
		m_fuelX = FUEL_X;
		m_liftPower = FUEL_Y;
		 
		if(m_lastDir== DIR.RIGHT)
		{ 
			m_swingDirection = DIR.LEFT;
		} 
		else
		{ 
			m_swingDirection = DIR.RIGHT;
		}
		m_lastDir=m_swingDirection;
	}
	
	public int getPosY()
	{
		return (int)m_yPos;
	}	
	
	public int getRad()
	{
		return (int)m_rad;
	}
	
	public int getColor()
	{
		return (int)m_color;
	}
	
	public Point getPos()
	{
		return new Point((int)(m_xPos),(int)(m_yPos));
	}

	public void Shatter()
	{ 
		AssetManager.wrong_hit_shield.play();

		m_shattering=true;
		Random l_rand = new Random();
//		Disapear();
		
		int SHATTER_SIZE=37+l_rand.nextInt(5);
  
		for(int idx=0;idx<SHATTER_SIZE;idx++)
		{
			m_dotShatter.add(new DotShatter(getPos(),m_color,false));
		}
	}
	
	
	public void PaintShattered(Graphics g)
	{  
		for(int idx=0;idx<m_dotShatter.size();idx++)
		{ 
			m_dotShatter.get(idx).Paint(g);
		 }
	}
	
	public void UpdateShatter(float p_deltaTime)
	{
		if(m_shattering)
		{
		  for(int idx=0;idx<m_dotShatter.size();idx++)
		   {
			   m_dotShatter.get(idx).Update(p_deltaTime);
		   }
		}
		 
	}
	private void rotateUpdate(float p_deltatime,float p_rotateSpeed)
	{ 
		if(m_swingDirection == DIR.LEFT)
		{
			m_rotation -= p_deltatime* p_rotateSpeed;
			if(m_rotation<-360)
				m_rotation=0;
			
		}
		else
		{
			m_rotation += p_deltatime* p_rotateSpeed;
			if(m_rotation>360)
				m_rotation=0;
			
		}
	}
	private void rotateUpdate(float p_deltatime)
	{ 
		if(m_swingDirection == DIR.LEFT)
		{
			m_rotation -= p_deltatime* m_rotateSpeed;
			if(m_rotation<-360)
				m_rotation=0;
			
		}
		else
		{
			m_rotation += p_deltatime* m_rotateSpeed;
			if(m_rotation>360)
				m_rotation=0;
			
		}
	}
}
