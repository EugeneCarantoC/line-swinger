package com.eugexstudios.lineswinger;

import java.util.Random;
 
import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;

import android.graphics.Point;

public class DotShatter {

	private Point m_pos;
	private float m_speedX;
	private float m_speedY;
	private short m_rad;
	private int   m_force; 
	private int   m_color; 
	private Random m_rand = new Random();
	
	public DotShatter(Point p_pos, int p_color, boolean p_forFinish)
	{
		m_rand   = new Random();
		m_pos    = p_pos;
		m_speedX = (m_rand.nextBoolean()?m_rand.nextInt(18)+(p_forFinish?1:4):-(m_rand.nextInt(18)+(p_forFinish?1:4)));
		m_speedY = (m_rand.nextBoolean()?m_rand.nextInt(18)+(p_forFinish?1:4):-(m_rand.nextInt(18)+(p_forFinish?1:4)));
		
		m_rad   = (short)(m_rand.nextInt(4)+6);
		m_color = p_color; 
		m_force = (short)(p_forFinish?280:260);
	}
	
	public void Update(float p_deltatime)
	{ 
		if(m_force<=0) return;
		m_pos.x += m_speedX * p_deltatime;
		m_pos.y += m_speedY * p_deltatime;

		if(m_pos.x<=0 || m_pos.x>=CONFIG.SCREEN_WIDTH)
		{
			if(m_pos.x<=0)
			{
				m_pos.x = 0;
			}
			else if(m_pos.x>=CONFIG.SCREEN_WIDTH)
			{
				m_pos.x = CONFIG.SCREEN_WIDTH;
			}
			
			m_speedX = -m_speedX; 
			m_speedY = (m_rand.nextBoolean()?m_rand.nextInt(16)+4:-(m_rand.nextInt(16)+4)); 
		}
		
		if(m_pos.y<=0 || m_pos.y>=CONFIG.SCREEN_HEIGHT)
		{
			if(m_pos.y<=0)
			{
				m_pos.y = 0;
			}
			else if(m_pos.y>=CONFIG.SCREEN_HEIGHT)
			{
				m_pos.y = CONFIG.SCREEN_HEIGHT;
			}
			
			m_speedY = -m_speedY;

			m_speedX = (m_rand.nextBoolean()?m_rand.nextInt(16)+4:-(m_rand.nextInt(16)+4)); 
			 
		}
		
		m_force -= Math.abs(m_speedX);
		
	}
	
	public void Paint(Graphics g)
	{
		if(m_force>0)
		g.drawCircle(m_pos, m_rad, m_color); 
	}
	
}
