package com.eugexstudios.lineswinger;

import java.util.Random;

import android.graphics.Color;
import android.graphics.Rect;

import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.utils.ColorUtil;

public class Line {

	private float m_xPos;
	private float m_yPos; 
	private int m_width;
	private int m_height;
	private int m_color;

	private float m_speedY; 
	private Rect m_bounds;
	private boolean m_isBreakable;
	
	private boolean m_active;
	private float m_deactiveSpeedX;
	private float m_deactiveSpeedY; 
	private float FADE_SPEED=0.01f; 
	private float m_fade;
	
	public Line(int p_xPos, int p_yPos, int p_width, int p_height,float p_speedY, boolean p_isBreakable,int p_color)
	{
		m_xPos   = p_xPos;
		m_yPos   = p_yPos;
		m_width  = p_width;
		m_height = p_height;
		m_speedY = p_speedY;
		m_isBreakable = p_isBreakable;
//		m_isBreakable=true;
		m_color = p_color;
		m_fade = p_isBreakable?0.5f:1;
		m_active=true;
		
		RecalcBounds();
	}
	private void RecalcBounds()
	{ 
		m_bounds = new Rect((int)(m_xPos),(int)(m_yPos),(int)(m_xPos)+ m_width,(int)(m_yPos)+m_height);
	}

	public void Paint(Graphics g) 
	{  
//		g.drawRect(m_bounds,Color.GRAY);
		if(m_active)
		{
			g.drawRect(m_bounds, m_isBreakable?ColorUtil.SetOpacity(m_fade, m_color):m_color);
		}
		else
		{
			if(m_fade>0)
				g.drawRect(m_bounds,  ColorUtil.SetOpacity(m_fade, m_color));
		}
	} 
	
	public void Updates(float p_deltaTime, float p_swingerPush) 
	{  
//		p_swingerPush=0;
		
		if(m_active)
		{ 
			m_yPos += p_deltaTime * m_speedY + p_swingerPush;	
		}
		else
		{
			m_xPos += p_deltaTime * m_deactiveSpeedX;	
			m_yPos += p_deltaTime * m_deactiveSpeedY + p_swingerPush;	 
			if(m_fade>0)
			{
				m_fade -= p_deltaTime*FADE_SPEED;
			}
		}
		RecalcBounds(); 
	}
	public int getPosY()
	{
		return (int)m_yPos;
	}
	public int getHeight()
	{
		return (int)m_height;
	}	
	public int getColor()
	{
		return (int)m_color;
	}
	public boolean isBreakable()
	{
		return m_isBreakable &&m_active ;
	}
	public boolean fadedAway()
	{
		return m_fade<=0;
	}
	public boolean isActive()
	{
		return m_active;
	}
	
	public Rect getBounds()
	{
		return m_bounds;
	}
	public void deactivate()
	{ 
		m_active =false;
		Random l_rand = new Random();
		m_deactiveSpeedX = l_rand.nextBoolean()?-l_rand.nextFloat()-l_rand.nextInt(2):l_rand.nextFloat()+l_rand.nextInt(2); 
		m_deactiveSpeedY = l_rand.nextInt(5)+3;
//		m_deactiveSpeedY = m_deactiveSpeedY*1.2f; 
	}	
	public void cut(float p_cutPosY)
	{
		 m_active =false; 
	}
	 
		
}
