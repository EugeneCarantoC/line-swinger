package com.eugexstudios.lineswinger;

import java.util.Random;
import java.util.Vector; 

import android.graphics.Color; 
import android.graphics.Paint.Align;  

import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics; 
import com.eugexstudios.framework.Input.TouchEvent; 
import com.eugexstudios.lineswinger.InstructionScreen.type; 
import com.eugexstudios.utils.ColorUtil;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.genSet;

public class PlayScreen extends Screen {
	
	public  enum State 
	{  
		  PLAYING
		, GAMEOVER 
		, INSTRUCTION
	}; 
	private static int                           MIN_LINE_BLOCKS = 7;
	private static int                           MAX_LINE_BLOCKS = 15-MIN_LINE_BLOCKS;
	private int                                  SWINGER_LIMIT_Y = 150;
	private final float                          GRAVITY_LIMIT = 32.45f; 
	private final float                          GRAVITY_INCR = 0.16f;
	private final float                          GRAVITY_INCR2 = 0.07f;
	private final int                            Y_BLOCK_SIZE = 25;
	private final int                            LINE_CAP = 20;
	private final int                            LINE_WIDTH = Constants.ScaleX(0.021f);
	private final int                            BLOCK_LINE_COLOR = Color.rgb(50,60,50); 
    private static GameOverWindow     			 m_gameOverWindow;
	private static State                         m_state;
	private static int					         m_score;
	private Swinger                              m_swinger;
	private float                                m_gravity;
	private Vector<Line>                         m_lines;
    private float                                m_lineSpeed=10.92f;
    private Random                               m_rand;
    private float                                m_gameOverCountdown;	
    private boolean                              m_goingToGameOver;
    private boolean                              m_started;
    
	private int		                             m_gemCount; 
	private int		                             m_gemChance; 
	private Vector<Gem>                          m_gems; 

    private boolean                              m_isVibrateOn;
	
    private InstructionScreen m_instructionScreen;
   
    public PlayScreen()
   	{
    	Initialize();
   	}
  
    public PlayScreen(type p_ins)
	{
    	Initialize();
		switch(p_ins)
		{
			case TAP:ShowInstruction(type.TAP); break;
		} 
	} 
    private void Initialize()
    { 
    	m_isVibrateOn = GameScreen.IsVibrateOn();
		RestartGame();
    }
    public void RestartGame()
    {
		SetState(State.PLAYING);	
		m_rand                         = new Random();
		m_gemChance                    = m_rand.nextInt(100)+400; 
		m_gemCount                     = GameScreen.GetGemsCollected();
		m_gems                         = new Vector<Gem>();  
		initializeLines();
		initializeSwinger();
		m_score                        = 0;
		m_gravity                      = 0;
	    MIN_LINE_BLOCKS                = 7;
		MAX_LINE_BLOCKS                = 11-MIN_LINE_BLOCKS; 
		m_gameOverCountdown            = 0;
		m_goingToGameOver              = false;
		m_onSlowMo                     = false;
		m_started                      = false;
//		m_gemChance=30;
	 
	}
    
    private void initializeSwinger()
    {
    	m_swinger                      = new Swinger(GameScreen.GetSwingerImageData(GameScreen.GetSwingerUsed()),m_rand.nextBoolean(),Color.WHITE, SWINGER_LIMIT_Y);
    	
    }
    private void ShowInstruction(type p_type)
    { 
		m_instructionScreen  = new InstructionScreen(p_type);
		SetState(State.INSTRUCTION);	
		
    }
 
    private void initializeLines()
	{
    	m_lines = new Vector<Line>();
		int l_baseY=CONFIG.SCREEN_HEIGHT;
		 
		for(int idx=0;idx<LINE_CAP;idx++)
		{
			generateLine(l_baseY,idx%2==0); 
			l_baseY-=m_lines.get(idx).getHeight();
		} 
	 }
	  
	private void gameOverUpdates(float p_deltaTime)
	{
		m_gameOverWindow.Updates(p_deltaTime);  
		m_swinger.UpdateShatter(p_deltaTime);
	}
	
	private void generateLine(int p_baseY, boolean l_isBreakable)
	{
		int l_height = (MIN_LINE_BLOCKS+m_rand.nextInt(MAX_LINE_BLOCKS))*Y_BLOCK_SIZE;
		m_lines.add(new Line(
			     CONFIG.SCREEN_MID-LINE_WIDTH/2
				 ,p_baseY-l_height
				 ,LINE_WIDTH
				 ,l_height
				 ,m_lineSpeed 
				 ,l_isBreakable
				 ,l_isBreakable?Color.argb(150, 255, 255, 255):BLOCK_LINE_COLOR
			 ));
	 }  
	 
	@Override
	public void Paint(Graphics g) 
	{
		switch (m_state) 
		{
		case PLAYING:  PaintGame(g);     break;  
		case GAMEOVER: PaintGameOver(g); break; 
		case INSTRUCTION: PaintInstructions(g); break;
		default:break;
		}  
	}
	
	private void PaintInstructions(Graphics g)
	{  
		PaintGame(g); 
		g.drawRect(Constants.SCREEN_RECT, Color.argb(80,0,0,0));    
		m_instructionScreen.Paint(g);
		
		m_swinger.Paint(g);
	}
	private void PaintGame(Graphics g)
	{ 
		g.drawRect(Constants.SCREEN_RECT, ColorUtil.BG1_LAYER1);     
		g.drawRect(Constants.SCREEN_RECT, ColorUtil.BG1_LAYER2);   
		
		if(m_state!=State.INSTRUCTION)
		{
			genSet.setTextProperties(115, Color.argb(25, 0, 0, 0), Align.CENTER); 
			g.drawStringFont(m_score+"", 65+5, 110+4, AssetManager.Comforta, genSet.paint);
			genSet.setTextProperties(110, Color.WHITE, Align.CENTER);  
			g.drawStringFont(m_score+"", 65, 110, AssetManager.Comforta, genSet.paint); 
		}
		
		for(int idx=0;idx<m_lines.size();idx++)
		{
			m_lines.get(idx).Paint(g);
		} 
		
		PaintGemCount(g);
		PaintGems(g);

		m_swinger.Paint(g);
	}

	private void PaintGems(Graphics g)
	{
		for(int idx=0;idx<m_gems.size();idx++)
		{
    		m_gems.get(idx).Paint(g);
		}
    		
	}
		
	private void PaintGemCount(Graphics g)
	{
		g.drawImage(AssetManager.sprites,CONFIG.SCREEN_WIDTH-Constants.GEMCOUNT_IMG.width-5, Constants.GEMCOUNT_IMG.height/2+3, Constants.GEMCOUNT_IMG);
		
		genSet.setTextProperties(56, Color.argb(25, 0, 0, 0), Align.RIGHT); 
		g.drawStringFont(m_gemCount+"", 5+CONFIG.SCREEN_WIDTH-Constants.GEMCOUNT_IMG.width-10, 4+Constants.GEMCOUNT_IMG.height+Constants.GEMCOUNT_IMG.height/2, AssetManager.Comforta, genSet.paint);
	
		genSet.setTextProperties(56, Color.argb(254, 254, 243, 255), Align.RIGHT); 
		g.drawStringFont(m_gemCount+"", CONFIG.SCREEN_WIDTH-Constants.GEMCOUNT_IMG.width-10, Constants.GEMCOUNT_IMG.height+Constants.GEMCOUNT_IMG.height/2, AssetManager.Comforta, genSet.paint);
	}
	
	private void PaintGameOver(Graphics g)
	{
		PaintGame(g);  
		m_gameOverWindow.Paint(g); 
	}
	
	private void swingerAndLineUpdates(float p_deltaTime)
	{ 
		boolean l_lastSwingStat= m_swinger.isOnSwing();
		float l_swinngerPush = m_swinger.Update(p_deltaTime, m_gravity,m_started);
	
		//If from swing
		if(l_lastSwingStat&&!m_swinger.isOnSwing())
		{
			m_gravity=0.4f;
		}
		
		if(m_gravity<GRAVITY_LIMIT&&m_started)
		{
			m_gravity += (float)(p_deltaTime*GRAVITY_INCR);
			
			if(m_gravity>3f)
				m_gravity+= (float)(p_deltaTime*GRAVITY_INCR2);

			if(m_gravity>GRAVITY_LIMIT)
				m_gravity=GRAVITY_LIMIT;
		}
		
		if(m_swinger.getPosY()+m_swinger.getRad()>CONFIG.SCREEN_HEIGHT)
		{
			m_swinger.Shatter(); 
			GoToGameOver();
		}

		int l_lineToCut = -1;
		for(int idx=0;idx<m_lines.size();idx++)
		{
			m_lines.get(idx).Updates(p_deltaTime,l_swinngerPush); 

			if(m_onSlowMo==false)
			if(m_lines.get(idx).isActive()&&m_swinger.collides(m_lines.get(idx).getBounds()))
			{ 
				if(m_lines.get(idx).isBreakable())
				{		
					m_swinger.hit();
					l_lineToCut=idx; 
				}	
				else
				{
					m_swinger.Shatter();
					GoToGameOver();
				} 
			}
			if( m_lines.get(idx).fadedAway())
			{
				m_lines.remove(idx);
				generateLine(m_lines.get(m_lines.size()-1).getPosY(),!m_lines.get(m_lines.size()-1).isBreakable());
				idx--;
			}
			if(m_lines.get(idx).getPosY()>CONFIG.SCREEN_HEIGHT)
			{
				m_lines.remove(idx);
				generateLine(m_lines.get(m_lines.size()-1).getPosY(),!m_lines.get(m_lines.size()-1).isBreakable());
			    idx--;
			}
		 }

		if(l_lineToCut!=-1)
		{ 
			for(int idx2=0;idx2<l_lineToCut;idx2++)
			{ 
				m_lines.get(idx2).deactivate();
 			} 
			cutLine(l_lineToCut,m_swinger.getPosY());
			m_lines.get(l_lineToCut).deactivate();

		} 
		if(SLOWMO_ON_ALMOST_HIT)
		{
			if(m_onSlowMo==false)
			{
				for(int idx=0;idx<m_lines.size();idx++)
				{ 
					if(m_lines.get(idx).isActive()&&m_swinger.collidesSlomo(m_lines.get(idx).getBounds())&&m_lines.get(idx).isBreakable()==false)
					{ 
						SlowMo(1.2f, 0);
					}
				}
			}
		}
	}
	private boolean SLOWMO_ON_ALMOST_HIT =false;
	private boolean SLOWMO_ON_GET_GEM =true;
	private void SlowMo(float p_duration, float p_reduceBuffer)
	{
		m_onSlowMo=true; 
		m_slowMoDuration = p_duration;
		m_slowMoBuffer = p_reduceBuffer;
		m_slowMoSpeed=0.01f;
	}
	private float m_slowMoDuration,m_slowMoBuffer,m_slowMoSpeed; 
	private boolean m_onSlowMo;
 
//	private float m_buffer=0;
	private void onPlayUpdates(float p_deltaTime)
	{
//		if(m_buffer<10)
//		{
//			m_buffer+=0.1f;
//			return;  
//		}
//		else{
//			m_buffer=0;
//		}
		if(m_goingToGameOver==false)
		{
			if(m_onSlowMo)
			{
				if(m_slowMoDuration<m_slowMoBuffer)
				{
					m_slowMoSpeed+=0.02f*p_deltaTime;
				}

				m_slowMoDuration-=0.01f*p_deltaTime;
				p_deltaTime =m_slowMoSpeed*p_deltaTime;
				
				if(m_slowMoSpeed>=1||m_slowMoDuration<=0)
					m_onSlowMo=false;
			}
			
			swingerAndLineUpdates(p_deltaTime);
			updateGems(p_deltaTime);
		}
		if(m_gameOverCountdown>0)
		{
			m_swinger.UpdateShatter(p_deltaTime);
			m_gameOverCountdown-=p_deltaTime*1f;
			if(m_gameOverCountdown<=0)
			{
				EndGame();
			}
		}
	}
	
    private int GEM_AREA [] = new int[]
    		{
    			 Constants.ScaleX(0.1f)
    			,Constants.ScaleX(0.375f)-Constants.ScaleX(0.1f)
    			,Constants.ScaleX(0.55f)
    			,Constants.ScaleX(0.85f)-Constants.ScaleX(0.55f)
    			
    		};
    private void updateGems(float p_deltatime)
    { 
    	if(m_rand.nextInt(m_gemChance)==0)
    	{
//    		if(m_rand.nextBoolean()||m_gems.size()==0)
    		{
    			m_gems.add(
        				new Gem( 
        						m_rand.nextBoolean()?	
        								GEM_AREA[0]+m_rand.nextInt(GEM_AREA[1])
        								:GEM_AREA[2]+m_rand.nextInt(GEM_AREA[3])
        						));
        	
    		}
    	}
    	
    	for(int idx=0;idx<m_gems.size();idx++)
		{
    		m_gems.get(idx).updates(p_deltatime);

    		if(m_swinger.collides(m_gems.get(idx).getBounds()))
    		{
    			int l_gemValue=m_gems.get(idx).getValue();
    			if(l_gemValue>0)
    			{
	    			GameScreen.AddGems(l_gemValue);
	    			m_gemCount+=l_gemValue;
	    			m_gems.get(idx).disapear();
	    			m_gems.get(idx).removeValue();
    			}
    		}
    		
    		if(m_gems.get(idx).isGone())
    		{
    			m_gems.remove(idx); 
    			idx--;
    		}
    		
    		if(SLOWMO_ON_GET_GEM)
    		{
    			if(m_onSlowMo==false)
    			{
	        		if((m_swinger.rightCollides(m_gems.get(idx).getBounds())&&m_swinger.isGoingRight() &&m_gems.get(idx).isOnShatter()==false)
	        				||( m_swinger.leftCollides(m_gems.get(idx).getBounds())&&m_swinger.isGoingLeft() &&m_gems.get(idx).isOnShatter()==false))
	        		{
	        			SlowMo(1.2f,0.3f);
	        		}
        		}
    		} 
		}
    }
    
	public static void addScore()
	{
		m_score++;
		
		if(m_score==15){
		    MIN_LINE_BLOCKS = 6;
			MAX_LINE_BLOCKS = 13-MIN_LINE_BLOCKS;
		}else if(m_score==30){
		    MIN_LINE_BLOCKS = 5;
			MAX_LINE_BLOCKS = 14-MIN_LINE_BLOCKS;
		}
		else if(m_score==50){
		    MIN_LINE_BLOCKS = 4;
			MAX_LINE_BLOCKS = 14-MIN_LINE_BLOCKS;
		}
	}
	
	@Override
	public void Updates(float p_deltaTime) 
	{ 
		switch (m_state) 
		{
			case PLAYING:  onPlayUpdates(p_deltaTime);        break;  
			case GAMEOVER: gameOverUpdates(p_deltaTime);      break; 
			case INSTRUCTION:InstructionsUpdate(p_deltaTime); break; 
		}  
	}
	
	public void InstructionsUpdate(float p_deltaTime) 
	{ 
		m_instructionScreen.Updates(p_deltaTime); 
		switch(m_instructionScreen.Type())
		{
			case TAP:  m_swinger.Update(p_deltaTime, 0,true);   break;
		}
		
		if(m_instructionScreen.willExitInstruction())
		{
			SetState(State.PLAYING);
			initializeSwinger();
			m_score = 0;
		}
	}
	
	private void cutLine(int p_lineIdx, int p_cutPosY)
	{
		// ###### CUT THE LINE TO TWO NEW LINES
		int l_basePosY = m_lines.get(p_lineIdx).getPosY();
		int l_baseHeight = m_lines.get(p_lineIdx).getHeight();
		int l_baseColor = m_lines.get(p_lineIdx).getColor();

		final int l_basePosY_1 = l_basePosY;
		final int l_height_1   = p_cutPosY-l_basePosY_1;
		 
		final int l_basePosY_2 = l_basePosY_1 + l_height_1;
		final int l_height_2   = l_baseHeight - l_height_1;

		m_lines.remove(p_lineIdx); 
		m_lines.add( 
				p_lineIdx,
				 new Line(
				      CONFIG.SCREEN_MID-LINE_WIDTH/2
					 ,l_basePosY_1
					 ,LINE_WIDTH
					 ,l_height_1
					 ,m_lineSpeed 
					 ,true
					 ,l_baseColor
					) 
				 ); 
		 m_lines.add(  
				 p_lineIdx,
				   new Line(
				      CONFIG.SCREEN_MID-LINE_WIDTH/2
					 ,l_basePosY_2
					 ,LINE_WIDTH
					 ,l_height_2
					 ,m_lineSpeed 
					 ,true
					 ,l_baseColor
					)
				 );
		 
	}

	public  void GoToGameOver()
	{  
		m_gameOverCountdown = 40f;
		m_goingToGameOver=true;

		if(m_isVibrateOn)
		{
			genSet.game.Vibrate(new long[]{0,85,50,85,50,180});
		 }
	}
	
	public static void EndGame()
	{  
		boolean l_newBestScore = m_score>GameScreen.GetBestScore();
		m_gameOverWindow = new GameOverWindow(m_score, l_newBestScore);
		m_gameOverWindow.Show(false);
		SetState(State.GAMEOVER);
		
		if(l_newBestScore)
		{
			GameScreen.SetBestScore(m_score);
		}
	}
	
	public static void SetState(State p_state) 
	{
		m_state = p_state; 
	}
	
	@Override
	public void TouchUpdates(TouchEvent g)
	{
		switch (m_state) 
		{  
			case PLAYING:   m_swinger.TouchUpdates(g); 
			
			if(g.type == TouchEvent.TOUCH_DOWN&&m_started==false)
			{
				m_started=true;
			}
			break;  
			case GAMEOVER: m_gameOverWindow.TouchUpdates(g);	   break;  
			case INSTRUCTION: InstructionsTouchUpdates(g);	 	break;  
			default:break;
		}  
	}
	
	public void InstructionsTouchUpdates(TouchEvent g) 
	{ 
		m_instructionScreen.TouchUpdates(g); 
		switch(m_instructionScreen.Type())
		{
			case TAP:
				if(m_instructionScreen.isButtonPressed()==false)
				{
					m_swinger.TouchUpdates(g); 
				} 
				break;
		}
	}
	
	@Override
	public void backButton() 
	{
		switch (m_state) 
		{  
			case PLAYING:     GameScreen.GoToMenu();             break;  
			case GAMEOVER:    GameScreen.GoToMenu();             break;  
			case INSTRUCTION: m_instructionScreen.backButton();  break;  
			default:break;
		}  
	}

}
