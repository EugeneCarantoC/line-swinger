# Line Swinger

Line Swinger is a game created using JAVA programming language and is targeted for Android. 

## Game Mechanics

Swing through the breakable lines! Reach the top, avoid obstacles along the way.

## Author

* **Eugene C. Caranto**  

## Acknowledgments

*  [Kilobolt](http://www.kilobolt.com/game-development-tutorial.html) - Base framework used
